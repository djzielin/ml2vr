% TODO: the transfers should be packed for more effeciency

classdef vr_parameters < handle
    properties %(Access=private,Hidden=true)
        vr_inter;
    end
    
    methods (Access=public)
        %% CONSTRUCTOR / DESTRUTOR
        function obj=vr_parameters(vi) %% constructor
            obj.vr_inter=vi;
            
        end
        function delete(obj) %% destructor
            
        end
        
        function set_background_color(obj,r,g,b)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(14,'uchar'); %%size of packet
            obj.vr_inter.send_data(8,'uchar'); %%environment
            obj.vr_inter.send_data(0,'uchar'); %%background color
            obj.vr_inter.send_data(r,'float32');
            obj.vr_inter.send_data(g,'float32');
            obj.vr_inter.send_data(b,'float32');
        end
        
        function set_wand_sensor_id(obj,value)
            obj.vr_inter.send_data(10,'uchar');
            obj.vr_inter.send_data(0,'uchar'); %% wand
            obj.vr_inter.send_data(value,'uchar');
            
            obj.vr_inter.send_data(5,'uchar'); % set app parameter
            obj.vr_inter.send_data(3,'uchar'); %%size of packet 
            obj.vr_inter.send_data(6,'uchar'); %% wand
            obj.vr_inter.send_data(0,'uchar');
            obj.vr_inter.send_data(value,'uchar');
        end      
        
        function set_joystick_threshold(obj, value)
            obj.vr_inter.send_data(10,'uchar');
            obj.vr_inter.send_data(1,'uchar'); %%joystick
            obj.vr_inter.send_data(value,'float32');
        end
    end
end