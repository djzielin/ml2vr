
classdef vr_virtual_hand < handle
    properties (Access=private,Hidden=true)
        vr_inter;
    end
    
    methods (Access=public)
        %% CONSTRUCTOR / DESTRUTOR
        function obj=vr_virtual_hand(vi) %% constructor
            obj.vr_inter=vi;
        end
        function delete(obj) %% destructor
            
        end
        
        function enable(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(2,'uchar'); %% drag
            obj.vr_inter.send_data(0,'uchar'); %%enable
            
            if value==true
                obj.vr_inter.send_data(1,'uchar'); %%true
            else
                obj.vr_inter.send_data(0,'uchar');
            end
        end
                
        function set_hand_button(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(2,'uchar'); %%slice
            obj.vr_inter.send_data(1,'uchar'); %%button id
            obj.vr_inter.send_data(value,'uchar');
        end
        
        
        function set_hand_constraints(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(8,'uchar'); %% size
            obj.vr_inter.send_data(2,'uchar'); %% drag
            obj.vr_inter.send_data(2,'uchar'); %% set constraint
            
            x_constrain=0;
            y_constrain=0;
            z_constrain=0;
            h_constrain=0;
            p_constrain=0;
            r_constrain=0;
            
            if ~isempty(strfind(value,'X'))
                x_constrain=1;
            end
            if ~isempty(strfind(value,'Y'))
                y_constrain=1;
            end
            if ~isempty(strfind(value,'Z'))
                z_constrain=1;
            end
            if ~isempty(strfind(value,'H'))
                h_constrain=1;
            end
            if ~isempty(strfind(value,'P'))
                p_constrain=1;
            end
            if ~isempty(strfind(value,'R'))
                r_constrain=1;
            end
            
            obj.vr_inter.send_data(x_constrain,'uchar');
            obj.vr_inter.send_data(y_constrain,'uchar');
            obj.vr_inter.send_data(z_constrain,'uchar');
            obj.vr_inter.send_data(h_constrain,'uchar');
            obj.vr_inter.send_data(p_constrain,'uchar');
            obj.vr_inter.send_data(r_constrain,'uchar');
        end
    end
end