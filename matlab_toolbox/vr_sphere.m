%TODO
%add code to dynamically change radius
%can we combine the unitX,unitY,unitZ into one variable?
%or just use transform node once intercept/renderer supports it

classdef vr_sphere < handle 
    properties
        unitX;
        unitY;
        unitZ;
        
        baseX;
        baseY;
        baseZ;
        
        currentX;
        currentY;
        currentZ;
        
        basePos;
        cX;
        cY;
        cZ;
        fhandle;
        color;
        
    end
    methods
        function obj=vr_sphere(radius, divisions)
            if nargin <2; divisions=10; end;
            if nargin <1; radius=1;      end;
                    
            [obj.unitX,obj.unitY,obj.unitZ]=sphere(divisions); %%do higher number for higher res sphere
                        
            obj.baseX=obj.unitX*radius;
            obj.baseY=obj.unitY*radius;
            obj.baseZ=obj.unitZ*radius;
            
            obj.color=ones(size(obj.baseZ,1),size(obj.baseZ,2),3);
                        
            obj.basePos=[obj.baseX(:) obj.baseY(:) obj.baseZ(:)];
            obj.basePos(:,4)=1;
            
            obj.setPos(0,0,0);
            obj.setColor(0,1,0);
        end
        
        function setColor(obj, r, g, b)
            obj.color(:,:,1)=r;
            obj.color(:,:,2)=g;
            obj.color(:,:,3)=b;

        end
        
        function setMatrix4(obj, m)
            res=obj.basePos*m';
            obj.currentX=reshape(res(:,1),size(obj.baseX));
            obj.currentY=reshape(res(:,2),size(obj.baseX));
            obj.currentZ=reshape(res(:,3),size(obj.baseX));
            %TODO; extract pos3 for cX,cY,cZ
        end
        
        function [X,Y,Z]=getPos(obj) %TODO make work for matrix4
            X=obj.cX;
            Y=obj.cY;
            Z=obj.cZ;
        end
        
        function setPos(obj,X,Y,Z)
            obj.currentX=obj.baseX+X;
            obj.currentY=obj.baseY+Y;
            obj.currentZ=obj.baseZ+Z;
            
            obj.cX=X;
            obj.cY=Y;
            obj.cZ=Z;
        end
        
        function render(obj)
           obj.fhandle=surf(obj.currentX,obj.currentY,obj.currentZ,obj.color); 
        end
        
        function translate(obj,X,Y,Z)
            obj.currentX=obj.currentX+X;
            obj.currentY=obj.currentY+Y;
            obj.currentZ=obj.currentZ+Z;
            
            obj.cX=obj.cX+X;
            obj.cY=obj.cY+Y;
            obj.cZ=obj.cZ+Z;
        end
        
    end
end
