function [distance position]=zgx_distancepointtoline(q,b)
x1=b(1,1);
x2=b(1,2);
y1=b(2,1);
y2=b(2,2);
if x1-x2==0
    distance=abs(x1-q(1));
    position=[x1;q(2)];
elseif y1-y2==0
    distance=abs(y1-q(2));
    position=[q(1);y1];
else
    k=(y2-y1)/(x2-x1);
    x0=(k^2*x1+k*(q(2)-y1)+q(1))/(k^2+1);
    y0=k*(x0-x1)+y1;
    position=[x0; y0];
    distance=norm(q(1:2)-position);
end