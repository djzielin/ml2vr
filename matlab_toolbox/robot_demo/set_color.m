%   SET_COLOR.M -- demo for the potential method
%   JAN 21, 2013
%   =========================================================
%   Wenjie Lu, LISC, Duke University
%
%   FUNCTIONS and FILES used by POTENTIAL_EXAMPLE
%           
%   DEFINITION of VARIABLES
%           obs_index         =   obstacle index
%           color             =   color
%

%   BEGINNING of MAIN PROGRAM
%   =========================

function fi_obstacle_color=set_color(fi_obstacle_color,obs_index,face_color)
fi_obstacle_color{obs_index}=face_color;
end