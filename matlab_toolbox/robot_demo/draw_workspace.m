% 	DRAW_WORKSPACE.M -- draw the 3D workspace 
%   July 28, 2012
%   =========================================================
%   Wenjie Lu, LISC, Duke University
%
%   FUNCTIONS and FILES used by CREATE_WORKSPACE 
%           DrawParaPipe.m          draws 3D obstacles
%
%   DEFINITION of VARIABLES

    
%   BEGINNING of MAIN PROGRAM
%   =========================
plot(0.0);
set(gca,'Box','on');
set(gca,'YDir','reverse')
axis equal tight
axis([0 (lengthx) 0 (lengthy) 0 10]);
%view(45,90);
%view(0,90);
%view([2,3,2]);
view([27,21]);

hold on
for i=1:length(fi_obstacle)
    if isempty(fi_obstacle_color{i})
        fi_obstacle_color{i}='blue';
        'in here'
    end
    if isempty(fi_obstacle_edge_color{i}) 
        fi_obstacle_edge_color{i}=fi_obstacle_color{i};
    end
    DrawParaPipe(fi_obstacle{i},fi_obstacleheight{i},fi_obstacle_color{i},fi_obstacle_edge_color{i}); 
end



os.render(); %% render the goal
%%scatter(goal(1),goal(2),300,'r','filled')

x=[0 0 lengthx lengthx 0];
y=[0 lengthy lengthy 0 0];
z=[0 0 0 0 0];
fill3(x,y,z,'g'); %%render the floor

