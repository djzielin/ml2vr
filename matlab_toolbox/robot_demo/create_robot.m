%   CREATE_ROBOT.M -- create the rectangular robot with specified hight 
%   July 28, 2012
%   =========================================================
%   Wenjie Lu, LISC, Duke University
%
%   FUNCTIONS and FILES used by CREATE_WORKSPACE 
%           
%   DEFINITION of VARIABLES
%           robot             =   four coordinates of a rectangle 
%           Robot_height      =   robot hight
%           q                 =   initial configuration
%           v                 =   linear and angular speed
    
%   BEGINNING of MAIN PROGRAM
%   =========================
robot=[-0.15 0.15, 0.15, -0.15; -0.25 -0.25 0.25 0.25].*5; % geometry of robots should be consistent with gazebo
%robot=[0 0.3 0.3 0; 0 0 0.5 0.5].*5;
Robot_height=0.3425*9;
q=[5 5 pi*1/4 ]';
v=[0 0];



