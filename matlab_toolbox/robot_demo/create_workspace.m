%   CREATE_WORKSPACE.M -- creates a 3D polygonal workspace
%   January 31, 2012
%   =========================================================
%   Wenjie Lu and Ashleigh Swingler, LISC, Duke University
%
%   FUNCTIONS and FILES used by CREATE_WORKSPACE
%           DrawParaPipe.m          draws 3D obstacles
%
%   DEFINITION of VARIABLES
%           lengthx           =   x dimension of the workspace
%           lengthy           =   y dimension of the workspace
%           num_obst          =   number of obstacles
%           fi_obstacle       =   defines the position and geometry of the
%                                 obstacles in the (x,y) plane: order
%                                 vertices CCW
%           fi_obstacleheight =   defines the height of the obstacles
%           fi_obstacle_color =   defines the color of the obstacles
%           fi_obstacle_edge_color =  defines the edge color of the obstacles



%   BEGINNING of MAIN PROGRAM
%   =========================

clear



%   Initialize variables

lengthx = 50;
lengthy = 25;
num_obst = 3;
fi_obstacle = cell(num_obst,1);
fi_obstacleheight = cell(num_obst,1);
fi_obstacle_color=cell(num_obst,1);
fi_obstacle_edge_color=cell(num_obst,1);

fi_obstacle{1}=fliplr([ 7 8 10 10 8; 17 19 18 16 15]);
fi_obstacleheight{1}=[0 5];
fi_obstacle_color{1}='blue';
fi_obstacle_edge_color{1}='black';

fi_obstacle{2}=fliplr([35 37 42; 16 18 16]);
fi_obstacleheight{2}=[0 4];
fi_obstacle_color{2}='blue';
fi_obstacle_edge_color{2}='black';

fi_obstacle{3}=fliplr([18 18 21 21; 2 8.5 8.5 2]);
fi_obstacleheight{3}=[0 1];
fi_obstacle_color{3}='blue';
fi_obstacle_edge_color{3}='black';


goal=[50 20]';

os=vr_sphere(2); % sphere as goal
os.setColor(1,0,0);
os.setPos(goal(1),goal(2),0);

fi_obstacle_original=fi_obstacle;

%   Plot Workspace

figure('Renderer','OpenGL');
%%   figure
set(gca,'Box','on');
set(gca,'YDir','reverse')
axis equal tight
axis([0 (lengthx) 0 (lengthy) 0 10]);
view(45,90);

view([2,3,2]);

hold on
for i=1:length(fi_obstacle)
    if isempty(fi_obstacle_color{i})
        fi_obstacle_color{i}='blue';
    end
    if isempty(fi_obstacle_edge_color{i})
        fi_obstacle_edge_color{i}=fi_obstacle_color{i};
    end
    DrawParaPipe(fi_obstacle{i},fi_obstacleheight{i},fi_obstacle_color{i},fi_obstacle_edge_color{i});
end
hold off

