%This function is tested and perform well on different polygons
% This function is to use algorithm similar to Latombe pg123 to construct
% the CB of a obstacle
%input:
% fi_obstacle{index}=[x1,x2,...;y1,y2,...] includes the vertics use
% obstacle coordiate FW
%theta: robot position in arc
% fi: field 
% The axis uses screen coordinate as:
% ---------> X
% |
% |
% |
% |
% V
% Y

function zgx_computeC = zgx_computeCB3(robot,theta,fi_obstacle)
fi_cobstacle=cell(length(fi_obstacle),1); % intial C-obstacles

% convert the angle of robot to [0, 2pi]
if length(theta)==1 % translate situlation
    if theta<0
        theta=theta+2*pi;
    elseif theta>2*pi
        theta=theta-2*pi;
    end
else % include rotate situlation
    f=zeros(2,2,length(theta));
end

if length(theta)==1 % transaction case
    %transaction matrix f, x in the world=f*x in the robot
    f=[cos(theta) -sin(theta); sin(theta) cos(theta)];
    [m n]=size(robot); % n represents the edge number of robot
    p=robot;
    pw=f*p; % robot vertix in FW
    % matrix to compute robot configuration (robot center)
    %from a world position in fact it equals f.
    % x_robot_configuration in the world = position of edge
    % a in the world -f2* position of edge a in the robot coordinate
    f2=[cos(theta) -sin(theta); sin(theta) cos(theta)];
    % Now compute the normal for each edge of robot
    pw2=[pw(:,2:n),pw(:,1)];
    deltapw=pw2-pw; % vector of each edge
    phi=atan2(deltapw(2,:),deltapw(1,:)); % angle of each edge in FW
    phi=phi-1/2*pi; % angle of normal for each edge in FW
    phi=phi+2*pi*(phi<0); %convert to [0,2pi]
    minusphi=phi-pi; % minus normal
    minusphi=minusphi+2*pi*(minusphi<0); % convert to [0, 2pi]
    %%%%%%%%%%%%%%%%%%%%
    % compute CB for each obstacle
    for index=1:length(fi_obstacle)
        [mb nb]=size(fi_obstacle{index});
        ow2=[fi_obstacle{index}(:,2:nb), fi_obstacle{index}(:,1)];
        deltaow=ow2-fi_obstacle{index};
        phib=atan2(deltaow(2,:),deltaow(1,:)); % angle of each edge in FW
        phib=phib-1/2*pi; % angle of normal for each edge in FW
        phib=phib+2*pi*(phib<0); %convert to [0,2pi]
        unitc=[minusphi phib];
        [unitc i_unitc]=sort(unitc); % construct the unite circle
        %unitc=[untic unitc(1)]; % add the first normal to the end
        %i_unitc=[i_unitc i_unitc(1)] % add the first normal to the end
        for i=1:n % find possible tape A contact for robot
            ai=find(~(i_unitc-i)); % find the ith normal of robot in the array
            ai_i_unitc=i_unitc;
            ai_i_unitc(1:ai)=0;
            ri=find(ai_i_unitc>n);
            % there is one edge of obstacle after ai
            if (sum(ri)>0)
                fi_cobstacle{index}=[fi_cobstacle{index} fi_obstacle{index}(:,i_unitc(ri(1))-n)-f2*robot(:,i)];
            else
                ai_i_unitc=i_unitc;
                ai_i_unitc(ai:length(i_unitc))=0;
                ri2=find(ai_i_unitc>n);
                % there is one edge of obstacle before ai
                if (sum(ri2)>0)
                    fi_cobstacle{index}=[fi_cobstacle{index} fi_obstacle{index}(:,i_unitc(ri2(1))-n)-f2*robot(:,i)];
                end
            end
        end

        for i=n+1:length(i_unitc) % find possible type B contact for robot
            bi=find(~(i_unitc-i)); % find the (i-n)th normal of obstacle in the array
            bi_i_unitc=i_unitc;
            bi_i_unitc(1:bi)=n+1;
            ri=find(bi_i_unitc<=n);
            % there is an edge of robot after bi
            if (sum(ri)>0)
                fi_cobstacle{index}=[fi_cobstacle{index} fi_obstacle{index}(:,i-n)-f2*robot(:,i_unitc(ri(1)))];
            else
                bi_i_unitc=i_unitc;
                bi_i_unitc(bi:length(i_unitc))=n+1;
                ri2=find(bi_i_unitc<=n);
                % there is an edge of robot before bi
                if (sum(ri2)>0)
                    fi_cobstacle{index}=[fi_cobstacle{index} fi_obstacle{index}(:,i-n)-f2*robot(:,i_unitc(ri2(1)))];
                end
            end
        end

        % the follow code tries to put the vertics of CB in order to
        % make it easy to draw the polygon
        mx=mean(fi_cobstacle{index}(1,:));
        my=mean(fi_cobstacle{index}(2,:));
        vec=fi_cobstacle{index}-[mx*ones(1,length(fi_cobstacle{index})); my*ones(1,length(fi_cobstacle{index}))];
        ang=atan2(vec(2,:),vec(1,:));
        ang=ang+2*pi*(ang<0);
        [nvec nindex]=sort(ang);
        fi_cobstacle{index}=fi_cobstacle{index}(:,nindex);
    end
    zgx_computeC=fi_cobstacle;

elseif length(theta)>1 % translation and rotation case

    %transaction matrix f, x in the world=f*x in the robot
    ntheta=length(theta);
    step=theta(2)-theta(1);
    p=robot;
    zgx_computeC=cell(ntheta,length(fi_obstacle));
    for ci=1:length(theta)
        fi_cobstacle=cell(length(fi_obstacle),1);
        f(:,:,ci)=[cos(theta(ci)) -sin(theta(ci)); sin(theta(ci)) cos(theta(ci))];
        [m n]=size(robot); % n represents the edge number of robot

        pw=f(:,:,ci)*p; % robot vertix in FW
        % matrix to compute robot configuration (robot center)
        %from a world position in fact it equals f.
        % x_robot_configuration in the world = position of edge
        % a in the world -f2* position of edge a in the robot coordinate
        f2=f(:,:,ci);
        % Now compute the normal for each edge of robot
        pw2=[pw(:,2:n),pw(:,1)];
        deltapw=pw2-pw; % vector of each edge
        phi=atan2(deltapw(2,:),deltapw(1,:)); % angle of each edge in FW
        phi=phi-1/2*pi; % angle of normal for each edge in FW
        phi=phi+2*pi*(phi<0); %convert to [0,2pi]
        minusphi=phi-pi; % minus normal
        minusphi=minusphi+2*pi*(minusphi<0); % convert to [0, 2pi]
        %%%%%%%%%%%%%%%%%%%%
        % compute CB for each obstacle
        for index=1:length(fi_obstacle)
            [mb nb]=size(fi_obstacle{index});
            ow2=[fi_obstacle{index}(:,2:nb), fi_obstacle{index}(:,1)];
            deltaow=ow2-fi_obstacle{index};
            phib=atan2(deltaow(2,:),deltaow(1,:)); % angle of each edge in FW
            phib=phib-1/2*pi; % angle of normal for each edge in FW
            phib=phib+2*pi*(phib<0); %convert to [0,2pi]
            unitc=[minusphi phib];
            [unitc i_unitc]=sort(unitc); % construct the unite circle
            %unitc=[untic unitc(1)]; % add the first normal to the end
            %i_unitc=[i_unitc i_unitc(1)] % add the first normal to the end
            for i=1:n % find possible tape A contact for robot
                ai=find(~(i_unitc-i)); % find the ith normal of robot in the array
                ai_i_unitc=i_unitc;
                ai_i_unitc(1:ai)=0;
                ri=find(ai_i_unitc>n);
                % there is one edge of obstacle after ai
                if (sum(ri)>0)
                    fi_cobstacle{index}=[fi_cobstacle{index} fi_obstacle{index}(:,i_unitc(ri(1))-n)-f2*robot(:,i)];
                else
                    ai_i_unitc=i_unitc;
                    ai_i_unitc(ai:length(i_unitc))=0;
                    ri2=find(ai_i_unitc>n);
                    % there is one edge of obstacle before ai
                    if (sum(ri2)>0)
                        fi_cobstacle{index}=[fi_cobstacle{index} fi_obstacle{index}(:,i_unitc(ri2(1))-n)-f2*robot(:,i)];
                    end
                end
            end

            for i=n+1:length(i_unitc) % find possible type B contact for robot
                bi=find(~(i_unitc-i)); % find the (i-n)th normal of obstacle in the array
                bi_i_unitc=i_unitc;
                bi_i_unitc(1:bi)=n+1;
                ri=find(bi_i_unitc<=n);
                % there is an edge of robot after bi
                if (sum(ri)>0)
                    fi_cobstacle{index}=[fi_cobstacle{index} fi_obstacle{index}(:,i-n)-f2*robot(:,i_unitc(ri(1)))];
                else
                    bi_i_unitc=i_unitc;
                    bi_i_unitc(bi:length(i_unitc))=n+1;
                    ri2=find(bi_i_unitc<=n);
                    % there is an edge of robot before bi
                    if (sum(ri2)>0)
                        fi_cobstacle{index}=[fi_cobstacle{index} fi_obstacle{index}(:,i-n)-f2*robot(:,i_unitc(ri2(1)))];
                    end
                end
            end

            % the follow code tries to put the vertics of CB in order to
            % make it easy to draw the polygon
            mx=mean(fi_cobstacle{index}(1,:));
            my=mean(fi_cobstacle{index}(2,:));
            vec=fi_cobstacle{index}-[mx*ones(1,length(fi_cobstacle{index})); my*ones(1,length(fi_cobstacle{index}))];
            ang=atan2(vec(2,:),vec(1,:));
            ang=ang+2*pi*(ang<0);
            [nvec nindex]=sort(ang);
            fi_cobstacle{index}=fi_cobstacle{index}(:,nindex);
        end
        zgx_computeC(ci,:)=fi_cobstacle;
    end
end
        
