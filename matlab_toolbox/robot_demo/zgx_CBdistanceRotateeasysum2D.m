%This function compute the distance and poisition between a configuration
%and the CB regions in 2 dimenstion.
% input: q=[x;y] are the position of a configuration.
% CB: CB regions comptued by the function zgx_computeCB3.m

function CB_distance2=zgx_CBdistanceRotateeasysum2D(q,robot,CB)
% robot=sensor;
% CB=CT;
for i=1:length(robot(1,:));
    lth(i)=norm(robot(:,i)); % distance from each vertix to the center
end
ltheta=max(lth); % find the highest distance of the robot vertix and use it as the weight of theta in distance

[mCB nCB]=size(CB); % size of CB
CB_distance=cell(mCB,nCB); % use to install distance to each CB
%CB_distance2=cell(1,1);
intheta=int16(q(3)/2/pi*mCB+1); % get the index number of theta
% first check whether the configuration is in the CB
flag=0;
% if q(1)==8 & q(2)==7
%     a=0;
% end
% inobstacle=0;
% for index=1:nCB
%     if inpolygon(q(1),q(2),[CB{intheta,index}(1,:) CB{intheta,index}(1,1)], [CB{intheta,index}(2,:) CB{intheta,index}(2,1)])
%         for i=1:mCB
%             for j=1:nCB
% 
%                 CB_distance{i,j}.distance=0;
%                 CB_distance{i,j}.position='';
%             end
%         end
%         inobstacle=1;
%         %flag=1;
%         %        index=nCB+1;
%     end
% end
if flag==0 % this configuration is not in the CB
    for itheta=1:mCB
        dtheta=abs(double(itheta-intheta))*ltheta; % compute the distance of theta
        for index=1:nCB %compute the distance in each layer
            lc=inf; %initial the temperay variable for distance choosen
            % first check wheather the perpendicular between point and
            % plane lines in the polygon. If so, this should be the small-
            % est one
            if inpolygon(q(1),q(2),[CB{itheta,index}(1,:), CB{itheta,index}(1,1)],[CB{itheta,index}(2,:), CB{itheta,index}(2,1)])
                lc=dtheta; % distance
                lp=[q(1); q(2); (itheta-1)*intheta]; % position
%                index=nCB+1;
            else % the perpendicular not in the polygon
                nCBi=length(CB{itheta,index}); % the number of edge of the obstalce
                iCBi=[1:nCBi, 1];
                for i=1:nCBi % compute the distance to the ith obstacle
                    li=norm([q(1:2)-CB{itheta,index}(:,i); dtheta]); % compute distance between points
                    if li<lc %if closer
                        lc=li; % current closest distance
                        lp=[CB{itheta,index}(:,i);(itheta-1)*intheta]; % current closest point
                    end
                    % compute the distance between each edge
                    [distance position]=zgx_distancepointtolinerotate(q,[CB{itheta,index}(:,i) CB{itheta,index}(:,iCBi(i+1))],dtheta);
                    % since the output position only include the positions, theta
                    % needs to be added to it.
                    position=[position; (itheta-1)*intheta];
                    % check whether the perpendicular is on the edge
                    if distance<lc & position(1)>=min(CB{itheta,index}(1,i), CB{itheta,index}(1,iCBi(i+1))) & position(1)<=max(CB{itheta,index}(1,i), CB{itheta,index}(1,iCBi(i+1))) & position(2)>=min(CB{itheta,index}(2,i), CB{itheta,index}(2,iCBi(i+1))) & position(2)<=max(CB{itheta,index}(2,i), CB{itheta,index}(2,iCBi(i+1)))
                        lc=distance;
                        lp=position;
                    end
                end
            end
             CB_distance{itheta,index}.distance=lc;
             CB_distance{itheta,index}.position=lp;
%             itheta
%             index
%             CB_distance{itheta,index}.distance
%             CB_distance{itheta,index}.position
        end
    end
end

%store the smallest distance to each c-obstacle
CB_distance2=cell(nCB,1);
for index=1:nCB
    temp=inf;
    tempindex=0;
    for thindex=1:mCB
        if CB_distance{thindex,index}.distance<temp
            temp=CB_distance{thindex,index}.distance;
            tempindex=thindex;
        end
    end
    CB_distance2{index,1}=CB_distance{tempindex,index};
end

%keyboard
