%   DrawParaPipe.M -- draws 3D obstacles 
%   January 31, 2012
%   =========================================================
%   Wenjie Lu and Ashleigh Swingler, LISC, Duke University

%   BEGINNING of FUNCTION
%   =========================

    function DrawParaPipe(polygon, height,color,edge_color)
    
    [m n]=size(polygon);
    fill3(polygon(1,:),polygon(2,:),ones(1,n)*height(1),color,'EdgeColor',edge_color);
    fill3(polygon(1,:),polygon(2,:),ones(1,n)*height(2),color,'EdgeColor',edge_color);
    
    for i=1:n
        x=[polygon(1,[i mod(i,n)+1]) polygon(1,[mod(i,n)+1 i])];
        y=[polygon(2,[i mod(i,n)+1]) polygon(2,[mod(i,n)+1 i])];
        z=[height(1) height(1) height(2) height(2)];
        fill3(x,y,z,color,'EdgeColor',edge_color);
    end
    
    end

