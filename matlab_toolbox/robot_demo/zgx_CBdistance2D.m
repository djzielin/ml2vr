%This function compute the distance and poisition between a configuration
%and the CB regions in 2 dimenstion.
% input: q=[x;y] are the position of a configuration.
% CB: CB regions comptued by the function zgx_computeCB3.m

function CB_distance=zgx_CBdistance2D(q,CB)
CB_distance=cell(length(CB),1);
for index=1:length(CB)
    % if the position in the polygon the distance is 0
    if inpolygon(q(1),q(2),[CB{index}(1,:) CB{index}(1,1)], [CB{index}(2,:) CB{index}(2,1)])
        %         CB_distance{index}.distance=0;
        %         CB_distance{index}.position='';
        vec2=q(1:2)-mean(CB{index},2);
        CB_distance{index}.distance=-norm(vec2);% the negative distance indicates the robot is in the obstacle
        CB_distance{index}.position=mean(CB{index},2);
        
        % if the position is outside the polgon
    else
        lc=inf;
        lCBi=length(CB{index});
        iCBi=[1:lCBi, 1];
        for i=1:lCBi
            li=norm(q(1:2)-CB{index}(:,i)); % compute distance between points
            if li<lc
                lc=li; % current closest distance
                lp=CB{index}(:,i); % current closest point
            end
            [distance position]=zgx_distancepointtoline(q,[CB{index}(:,i) CB{index}(:,iCBi(i+1))]);
            % check whether the perpendicular is on the edge
            if distance<lc & position(1)>=min(CB{index}(1,i), CB{index}(1,iCBi(i+1))) & position(1)<=max(CB{index}(1,i), CB{index}(1,iCBi(i+1))) & position(2)>=min(CB{index}(2,i), CB{index}(2,iCBi(i+1))) & position(2)<=max(CB{index}(2,i), CB{index}(2,iCBi(i+1)))
                lc=distance;
                lp=position;
            end
        end
        if lc==0
            lp=mean(CB{index},2);
        end
        CB_distance{index}.distance=lc;
        CB_distance{index}.position=lp;
        if lc==0
            lp=mean(CB{index},2);
        end
    end
end

