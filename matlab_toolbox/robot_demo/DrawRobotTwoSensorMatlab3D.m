function DrawRobotTwoSensorMatlab3D(q,theta,robot,Robot_height,hr,lr,C_Robot,C_S_L,C_S_H)
%draw the 3D robot 
%define the FOV and robot
robot(1,:)=robot(1,:)/4*3;
Max_Angle=pi/8*3;
lr=0;%since the sensor with low accuracy is hidden
%Robot_height=2;
%C_S_H='r';
f=[cos(theta) -sin(theta) q(1); sin(theta) cos(theta) q(2); 0 0 1];  % transfer function of the sensor
qw=double(f)*[robot; double(ones(1,length(robot)))]; % the position of robot corners in the world coordinates

thetaii=0:pi/20:pi;
yl=lr.*sin(thetaii)+abs(robot(2,1));
xl=lr.*cos(thetaii);
%sw1=double(f)*[xh; yh; ones(1,length(xh))]; % the position of the high accuracy sensor in the world coordinates
sw2=double(f)*[xl; yl; ones(1,length(xl))]; % the position of the low accuracy sensor in the world coordinates
%fill(sw2(1,:),sw2(2,:),cs2) % color the low accuracy sensor
% Z plot3([sw2(1,:) sw2(1,1)],[sw2(2,:) sw2(2,1)],Robot_height*ones(1,length(sw2(1,:))+1),C_S_L) % color the low accuracy sensor
hold on
%fill(qw(1,:),qw(2,:),cr) % color the robot

%fill(sw1(1,:),sw1(2,:),cs1) % color the high accuracy sensor
%plot([sw1(1,:) sw1(1,1)],[sw1(2,:) sw1(2,1)],cs1) % color the high accuracy sensor

for i=1:10
    pitch=2*Max_Angle/9*(i-1)-Max_Angle;
    theta0=acos(cos(Max_Angle)/cos(pitch));
    thetai=-theta0:theta0/10:theta0;
    yh=hr.*cos(thetai)*cos(pitch)+abs(robot(2,1));
    yh(end+1)=abs(robot(2,1));
    xh=hr.*sin(thetai);
    xh(end+1)=0;
    zh=hr.*cos(thetai)*sin(pitch)+Robot_height;
    zh(end+1)=Robot_height;
    sw1=double(f)*[xh; yh; ones(1,length(xh))];
    %Z plot3([sw1(1,:) sw1(1,1)],[sw1(2,:) sw1(2,1)],[zh zh(1)],C_S_H);
end

DrawParaPipe(qw,[Robot_height/3 Robot_height],C_Robot,'black'); %this causes new nvidia driver to crash. why?
%draw two wheels
[X Y Z0]=cylinder(1); %adjust the raduis of wheel
Z=Z0/2+0.25;%adjust the width of wheel
ZZ=Z*cos(theta)-Y*sin(theta)+q(1);
YY=Z*sin(theta)+Y*cos(theta)+q(2);
surf(ZZ,YY,X+1);
rim_color='cyan';
fill3(ZZ(1,:),YY(1,:),X(1,:)+1,rim_color);
fill3(ZZ(2,:),YY(2,:),X(2,:)+1,rim_color);
Z=Z0/2-0.75;
ZZ=Z*cos(theta)-Y*sin(theta)+q(1);
YY=Z*sin(theta)+Y*cos(theta)+q(2);
surf(ZZ,YY,X+1);

fill3(ZZ(1,:),YY(1,:),X(1,:)+1,rim_color);
fill3(ZZ(2,:),YY(2,:),X(2,:)+1,rim_color);

end