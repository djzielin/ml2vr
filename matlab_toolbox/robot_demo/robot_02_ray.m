%   POTENTIAL_EXAMPLE.M -- demo for the potential method
%   July 28, 2012
%   =========================================================
%   Wenjie Lu, LISC, Duke University
%
%   FUNCTIONS and FILES used by POTENTIAL_EXAMPLE
%           create_workspace.m          create workspace with obstacles and
%                                       a goal
%           create_robot.m              create a robot with a initial
%                                       position q
%           Potential_force.m           calculate the potential force,
%                                       which will be used in updating q
%           draw_workspace.m            draw the workspace to show the
%                                       simulation
%           DrawRobotTwoSensorMatlab3D  draw robot with the configuration
%                                       provided by robot and q
%
%   DEFINITION of VARIABLES
%           hr                =   radius of high difinition sensor
%           lr                =   radius of low difinition sensor
%           dt                =   simulated time step
%

%   BEGINNING of MAIN PROGRAM
%   =========================


addpath('..');
close all
clear

%initialize variables

create_workspace;
create_robot;

hr=0;
lr=0;
dt=0.05;

frames_spent=0;
time_spent=0;

do_vr=true;

%% for VR
if do_vr

        
    vr_config;
    vi=vr_interface(ServerIP,ServerPort);
    vi.set_return_type('matrix4');
    
    
    ma = [1 0 0 0;
          0 0 1 0;
          0 1 0 0;
          0 0 0 1];
    mt = makehgtform('translate',[-2.5 2.25 -4]);
    ms = makehgtform('scale',0.1);
    mf=mt*ms*ma;
   
    vi.set_transform_matrix(mf);
    
    is_dragging=false;
    drag_diff=[0 0 0];
    
    vc=vr_cursor(vi);
    vc.setup_ray_preset();    
    
end
%%
tic
fr_a=0;
%while norm(q(1:2)-goal)>2
while 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %code added to make the obstacle move
    %move obstacle 1
    
    %    obs_index=3;
    %    obs_goal=[20 25]';
    %    if norm(get_obstacle_pos(fi_obstacle,3)-obs_goal)>1;
    %        pos=get_obstacle_pos(fi_obstacle,obs_index)+...
    %            (obs_goal-get_obstacle_pos(fi_obstacle,obs_index))/norm(obs_goal-get_obstacle_pos(fi_obstacle,obs_index))*0.01;
    %    fi_obstacle=set_obstacle_pos(fi_obstacle,obs_index,pos);
    %
    %    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% VR Interaction Code
    
    if do_vr
        [event,button,matrix4]=vi.get_button_event();
        
        mFixed=matrix4*ma; %post multiply to account for coordinate change
        fdir=vec4_to_vec3(mFixed*[0 -1 0 1]')-vec4_to_vec3(mFixed*[0 0 0 1]');
        fdir=(fdir./norm(fdir))'; %normalize
        pos3=mFixed([13 14 15]); %%extract pos3
              
        tIndex=num_obst+1;
        all_pos=zeros(tIndex,3);
        for f=1:num_obst
            all_pos(f,:)=[get_obstacle_pos(fi_obstacle,f); 0];
        end
        
        [Tx,Ty,Tz]=os.getPos();
        all_pos(tIndex,:)=[Tx Ty Tz]; % add target to our array of positions
        
        Q2=pos3+fdir;
        Q1=pos3;
        all_dist=eye(tIndex,1);
        
        for f=1:tIndex
          %%http://www.mathworks.com/matlabcentral/newsreader/view_thread/164048

          P=all_pos(f,:);   
          d = norm(cross(Q2-Q1,P-Q1));
          all_dist(f)=d;
        end
          
        pos3(3)=0; %restrain to floor
               
        [closest_dist,closest_index]=min(all_dist); % get closest object
        
        if is_dragging==false % touch processing
            is_touching_obstacle=false;
            touched_object=-1;
            
            if(closest_dist<3)
                is_touching_obstacle=true;
                touched_object=closest_index;
            end
            
            for f=1:tIndex
                if f==touched_object
                    if f==tIndex
                        os.setColor(1,1,0);
                    else
                        fi_obstacle_color=set_color(fi_obstacle_color,touched_object,'yellow');
                    end
                else
                    if f==tIndex
                        os.setColor(1,0,0);
                    else
                        fi_obstacle_color=set_color(fi_obstacle_color,f,'blue');
                    end
                end
            end
            
        end
        
        if event==1 && button==5 && is_touching_obstacle%% user clicked yellow button
            is_dragging=true;
            stored_pos=all_pos(touched_object,:);
            
            stored_transform=makehgtform('translate',stored_pos);
            drag_diff=inv(mFixed);
            
            if touched_object==tIndex
                os.setColor(1,0,1);
            else
                fi_obstacle_color=set_color(fi_obstacle_color,touched_object,'magenta');
            end
            'drag starting'
        end
        
        if is_dragging
            %computed_pos=pos3+drag_diff;
            matrix4_current=mFixed*drag_diff*stored_transform;
            computed_pos=matrix4_current([13 14 15]);
            computed_pos(3)=0; % restrain movement
            obstacle_pos=computed_pos;%+stored_pos;
            
            if touched_object==tIndex
                os.setPos(obstacle_pos(1),obstacle_pos(2),0);
                goal=obstacle_pos(1:2)';
            else
                fi_obstacle=set_obstacle_pos(fi_obstacle,touched_object,obstacle_pos(1:2));
            end
        end
        
        if is_dragging==true && event==2 && button==5 %% user has released object
            is_dragging=false;
            'drag ending'
        end
    end
    
    %%
    
    force=Potential_force(q, robot, fi_obstacle, goal);
    q(1:2)=q(1:2)+v(1)*[cos(q(3)) sin(q(3))]'*dt;%update configuration
    
    delta_theta=atan2(force(2),force(1))-q(3);
    if delta_theta>pi
        delta_theta=delta_theta-2*pi;
    elseif delta_theta<-pi
        delta_theta=delta_theta+2*pi;
    end
    
    
    w_max=1;%constraint on angular speed
    if delta_theta>w_max
        delta_theta=w_max;
    elseif delta_theta<-w_max
        delta_theta=-w_max;
    end
    
    q(3)=q(3)+0.5*(delta_theta)*dt;%update heading
    if q(3)>pi
        q(3)=q(3)-2*pi;
    elseif q(3)<-pi
        q(3)=q(3)+2*pi;
    end
    
    
    max_delta_v=1;
    delta_v=[cos(q(3)) sin(q(3))]*force-v(1);%constraint on linear accerlation
    if delta_v>max_delta_v
        delta_v=max_delta_v;
    elseif delta_v<-max_delta_v
        delta_v=-max_delta_v;
    end
    
    v(1)=v(1)+(delta_v)*dt;
    v_max=2;%constraint on linear speed
    if v(1)>v_max
        v(1)=v_max;
    elseif v(1)<-v_max
        v(1)=-v_max;
    end
    hold off
    draw_workspace;
    DrawRobotTwoSensorMatlab3D(q(1:2),q(3)-pi/2,robot,Robot_height,hr,lr,[0.58 0.39 0.39],'y','r');
    %%%%%%%%
    %debug
    %plot([0 force(1)]/norm(force)*5+q(1),[0 force(2) ]/norm(force)*5+q(2),'b');
    %view(0,90)
    %force=Potential_force(q, robot, fi_obstacle, goal);
    drawnow
    
    frames_spent=frames_spent+1;
    
    if frames_spent==30
        t1=toc;
        fps=frames_spent/t1
        tic;
        frames_spent=0;
        fr_a=fr_a+1;
        if fr_a>1
            results(fr_a-1)=fps;
        end
    end
end

avg=sum(results)/length(results)



