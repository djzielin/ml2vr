%   GET_OBSTACLE_POS.M -- demo for the potential method
%   AUG 14, 2012
%   =========================================================
%   Wenjie Lu, LISC, Duke University
%
%   FUNCTIONS and FILES used by POTENTIAL_EXAMPLE
%           
%   DEFINITION of VARIABLES
%           obs_index         =   obstacle index
%           pos               =   new position
%           dt                =   simulated time step
%

%   BEGINNING of MAIN PROGRAM
%   =========================

function pos=get_obstacle_pos(fi_obstacle,obs_index)
pos=mean(fi_obstacle{obs_index},2);
end

