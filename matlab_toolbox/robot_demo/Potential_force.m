% 	POTENTIAL_FORCE.M -- return potential force by the obstacles and the goal 
%   July 28, 2012
%   =========================================================
%   Wenjie Lu, LISC, Duke University
%
%   FUNCTIONS and FILES used by CREATE_WORKSPACE 
%           zgx_computeCB3.m                      compute the C-obstacle
%           zgx_CBdistanceRotateeasysum2D.m       obtain the distance from
%                                                 q and C-obstalce and nearest point in C-obstacle
%
%   DEFINITION of VARIABLES
%           eta1              =   weight of atractive force 
%           eta2              =   weight of repulsive force
%           rho               =   threshold of repulsive force
%           
    
%   BEGINNING of MAIN PROGRAM
%   =========================


function  force= Potential_force(q, robot, fi_obstacle, goal)

%initialize variables
eta1=0.06;
eta2=100;
rho=3;
force=eta1*(goal-q(1:2));% force from goal
n=size(fi_obstacle);
theta=q(3);
for i=1:n
    CB=zgx_computeCB3(robot,theta,fi_obstacle(i));
%     hold on
%     plot(CB{1}(1,[1:end 1]),CB{1}(2,[1:end 1]),'r');
%     hold off;
    %CB_distance=zgx_CBdistanceRotateeasysum2D(q,robot,CB);
    CB_distance=zgx_CBdistance2D(q,CB);
    distance=CB_distance{1,1}.distance;
    position=CB_distance{1,1}.position;
%     hold on;
%     scatter(position(1),position(2),'b');
%     hold off;
    if distance>rho
    elseif distance<=0
        distance=1e-5;
        force=force+eta2*(1/distance-1/rho)/distance^3*(q(1:2)-double(position(1:2)));
    else
        force=force+eta2*(1/distance-1/rho)/distance^3*(q(1:2)-double(position(1:2)));
    end
end

