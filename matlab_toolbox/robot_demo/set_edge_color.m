%   SET_EDGE_COLOR.M -- demo for the potential method
%   JAN 21, 2013
%   =========================================================
%   Wenjie Lu, LISC, Duke University
%
%   FUNCTIONS and FILES used by POTENTIAL_EXAMPLE
%           
%   DEFINITION of VARIABLES
%           obs_index         =   obstacle index
%           color             =   color
%

%   BEGINNING of MAIN PROGRAM
%   =========================

function fi_obstacle_edge_color=set_edge_color(fi_obstacle_edge_color,obs_index,edge_color)
fi_obstacle_edge_color{obs_index}=edge_color;
end