% functions to set framework parameters of the local slice

classdef vr_local_slice < handle
    properties (Access=private,Hidden=true)
        vr_inter;
    end
    
    methods (Access=public)
        
        function obj=vr_local_slice(vi) %% constructor
            obj.vr_inter=vi;
        end
        
        function delete(obj) %% destructor
        end
        
        function set_begin_marker(obj, slice_num)
            fill3([1 1 1],[0 0 0],[slice_num slice_num slice_num],'b','EdgeColor','none');
        end
        
        function set_end_marker(obj, slice_num)
            fill3([1 1 1],[1 1 1],[slice_num slice_num slice_num],'b','EdgeColor','none');
        end
        
        function enable(obj, value)
            
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(11,'uchar'); %% local slice params
            obj.vr_inter.send_data(0,'uchar'); %%enable
            
            if value==true
                obj.vr_inter.send_data(1,'uchar'); %%true
            else
                obj.vr_inter.send_data(0,'uchar');
            end
        end
        
    end
end

