%TODO
%add code to dynamically change radius
%can we combine the unitX,unitY,unitZ into one variable?
%or just use transform node once intercept/renderer supports it
classdef vr_cube < handle
    properties
        unitX;
        unitY;
        unitZ;
        
        baseX;
        baseY;
        baseZ;
        
        currentX;
        currentY;
        currentZ;
        
        basePos;
        cX; cY; cZ;
        fhandle;
        radius;
        color;
        
        f1x; f1y; f1z;
        f2x; f2y; f2z;
        f3x; f3y; f3z;
        f4x; f4y; f4z;
        f5x; f5y; f5z;
        f6x; f6y; f6z;
        
        p1x; p1y; p1z;
        p2x; p2y; p2z;
        p3x; p3y; p3z;
        p4x; p4y; p4z;
        
        current_transform;
        current_transform_inv;
        
    end
    methods
        function obj=vr_cube(radius)
            if nargin <1; radius=1;      end;
           
            obj.radius=radius;
            
            obj.unitX=[1 1 -1 -1 1 1 -1 -1];
            obj.unitY=[1 -1 1 -1 1 -1 1 -1];
            obj.unitZ=[-1 -1 -1 -1 1 1 1 1];
            
            obj.baseX=obj.unitX*radius;
            obj.baseY=obj.unitY*radius;
            obj.baseZ=obj.unitZ*radius;
            
            obj.basePos=[obj.baseX(:) obj.baseY(:) obj.baseZ(:)];
            obj.basePos(:,4)=1;
            
            obj.setPos(0,0,0);
            obj.setColor('blue');
        end
        
        function setColor(obj, color)
            obj.color=color;
            
        end
        
        function setMatrix4(obj, m)
            res=obj.basePos*m';
            obj.currentX=reshape(res(:,1),size(obj.baseX));
            obj.currentY=reshape(res(:,2),size(obj.baseX));
            obj.currentZ=reshape(res(:,3),size(obj.baseX));
            
            %TODO extract cx,cy,cz
            
            obj.compute_faces();
            
            obj.current_transform=m;     
            obj.current_transform_inv=inv(obj.current_transform);
        end
        
        function [X,Y,Z]=getPos(obj) %TODO make work for matrix4
            X=obj.cX;
            Y=obj.cY;
            Z=obj.cZ;
        end
        
        function setPos(obj,X,Y,Z)
            obj.currentX=obj.baseX+X;
            obj.currentY=obj.baseY+Y;
            obj.currentZ=obj.baseZ+Z;
            
            obj.cX=X;
            obj.cY=Y;
            obj.cZ=Z;
            
            obj.compute_faces();
            
            obj.current_transform=makehgtform('translate',[obj.cX obj.cY obj.cZ]);
            obj.current_transform_inv=inv(obj.current_transform);
        end
        
        function render(obj)
            fill3(obj.f1x,obj.f1y,obj.f1z,obj.color);
            fill3(obj.f2x,obj.f2y,obj.f2z,obj.color);
            fill3(obj.f3x,obj.f3y,obj.f3z,obj.color);
            fill3(obj.f4x,obj.f4y,obj.f4z,obj.color);
            fill3(obj.f5x,obj.f5y,obj.f5z,obj.color);
            fill3(obj.f6x,obj.f6y,obj.f6z,obj.color);
        end
        
        function render_wireframe(obj)
            h=plot3(obj.p1x,obj.p1y,obj.p1z,'Color',obj.color);
            set(h,'Parent',hgtransform)     %% HACK TO FORCE INTO 3D
            h2=plot3(obj.p2x,obj.p2y,obj.p2z,'Color',obj.color);
            set(h2,'Parent',hgtransform)     %% HACK TO FORCE INTO 3D
            h3=plot3(obj.p3x,obj.p3y,obj.p3z,'Color',obj.color);
            set(h3,'Parent',hgtransform)     %% HACK TO FORCE INTO 3D
            h4=plot3(obj.p4x,obj.p4y,obj.p4z,'Color',obj.color);
            set(h4,'Parent',hgtransform)     %% HACK TO FORCE INTO 3D
        end        
        
        function translate(obj,X,Y,Z)
            obj.currentX=obj.currentX+X;
            obj.currentY=obj.currentY+Y;
            obj.currentZ=obj.currentZ+Z;
            
            obj.cX=obj.cX+X;
            obj.cY=obj.cY+Y;
            obj.cZ=obj.cZ+Z;
            
            obj.compute_faces();
            
            obj.current_transform=makehgtform('translate',[obj.cX obj.cY obj.cZ]);
            obj.current_transform_inv=inv(obj.current_transform);
        end
        
        function [pos3]=get_corner(obj, which)
            pos3=[obj.currentX(which) obj.currentY(which) obj.currentZ(which)];        
        end
        
        function [inside]=is_point_inside(obj, pos3)
            
            in_our_reference=obj.current_transform_inv* makehgtform('translate',[pos3(1) pos3(2) pos3(3)]);
            
            rX=in_our_reference(13);
            rY=in_our_reference(14);
            rZ=in_our_reference(15);
            
            if(rX>obj.radius || rX<-obj.radius)
                inside=false;
                return;
            end
            
            if(rY>obj.radius || rY<-obj.radius)
                inside=false;
                return;
            end
            
            if(rZ>obj.radius || rZ<-obj.radius)
                inside=false;
                return;
            end
            
            inside=true;
        end

        
    end
    methods (Access=private,Hidden=true)
        
        function compute_faces(obj)
            obj.f1x=[obj.currentX(5) obj.currentX(6) obj.currentX(8) obj.currentX(7)];
            obj.f1y=[obj.currentY(5) obj.currentY(6) obj.currentY(8) obj.currentY(7)];
            obj.f1z=[obj.currentZ(5) obj.currentZ(6) obj.currentZ(8) obj.currentZ(7)];
            
            obj.f2x=[obj.currentX(1) obj.currentX(2) obj.currentX(6) obj.currentX(5)];
            obj.f2y=[obj.currentY(1) obj.currentY(2) obj.currentY(6) obj.currentY(5)];
            obj.f2z=[obj.currentZ(1) obj.currentZ(2) obj.currentZ(6) obj.currentZ(5)];
            
            obj.f3x=[obj.currentX(7) obj.currentX(8) obj.currentX(4) obj.currentX(3)];
            obj.f3y=[obj.currentY(7) obj.currentY(8) obj.currentY(4) obj.currentY(3)];
            obj.f3z=[obj.currentZ(7) obj.currentZ(8) obj.currentZ(4) obj.currentZ(3)];
            
            obj.f4x=[obj.currentX(3) obj.currentX(4) obj.currentX(2) obj.currentX(1)];
            obj.f4y=[obj.currentY(3) obj.currentY(4) obj.currentY(2) obj.currentY(1)];
            obj.f4z=[obj.currentZ(3) obj.currentZ(4) obj.currentZ(2) obj.currentZ(1)];
            
            obj.f5x=[obj.currentX(1) obj.currentX(5) obj.currentX(7) obj.currentX(3)];
            obj.f5y=[obj.currentY(1) obj.currentY(5) obj.currentY(7) obj.currentY(3)];
            obj.f5z=[obj.currentZ(1) obj.currentZ(5) obj.currentZ(7) obj.currentZ(3)];
            
            obj.f6x=[obj.currentX(4) obj.currentX(8) obj.currentX(6) obj.currentX(2)];
            obj.f6y=[obj.currentY(4) obj.currentY(8) obj.currentY(6) obj.currentY(2)];
            obj.f6z=[obj.currentZ(4) obj.currentZ(8) obj.currentZ(6) obj.currentZ(2)];
            
            obj.p1x=[obj.currentX(5) obj.currentX(6) obj.currentX(8) obj.currentX(7) obj.currentX(5) obj.currentX(1)];
            obj.p1y=[obj.currentY(5) obj.currentY(6) obj.currentY(8) obj.currentY(7) obj.currentY(5) obj.currentY(1)];
            obj.p1z=[obj.currentZ(5) obj.currentZ(6) obj.currentZ(8) obj.currentZ(7) obj.currentZ(5) obj.currentZ(1)];
            
            obj.p2x=[obj.currentX(3) obj.currentX(4) obj.currentX(2) obj.currentX(1) obj.currentX(3) obj.currentX(7)];
            obj.p2y=[obj.currentY(3) obj.currentY(4) obj.currentY(2) obj.currentY(1) obj.currentY(3) obj.currentY(7)];
            obj.p2z=[obj.currentZ(3) obj.currentZ(4) obj.currentZ(2) obj.currentZ(1) obj.currentZ(3) obj.currentZ(7)];
            
            obj.p3x=[obj.currentX(2) obj.currentX(6)];
            obj.p3y=[obj.currentY(2) obj.currentY(6)];
            obj.p3z=[obj.currentZ(2) obj.currentZ(6)];
            
            obj.p4x=[obj.currentX(4) obj.currentX(8)];
            obj.p4y=[obj.currentY(4) obj.currentY(8)];    
            obj.p4z=[obj.currentZ(4) obj.currentZ(8)];
            
        end
    end
end

