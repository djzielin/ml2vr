
function vr_cam(radius)
  a=axes;
  axis(a,[-radius radius 0 radius*2.0 -radius radius]);

  set(a,'Xgrid','on');
  set(a,'Ygrid','on');
  set(a,'Zgrid','on');
  
  set(a,'Xtick',-radius:(radius/5.0):radius);
  set(a,'Ytick',0:(radius/5.0):(radius*2));
  set(a,'Ztick',-radius:(radius/5.0):radius);

  camup([0 1 0]);
  camtarget([0 radius 0]);
  campos([radius*4 radius*4 radius*10]);

  zlabh = get(a,'ZLabel');
  zlabh_pos=get(zlabh,'Position');
  set(zlabh,'Position', [-radius*2 0 radius*2]);
end
