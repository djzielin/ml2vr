addpath('..');
vr_config
vi=vr_interface(ServerIP,ServerPort); 

vi.set_return_type('matrix4');
vhd=vr_handoff_drag(vi);
vhd.setup_preset(VRInteractionButton);

close all;
figure('Renderer','OpenGL');

num_spheres=100
is_num=45;
sphere_size=0.1;
selection_size=0.2;

for f= 1:num_spheres
    x=mod(f-1,10)*0.75-4;
    y=floor((f-1)/10.0)*0.75+2;
    
    os=vr_sphere(sphere_size);
    os.setPos(x,y,0);
    mt = makehgtform('translate',[x y 0]);
    os.setMatrix4(mt);
    
    if f==is_num
        os.setColor(0,1,0);
        matrix4_current=mt;
        is=os;
    else
        os.setColor(1,1,1);
    end
    
    SphereArray(f)=os;
end

is_dragging=false;
do_redraw=true;
do_fancy=true;
old_d=100;
is_touching=false;

 

   
   
frame=0; 

tic;

while 1
    [event,button,matrix4]=vi.get_button_event();
    
    if is_dragging && do_fancy==false
        matrix4_current=matrix4*drag_diff*stored_transform;
        is.setMatrix4(matrix4_current);
        do_redraw=true;
    end
    
    if is_touching && event==1 && button==VRInteractionButton %% user clicked
        is_dragging=true;
        stored_transform=matrix4_current;
        drag_diff=inv(matrix4);
        is.setColor(1,0,0);
        fprintf('started drag at: %f\n',cputime);
        %matrix4([13 14 15])
        %drag_diff([13 14 15])
        do_redraw=true;
    end
    
    if is_dragging && event==2 && button==VRInteractionButton %% user has released object
        is_dragging=false;
        matrix4_current=matrix4*drag_diff*stored_transform;
        is.setMatrix4(matrix4_current);
        is.setColor(0,0,1);
        fprintf('stopped drag at: %f\n',cputime);
        %matrix4([13 14 15])        
        %testf=matrix4*drag_diff;
        %testf([13 14 15])
        do_redraw=true;
    end
    
    if is_dragging==false
        wand_pos=matrix4([13 14 15]); %%extract pos3
        sphere_pos=matrix4_current([13 14 15]);
        
        pinput=[sphere_pos;wand_pos];
        d=pdist(pinput);
        
        if(d<=selection_size && old_d>selection_size) %% touch begining
            is.setColor(0,0,1);
            is_touching=true;
            fprintf('started touching at: %f\n',cputime);
            do_redraw=true;
        end
        if(d>selection_size && old_d<=selection_size) %% touch ending
            is.setColor(0,1,0);
            is_touching=false;
            fprintf('stopped touching at: %f\n',cputime);
            do_redraw=true;
        end
    end   
    
    if do_redraw
        clf;
        vr_cam(5);
        hold on;
        
        for f=1:num_spheres
            if f~=is_num
                SphereArray(f).render();
            else
                if is_dragging==true && do_fancy
                    vhd.set_marker();
                end
                SphereArray(f).render();
                if is_dragging==true && do_fancy
                    vhd.set_marker();
                end
            end
        end;
        
        drawnow;
        do_redraw=false;  
    end
    
    old_d=d;
    
    if vi.is_q_pressed()==true
            break;
    end
    
    frame=frame+1;
    
    if(frame==20)
       time_to_run=toc;
       fps=frame/time_to_run;
       fprintf('fps: %f\n',fps);
       frame=0;
       tic;
   end
end

delete(vi); %% close the connection