%viz_02_surf.m - djzielin
%show the demo surf (from the MATLAB documentation)
%user can modify surface parameters with joystick x axis
%use button 5 for virtual hand interaction

addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort);
vi.set_return_type('pos3');

close all;
figure('Renderer','OpenGL');

do_redraw=true;

mz = [1 0 0 0; 0 1 0 0; 0 0 -1 0; 0 0 0 1]; % z flip
m1 = makehgtform('translate',[0 3 2]);
m2 = makehgtform('xrotate',-pi/2.0);
m3 = makehgtform('scale',1);
mf=mz*m1*m2*m3;
vi.set_transform_matrix(mf);

vs=vr_slice(vi,0);
vs.setup_ortho_xz(0); 
vs.set_global(false);

vls=vr_local_slice(vi);
vls.enable(true);

vvh=vr_virtual_hand(vi);
vvh.enable(true);                      
vvh.set_hand_button(VRInteractionButton);

[X,Y] = meshgrid(-1.5:.05:1.5);
[X2,Y2] = meshgrid(-1.5:.05:1.5);
do_redraw=true;

scalef=10;

num_spheres=10;
sphere_size=0.25;

for f= 1:num_spheres
    radians=2*pi*f/num_spheres;
    x=sin(radians)*1;
    y=cos(radians)*1;
    
    os=vr_sphere(sphere_size);
    mt = makehgtform('translate',[x y 0]);
    os.setMatrix4(mt);
    os.setColor(1,1,1);
    SphereArray(f)=os;
end

inc_amount=0.05;

while 1
    scalef=scalef+inc_amount;
    if(scalef<7)
        inc_amount=0.05;
    end
    if(scalef>10)
        inc_amount=-0.05;
    end
    
    do_redraw=true;
    
    if do_redraw
        R = sqrt((X*scalef).^2 + (Y*scalef).^2) + eps;
        Z = sin(R)./R * 1.5;
        
        clf;
        axis equal;
        view([2,3,2]);
        
        hold on;
        for f=1:num_spheres
            SphereArray(f).render();
        end
        
        vls.set_begin_marker(0);
        surf(X,Y,Z,'FaceColor','interp','EdgeColor','none');
        vls.set_end_marker(0);
        
        drawnow;
        do_redraw=false;
    end
    
    if vi.is_q_pressed()==true
        break;
    end
end
