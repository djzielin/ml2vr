% functions to set framework parameters of the hand off dragging

classdef vr_handoff_drag < handle
    properties (Access=private,Hidden=true)
        vr_inter;
    end
    
    methods (Access=public)
        
        function obj=vr_handoff_drag(vi) %% constructor
            obj.vr_inter=vi;
        end
        
        function delete(obj) %% destructor
        end
        
        function set_marker(obj)
            fill3([0 0 0],[0 0 0],[0 0 0],'b','EdgeColor','none');
        end
        
        function setup_preset(obj, button)
            obj.set_constrain('');
            obj.set_trigger_type(1);
            obj.set_trigger_button(button);
            obj.set_release_type(2);
            obj.set_release_button(button);
            obj.set_enable(true);
        end
        
        function set_enable(obj, value)
            
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(9,'uchar'); %% hand off drag
            obj.vr_inter.send_data(0,'uchar'); %%enable
            
            if value==true
                obj.vr_inter.send_data(1,'uchar'); %%true
            else
                obj.vr_inter.send_data(0,'uchar');
            end
        end
        
        function set_constrain(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(8,'uchar'); %% size
            obj.vr_inter.send_data(9,'uchar');
            obj.vr_inter.send_data(1,'uchar'); %% set constraint
            
            x_constrain=0;
            y_constrain=0;
            z_constrain=0;
            h_constrain=0;
            p_constrain=0;
            r_constrain=0;
            
            if ~isempty(strfind(value,'X'))
                x_constrain=1;
            end
            if ~isempty(strfind(value,'Y'))
                y_constrain=1;
            end
            if ~isempty(strfind(value,'Z'))
                z_constrain=1;
            end
            if ~isempty(strfind(value,'H'))
                h_constrain=1;
            end
            if ~isempty(strfind(value,'P'))
                p_constrain=1;
            end
            if ~isempty(strfind(value,'R'))
                r_constrain=1;
            end
            
            obj.vr_inter.send_data(x_constrain,'uchar');
            obj.vr_inter.send_data(y_constrain,'uchar');
            obj.vr_inter.send_data(z_constrain,'uchar');
            obj.vr_inter.send_data(h_constrain,'uchar');
            obj.vr_inter.send_data(p_constrain,'uchar');
            obj.vr_inter.send_data(r_constrain,'uchar');
        end
        
        
        function set_trigger_type(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(9,'uchar'); %% hand off drag
            obj.vr_inter.send_data(2,'uchar');
            obj.vr_inter.send_data(value,'uchar'); 
        end
        
        function set_trigger_button(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(9,'uchar'); %% hand off drag
            obj.vr_inter.send_data(3,'uchar');
            obj.vr_inter.send_data(value,'uchar'); 
        end
        
        function set_release_type(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(9,'uchar'); %% hand off drag
            obj.vr_inter.send_data(4,'uchar');
            obj.vr_inter.send_data(value,'uchar'); 
        end
        
        function set_release_button(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(9,'uchar'); %% hand off drag
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(value,'uchar'); 
        end
    end
end

