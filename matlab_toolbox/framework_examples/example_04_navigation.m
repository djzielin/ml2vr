%example_04_navigation.m - djzielin

addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort); 

vn=vr_navigation(vi);       
vn.setup_fly_preset(1,10);

close all;
figure('Renderer','OpenGL');

vs=vr_sphere(1);
vs.setPos(0,3,0);
vs.setColor(0,1,0);

fmin=-4; %for floor
fmax=4;

fx=[fmin fmin fmax fmax];
fy=[0 0 0 0];
fz=[fmin fmax fmax fmin ];

clf;
vr_cam(5);
hold on;

vs.render();
fill3(fx,fy,fz,'black'); %%render the floor

drawnow;
