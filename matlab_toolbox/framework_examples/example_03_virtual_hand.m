%example_03_virtual_hand.m - djzielin

addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort); 

vvh=vr_virtual_hand(vi);
        
vvh.enable(true);                      
vvh.set_hand_button(VRInteractionButton);
vvh.set_hand_constraints(''); %TODO: test other constraints
    
close all;
figure('Renderer','OpenGL');
os=vr_sphere();
os.render();
drawnow;            