%example_02_cursor.m - djzielin

addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort); 

vc=vr_cursor(vi);
        
vc.set_cursor_color(0.0,1.0,0.0);
vc.set_cursor_model('pyramid', 100, 0.1);
vc.set_push_forward_amount(0.25);
vc.set_position_at_tip(false);
vc.enable(true);


while 1   %to test position at tip
   [event,button,pos3]=vi.get_button_event();
   fprintf('pos of wand: %f %f %f\n',pos3(1),pos3(2),pos3(3));   
end
