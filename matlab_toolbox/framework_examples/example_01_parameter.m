%example_01_parameter.m - djzielin

addpath('..');

close all;
figure('Renderer','OpenGL');

vr_config;
vi=vr_interface(ServerIP,ServerPort); 

vp=vr_parameters(vi);

vp.set_background_color(1.0,0.0,0.0);
vp.set_joystick_threshold(0.1);
vp.set_wand_sensor_id(1);

fprintf('finished sending parameter requests\n');

while 1 %to test set threshold
    [event,axis,pos3,val]=vi.get_joystick_event();
    
    if event == 1
        if axis == 0
            fprintf('axis 0 value: %f\n',val);        
        end
        
        if axis == 1
            fprintf('axis 1 value: %f\n',val);
            
        end
    end
    
    clf
    drawnow;
end
           