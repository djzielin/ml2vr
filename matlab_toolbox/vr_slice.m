% functions to set framework parameters of the slice plane

classdef vr_slice < handle
    properties (Access=private,Hidden=true)
        vr_inter;
        snum;
    end
    
    methods (Access=public)
        
        function obj=vr_slice(vi,slice_number) %% constructor
            obj.vr_inter=vi;
            obj.snum=slice_number;
        end
        
        function delete(obj) %% destructor
        end
        
        function setup_ortho_xy(obj,button)
            obj.set_pos([0 0 0]);
            obj.set_up([0 1 0]);
            obj.set_right([1 0 0]);
            obj.set_size(3);
            obj.set_color([1 0 0]);
            obj.set_button(button);
            obj.set_global(true);
            obj.enable(true);
        end
        
        function setup_ortho_xz(obj,button)
            obj.set_pos([0 0 0]);
            obj.set_up([1 0 0]);
            obj.set_right([0 0 1]);
            obj.set_size(3);
            obj.set_color([1 0 0]);
            obj.set_button(button);
            obj.set_global(true);
            obj.enable(true);
        end
        
        
        function setup_ortho_yz(obj,button)
            obj.set_pos([0 0 0]);
            obj.set_up([0 1 0]);
            obj.set_right([0 0 1]);
            obj.set_size(3);
            obj.set_color([1 0 0]);
            obj.set_button(button);
            obj.set_global(true);
            obj.enable(true);
        end     
        
        function enable(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(4,'uchar'); %% size
            obj.vr_inter.send_data(1,'uchar'); %%slice
            obj.vr_inter.send_data(0,'uchar'); %%enable
            obj.vr_inter.send_data(obj.snum,'uchar');
            
            if value==true
                obj.vr_inter.send_data(1,'uchar'); %%true
            else
                obj.vr_inter.send_data(0,'uchar');
            end
        end
        
        function set_pos(obj, value)
            obj.send_slice_value3(1,obj.snum,value);
        end
        
        function set_up(obj,value)
            obj.send_slice_value3(2,obj.snum,value);
        end
        
        function set_right(obj,value)
            obj.send_slice_value3(3,obj.snum,value);
        end
        
        function set_size(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(7,'uchar'); %% packet size
            obj.vr_inter.send_data(1,'uchar'); %% slice
            obj.vr_inter.send_data(4,'uchar');
            obj.vr_inter.send_data(obj.snum,'uchar');
            obj.vr_inter.send_data(value,'float32');
        end
        
        function set_color(obj,value)
            obj.send_slice_value3(5,obj.snum,value);
        end
        
        function set_button(obj, value)
            
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(4,'uchar'); %% size
            obj.vr_inter.send_data(1,'uchar'); %%slice
            obj.vr_inter.send_data(6,'uchar'); %%button id
            obj.vr_inter.send_data(obj.snum,'uchar');
            obj.vr_inter.send_data(value,'uchar');
        end
        
        function set_global(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(4,'uchar'); %% size
            obj.vr_inter.send_data(1,'uchar'); %%slice
            obj.vr_inter.send_data(7,'uchar'); %%button id
            obj.vr_inter.send_data(obj.snum,'uchar');
            
            if value==true
                obj.vr_inter.send_data(1,'uchar'); %%true
            else
                obj.vr_inter.send_data(0,'uchar');
            end
        end
    end
   
    methods (Access=private)
        
        function send_slice_value3(obj, func, plane_id, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(15,'uchar'); %% packet size
            obj.vr_inter.send_data(1,'uchar'); %% slice
            obj.vr_inter.send_data(func,'uchar');
            obj.vr_inter.send_data(obj.snum,'uchar');
            obj.vr_inter.send_data(value(1),'float32');
            obj.vr_inter.send_data(value(2),'float32');
            obj.vr_inter.send_data(value(3),'float32');
        end     
    end
end
