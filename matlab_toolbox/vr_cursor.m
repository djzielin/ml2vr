% functions to set framework parameters of the virtual cursor

classdef vr_cursor < handle
    properties (Access=private,Hidden=true)
        vr_inter;
    end
    
    methods (Access=public)
        function obj=vr_cursor(vi) %% constructor
            obj.vr_inter=vi;
        end
        function delete(obj) %% destructor
        end
        
        function enable(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(5,'uchar'); %%
            obj.vr_inter.send_data(0,'uchar'); %% show
            
            if value==true
                obj.vr_inter.send_data(1,'uchar'); %%true
            else
                obj.vr_inter.send_data(0,'uchar');
            end
            
        end
                
        function set_cursor_color(obj, r, g, b)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(14,'uchar'); %% size
            obj.vr_inter.send_data(5,'uchar'); %%
            obj.vr_inter.send_data(3,'uchar'); %%
            obj.vr_inter.send_data(r,'float32');
            obj.vr_inter.send_data(g,'float32');
            obj.vr_inter.send_data(b,'float32');
        end
        
        function set_cursor_model(obj, model, length, width)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(5,'uchar'); %%
            obj.vr_inter.send_data(4,'uchar');
            
            if strcmpi(model,'box') == 1
                obj.vr_inter.send_data(0,'uchar'); %%true
            else
                obj.vr_inter.send_data(1,'uchar');
            end
            
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(6,'uchar'); %% size
            obj.vr_inter.send_data(5,'uchar'); %%
            obj.vr_inter.send_data(1,'uchar'); %%
            obj.vr_inter.send_data(length,'float32');
            
            
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(6,'uchar'); %% size
            obj.vr_inter.send_data(5,'uchar'); %%
            obj.vr_inter.send_data(2,'uchar'); %%
            obj.vr_inter.send_data(width,'float32');
            
        end
        
        function set_position_at_tip(obj,value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(3,'uchar'); %% size
            obj.vr_inter.send_data(5,'uchar'); %%
            obj.vr_inter.send_data(5,'uchar');
            
            if value==true
                obj.vr_inter.send_data(1,'uchar'); %%true
            else
                obj.vr_inter.send_data(0,'uchar');
            end
        end
        
        function set_push_forward_amount(obj,value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(6,'uchar'); %% size
            obj.vr_inter.send_data(5,'uchar'); %%
            obj.vr_inter.send_data(6,'uchar');
            obj.vr_inter.send_data(value,'float');     
            
        end
        
        function setup_ray_preset(obj)
            obj.set_cursor_color(1.0,1.0,0.0);
            obj.set_cursor_model('pyramid', 100, 0.1);
            obj.set_push_forward_amount(0.25);
            obj.set_position_at_tip(false);
            obj.enable(true);
        end
        
        function setup_hand_preset(obj)
            obj.set_cursor_color(1.0,1.0,0.0);
            obj.set_cursor_model('box', 0.25, 0.25);
            obj.set_push_forward_amount(0.25);
            obj.set_position_at_tip(true);
            obj.enable(true);
        end
    end
end
