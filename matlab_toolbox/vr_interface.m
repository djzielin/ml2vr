%TODO:
%ideally all sends are packed data
%have try and catch around all read/write to catch any errors

classdef vr_interface < handle
    methods (Access=public)
        %% CONSTRUCTOR / DESTRUTOR
        function obj=vr_interface(ip_a, ip_p) %% constructor
            opengl HARDWARE; %force to choose opengl 
            
            if nargin<2
                obj.ip_port=9999;
            else
                obj.ip_port=ip_p;
            end
            
            if nargin<1
                obj.ip_addy='127.0.0.1';
            else
                obj.ip_addy=ip_a;
            end
            
            obj.connect();
            
            if obj.is_connected==false
                obj.ip_addy='127.0.0.1';
                obj.connect();
            end
            
            if obj.is_connected==false
                obj.ip_port=9999;
                obj.connect();
            end
            
            if obj.is_connected==false
                fprintf('giving up trying to connect to interface server\n');
                return;
            end
            
            obj.set_return_type('pos3');
            obj.clear_event_queues();
            obj.reset_app_parameters();
            obj.is_simulator=obj.internal_get_is_simulator();
            obj.vs=vr_sphere(0.25);
             
        end
        
        function delete(obj) %% destructor
            obj.disconnect();
        end
        
        
        %% EVENT FUNCTIONS (need to add try/catch around fread/fwrite)
        function [event, button, retval]=get_button_event(obj)
            if obj.is_connected==false
                event=0;
                button=0;
                retval=obj.default_return;
                return; %or should we try and reconnect to server?
            end
            
            s1=[0 0];
            s2=[0 1];
            %tic;
            if obj.ret_type == 0
                fwrite(obj.tcp_connection,s1,'uchar'); %%request request pos3 return
                
            else
                fwrite(obj.tcp_connection,s2,'uchar'); %%request request matrix4 return
                
            end
            
            [event,count]=fread(obj.tcp_connection,1,'uchar'); %%get button status
            if count==0
                'read count was 0, exiting'
                obj.is_connected=false;
                event=0;
                return;
            end;
            
            
            [button,count]=fread(obj.tcp_connection,1,'uchar'); %%get button number
            if count==0
                'read count was 0, exiting'
                obj.is_connected=false;
                event=0;
                return;
            end;
            
            if obj.ret_type == 0
                [retval,count]=fread(obj.tcp_connection,3,'float32'); %%get 3 32-bit floats (x,y,z)
                retval=obj.get_in_model_reference_frame(retval);
                if count==0
                    'read count was 0, exiting'
                    obj.is_connected=false
                    event=0
                    return;
                end;
            else
                [retval,count]=fread(obj.tcp_connection,16,'float32'); %%get 16 32-bit floats (x,y,z)
                retval=reshape(retval,4,4);
                retval=obj.get_in_model_reference_frame(retval);
                if count==0
                    'read count was 0, exiting'
                    obj.is_connected=false
                    event=0
                    return;
                end;
            end
            
            %'get button time: ' 
            %toc
            
        end
        function [event, axis, retval, val]=get_joystick_event(obj)
            if obj.is_connected==false
                event=0;
                axis=0;
                retval=0;
                val=obj.default_return
                return;
            end
            
            s1=[1 0];
            s2=[1 1];
            
            if obj.is_connected==true
                if obj.ret_type == 0
                    fwrite(obj.tcp_connection,s1,'uchar'); %%request request pos3 return
                else
                    fwrite(obj.tcp_connection,s2,'uchar'); %%request request matrix4 return
                end
                
                [event,count]=fread(obj.tcp_connection,1,'uchar'); %%get button status
                if count==0
                    'read count was 0, exiting'
                    obj.is_connected=false;
                    event=0;
                    return;
                end;
                
                [axis,count]=fread(obj.tcp_connection,1,'uchar'); %%get button number
                if count==0
                    'read count was 0, exiting'
                    obj.is_connected=false;
                    event=0;
                    return;
                end;
                
                [val,count]=fread(obj.tcp_connection,1,'float32');
                if count==0
                    'read count was 0. exiting' %% this never seems to be called, is it neccisary??
                    obj.is_connected=false;
                    event=0;
                    return;
                end;
                
                if obj.ret_type == 0
                    [retval,count]=fread(obj.tcp_connection,3,'float32'); %%get 3 32-bit floats (x,y,z)
                    retval=obj.get_in_model_reference_frame(retval);
                    if count==0
                        'read count was 0, exiting'
                        obj.is_connected=false;
                        event=0;
                        return;
                    end;
                else
                    [retval,count]=fread(obj.tcp_connection,16,'float32'); %%get 3 32-bit floats (x,y,z)
                                    retval=obj.get_in_model_reference_frame(retval);
                    retval=reshape(retval,4,4);
                    retval=obj.get_in_model_reference_frame(retval);
                    if count==0
                        'read count was 0, exiting'
                        obj.is_connected=false;
                        event=0;
                        return;
                    end;
                end
            end
        end
        
        function set_return_type(obj, value)
            if strcmp(value,'pos3') == 1
                obj.ret_type=0;
                obj.default_return=[0 0 0]';
            else
                obj.ret_type=1;
                obj.default_return=eye(4,4);
            end
        end
        
        function clear_event_queues(obj)
            if(obj.is_connected)
                try
                    fwrite(obj.tcp_connection,6,'uchar');
                catch
                    'error while trying to clear event queue'
                    obj.disconnect();
                end
            end
            
        end
        
        
        %% POLLING FUNCTIONS
        function [retval]=get_sensor_state(obj, sensorID)
            fwrite(obj.tcp_connection,2,'uchar'); %%request sensor location
            
            if obj.ret_type == 0
                fwrite(obj.tcp_connection,0,'uchar'); %%request request pos3 return
            else
                fwrite(obj.tcp_connection,1,'uchar'); %%request request matrix4 return
            end
            
            fwrite(obj.tcp_connection,sensorID,'uchar');
            
            if obj.ret_type == 0
                [retval,count]=fread(obj.tcp_connection,3,'float32'); %%get 3 32-bit floats (x,y,z)
                retval=obj.get_in_model_reference_frame(retval);
                if count==0
                    'read count was 0, exiting'
                    obj.is_connected=false;
                    event=0;
                    return;
                end
            else
                [retval,count]=fread(obj.tcp_connection,16,'float32'); %%get 16 32-bit, ie matrix4
                retval=reshape(retval,4,4);
                retval=obj.get_in_model_reference_frame(retval);
                if count==0
                    'read count was 0, exiting'
                    obj.is_connected=false;
                    event=0;
                    return;
                end
            end
        end
        
        function [val]=get_button_state(obj, buttonID) %TODO 7
            if obj.is_connected==false
                val=false;
                return
            end
            %%TODO try/catch around there
            fwrite(obj.tcp_connection,7,'uchar'); %%request sensor location
            fwrite(obj.tcp_connection,buttonID,'uchar');
            
            [val,count]=fread(obj.tcp_connection,1,'uchar'); %%get button status
            if count==0
                'read count was 0, exiting'
                obj.is_connected=false;
                event=0;
                return;
            end
        end
        
        function [val]=get_joystick_state(obj, axisID) %TODO 8
            if obj.is_connected==false
                val=0;
                return
            end
            
            fwrite(obj.tcp_connection,8,'uchar'); %%request sensor location
            fwrite(obj.tcp_connection,axisID,'uchar');
            
            [val,count]=fread(obj.tcp_connection,1,'float'); %%get axis status
            if count==0
                'read count was 0, exiting'
                obj.is_connected=false;
                event=0;
                return;
            end
        end
        
        %% NAVIGATION FUNCTIONS (test!) %%
        function do_navigation_translate(obj, vec3)
            if obj.is_connected==false
                return;
            end;
            fwrite(obj.tcp_connection,3,'uchar'); %%request nav translate
            fwrite(obj.tcp_connection,vec3,'float32');
        end
        function do_navigation_rotate(obj, vec3, amount)
            if obj.is_connected==false
                return;
            end
            fwrite(obj.tcp_connection,4,'uchar'); %%request nav rotate
            fwrite(obj.tcp_connection,vec3,'float32');
            fwrite(obj.tcp_connection,amount, 'float32');
        end
        
        %%%%%%%%%%
        
        function set_transform_matrix(obj, value)
            obj.model_transform=value;
            obj.model_transform_inv=inv(value);
            
            if obj.is_connected               
                fwrite(obj.tcp_connection,5,'uchar');
                fwrite(obj.tcp_connection,66,'uchar'); %% size
                fwrite(obj.tcp_connection,3,'uchar'); %% transform
                fwrite(obj.tcp_connection,0,'uchar'); %% matrix4
                
                for i=1:16
                    fwrite(obj.tcp_connection,value(i),'float32');
                end
                
                fwrite(obj.tcp_connection,5,'uchar');
                fwrite(obj.tcp_connection,66,'uchar'); %% size
                fwrite(obj.tcp_connection,3,'uchar'); %% transform
                fwrite(obj.tcp_connection,1,'uchar'); %% inverse matrix4
                
                for i=1:16
                    fwrite(obj.tcp_connection,obj.model_transform_inv(i),'float32');
                end
                
            end
            

        end      
        
        function [result]=is_q_pressed(obj)
            kkey = get(gcf,'CurrentCharacter');
            pause(0.001);
            
            if(kkey=='q')
               result=true;
            else
               result=false;
            end
    
        end
        
        %% used by helper classes (vr_parameters, vr_virtual_hand, ect..)
        function send_data(obj,data_to_send,data_type)
            if obj.is_connected==false
                return;
            end
            
            try
                fwrite(obj.tcp_connection,data_to_send,data_type);
            catch
                fprintf('error while using send_data\n');
                obj.disconnect();
            end
        end        
        
        function [result]=get_is_connected(obj)
            result=obj.is_connected;
        end
        
        function [result]=get_is_simulator(obj)
            result=obj.is_simulator;
        end
        
        function [result]=draw_simulator(obj)
            %if obj.is_simulator==false
            %    return
            %end

            val=obj.get_sensor_state(1);
            if(size(val,1)==3)
                obj.vs.setPos(val(1),val(2),val(3));
            else
                obj.vs.setMatrix4(val);
            end
            
            obj.vs.render();
        end
        
    end
    
    
    properties (Access=private,Hidden=true)
        tcp_connection;
        ip_addy='';
        ip_port='';
        is_simulator=false;
        is_connected=false;
        ret_type=0;
        model_transform=eye(4,4);
        model_transform_inv=eye(4,4);
        default_return=eye(4,4);
        vs;
    end
    
    methods (Access=private,Hidden=true)
        
        function [val]=internal_get_is_simulator(obj)
            if obj.is_connected==false
                val=true;
            end
            
            fwrite(obj.tcp_connection,9,'uchar'); 
            
            [val,count]=fread(obj.tcp_connection,1,'uchar');
            if count==0
                'read count was 0, exiting'
                obj.is_connected=false;
                event=0;
                return;
            end;
        end
        
        function [newretval]=get_in_model_reference_frame(obj,pval)
            s=length(pval);
            
            if s==3
                newr=obj.model_transform_inv*makehgtform('translate',pval);
                newretval=newr([13 14 15])'; %extract pos3
            else
                newretval=obj.model_transform_inv*pval;
            end
        end
        
        function connect(obj)
            obj.tcp_connection = tcpip(obj.ip_addy,obj.ip_port,'NetworkRole','Client','TransferDelay','off');
            try
                fopen(obj.tcp_connection);
            catch
                fprintf('unable to connect to interface server at: %s:%d\n',obj.ip_addy,obj.ip_port);
                obj.is_connected=false;
                return;
            end
            
            'successfully connected to interface server';
            obj.is_connected=true;
            
        end
        
        function disconnect(obj)
            if(obj.is_connected)
                try
                    fclose(obj.tcp_connection);
                catch
                    'error during event server disconnect'
                end
                obj.is_connected=false;
            end
        end
        
        function reset_app_parameters(obj)
            if(obj.is_connected)
                try
                    fwrite(obj.tcp_connection,5,'uchar');
                    fwrite(obj.tcp_connection,1,'uchar'); %% size
                    fwrite(obj.tcp_connection,10,'uchar'); %%
                catch
                    'error during app papameter reset'
                    obj.is_connected=false;
                end
            end
            
        end
        
    end
end