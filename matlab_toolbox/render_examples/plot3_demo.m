addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('pos3');

vvh=vr_virtual_hand(vi);
vvh.enable(true);                      
vvh.set_hand_button(5);

m1 = makehgtform('translate',[0 3 0]);
m2 = makehgtform('xrotate',-pi/2.0);
m3 = makehgtform('scale',[1,1,0.1]);
mf=m1*m2*m3;
vi.set_transform_matrix(mf);

close all
figure('Renderer','OpenGL');

%demo code below from: 
%  http://www.mathworks.com/help/matlab/ref/plot3.html

t = 0:pi/50:10*pi;
h=plot3(sin(t),cos(t),t)
set(h,'Parent',hgtransform)     %% HACK TO FORCE INTO 3D