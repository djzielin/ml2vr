addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('pos3');

vvh=vr_virtual_hand(vi);
vvh.enable(true);                      
vvh.set_hand_button(5);

m1 = makehgtform('translate',[0 3 0]);
m2 = makehgtform('xrotate',-pi/2.0);
m3 = makehgtform('scale',[1,1,1]);
mf=m1*m2*m3;
vi.set_transform_matrix(mf);

close all
figure('Renderer','OpenGL');

%% render code below

fill3([0 1 1],[0 0 1], [0 0 0],'blue');