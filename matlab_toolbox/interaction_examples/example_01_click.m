% example_01_click.m 
% user presses button to change color of the sphere

addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('pos3');

close all;
f=figure('Renderer','OpenGL');

vs=vr_sphere(1);
vs.setPos(0,5,0);
vs.setColor(0,1,0);

do_redraw=true;

while 1
   [event,button,pos3]=vi.get_button_event();
    
   if button == VRInteractionButton 
       if event==1 % button pressed
            fprintf('button pressed  at: %f\n',cputime);           
            vs.setColor(1,0,0);
            do_redraw=true;
        end
     
        if event == 2 % button released
            fprintf('button released at: %f\n',cputime);
            vs.setColor(0,1,0);
            do_redraw=true;
        end
    end
   
    if do_redraw
        clf;
        %vr_cam(5);
        hold on;
        vs.render();
        drawnow;
        do_redraw=false;
    end
    
    if vi.is_q_pressed()==true
        break;
    end
    
end

delete(vi); % close the connection

    
