% example_02_joystick.m 
% manipulate joystick to change color of spheres. 
% x axis (left to right) changes left sphere
% y axis (up and down) changes right sphere

addpath('..');
vr_config
vi=vr_interface(ServerIP,ServerPort); 

close all;
figure('Renderer','OpenGL');

vs=vr_sphere(0.5);
vs.setPos(-1,5,-2);

vs2=vr_sphere(0.5);
vs2.setPos(1,5,-2);

vs.setColor(0.5,0,0.5);
vs2.setColor(0.5,0,0.5);

do_redraw=true;

while 1
    [event,axis,pos3,val]=vi.get_joystick_event();
    
    val_conv=(val+1.0)*0.5;
    
    if event == 1
        if axis == 0
            fprintf('axis 0 value: %f\n',val);
            vs.setColor(val_conv,0,1-val_conv);
            do_redraw=true;
        end
        
        if axis == 1
            fprintf('axis 1 value: %f\n',val);
            vs2.setColor(val_conv,0,1-val_conv);
            do_redraw=true;
        end
    end
    
    if do_redraw
        clf;
        vr_cam(5);
        hold on;
        vs.render();
        vs2.render();
        drawnow;
        do_redraw=false;
    end
    
    if vi.is_q_pressed()==true
        break;
    end
end

delete(vi); %% close the connection




