% example_11_navigation.m 
% user uses joystick x and y to navigate around scene

addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('matrix4');

close all;
figure('Renderer','OpenGL');

vs=vr_sphere(1);
vs.setPos(0,3,0);
vs.setColor(0,1,0);

fmin=-4; %for floor
fmax=4;

fx=[fmin fmin fmax fmax];
fy=[0 0 0 0];
fz=[fmin fmax fmax fmin ];

do_redraw=true;

while 1
    [m4]=vi.get_sensor_state(1);
    wand_fdir=m4*[0 0 -1 1]' - m4*[0 0 0 1]';
    wand_fdir=wand_fdir(1:3)';
    wand_fdir=wand_fdir/norm(wand_fdir); %make sure unit length
    
    [axis0]=vi.get_joystick_state(0);
    [axis1]=vi.get_joystick_state(1);
    
    if axis1~=0
        vi.do_navigation_translate(wand_fdir*axis1*0.1);
    end
    
    if axis0~=0
        vi.do_navigation_rotate([0 1 0],axis0*0.1);
    end
    
    if do_redraw
        clf;
        vr_cam(5);
        hold on;
        
        vs.render();
        fill3(fx,fy,fz,'black'); %%render the floor
        
        drawnow;
        do_redraw=false;
    end
    
    if vi.is_q_pressed()==true
       break;
    end
end

delete(vi); % close the connection

    