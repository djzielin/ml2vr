% example_09_sphere_spawner.m 
% the user presses a button to "drop" a sphere at the current location. 
% this allows the user to fill up the world with spheres at the locations
% of their choosing. 

addpath('..');
vr_config
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('pos3');

close all;
figure('Renderer','OpenGL');
    
sphere_array={};
current_index=0;

do_redraw=true;

while 1
    [event,button,pos3]=vi.get_button_event();
    
    if button == VRInteractionButton
        if event==1 %% button pressed
            fprintf('spawning sphere at: %f %f %f\n',pos3(1),pos3(2),pos3(3));
            vs=vr_sphere(0.25);
            vs.setPos(pos3(1),pos3(2),pos3(3));
            vs.setColor(0,1,0);
            current_index=current_index+1;
            sphere_array{current_index}=vs;
            do_redraw=true;
        end
    end
    
    if do_redraw
        clf;
        vr_cam(5);
        hold on;
        
        for i=1:current_index
            sphere_array{i}.render();
        end
        
        drawnow;
        do_redraw=false;
    end
    
    if vi.is_q_pressed()==true
        break;
    end
end
