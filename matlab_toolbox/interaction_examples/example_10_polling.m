% example_10_polling.m 
% illustrate how to get immediate button/joystick/sensor values. 
% for most cases use the event based methods instead

addpath('..');
vr_config
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('pos3');

close all;

while 1
    [pos3]=vi.get_sensor_state(0);
    [button0]=vi.get_button_state(0);
    [button1]=vi.get_button_state(1);    
    [axis0]=vi.get_joystick_state(0);
    [axis1]=vi.get_joystick_state(1);
    
    fprintf('---------------------------------------------\n');
    fprintf('sensor 0 is at pos: %f %f %f\n',pos3(1),pos3(2),pos3(3));
    fprintf('buttons have values: %d %d\n',button0,button1);
    fprintf('joystick has values: %f %f\n',axis0,axis1);
    
    pause(1);
    
    if vi.is_q_pressed()==true
        break;
    end
end

delete(vi); %% close the connection

    