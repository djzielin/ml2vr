% example_07_virtual_hand_matrix4.m 
% touch, click, drag interaction. the user first makes contact with 
% the sphere, then presses button to initiate dragging. while the button 
% is held, the sphere remains "attached" and mimics the users's wand
% movements (position and orientation). When the user releases the button,
% the sphere becomes "detached" and no longer follows the wand's movements.

addpath('..');
vr_config
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('matrix4');

close all;
figure('Renderer','OpenGL');

sphere_size=0.25;
vs=vr_sphere(sphere_size);
matrix4_current = makehgtform('translate',[0 5 0]);
vs.setMatrix4(matrix4_current);
vs.setColor(0,0,1);

is_dragging=false;
is_touching=false;
old_d=100;

interaction_button=VRInteractionButton;
do_redraw=true;

while 1
    [event,button,matrix4]=vi.get_button_event();
    
  
    wand_pos=matrix4([13 14 15]); %%extract pos3
    sphere_pos=matrix4_current([13 14 15]);
    
    pinput=[sphere_pos;wand_pos];
    d=pdist(pinput);
      
    if is_dragging
        matrix4_current=matrix4*drag_diff*stored_transform;
        vs.setMatrix4(matrix4_current);
        do_redraw=true;
    else
        if(d<=sphere_size && old_d>sphere_size) %% touch begining
            vs.setColor(0,1,0);
            is_touching=true;
            fprintf('started touching at: %f\n',cputime);
            do_redraw=true;
        end
        if(d>sphere_size && old_d<=sphere_size) %% touch ending
            vs.setColor(0,0,1);
            is_touching=false;
            fprintf('stopped touching at: %f\n',cputime);
            do_redraw=true;
        end
    end
    
    if is_touching==true && event==1 && button==interaction_button %% user touched and clicked
        is_dragging=true;
        stored_transform=matrix4_current;
        drag_diff=inv(matrix4);
        vs.setColor(1,0,0);
        fprintf('drag starting    at: %f\n',cputime);
        do_redraw=true;
    end
    
    if is_dragging==true && event==2 && button==interaction_button %% user has released object
        is_dragging=false;
        matrix4_current=matrix4*drag_diff*stored_transform;
        vs.setMatrix4(matrix4_current);
        vs.setColor(0,1,0);
        fprintf('drag ending      at: %f\n',cputime);
        do_redraw=true;
    end

    if do_redraw
        clf;
        vr_cam(5);
        hold on;
        vs.render();
        drawnow;
        do_redraw=false;
    end
    old_d=d;
    
    if vi.is_q_pressed()==true
        break;
    end
end

delete(vi); %% close the connection