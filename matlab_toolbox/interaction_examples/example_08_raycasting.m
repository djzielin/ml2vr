% example_08_raycasting_matrix4.m 
% ray, click, drag interaction. the user first intersects the sphere with 
% a ray (line) coming from the tip of the user's wand, then presses a 
% button to initiate dragging. while the button  is held, the sphere 
% remains "attached" and mimics the users's wand movements (position and 
% orientation). When the user releases the button, the sphere becomes 
% "detached" and no longer follows the wand's movements.

addpath('..');
vr_config
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('matrix4');

close all;
figure('Renderer','OpenGL');

sphere_size=0.25;
vs=vr_sphere(sphere_size);
matrix4_current = makehgtform('translate',[0 5 0]);
vs.setMatrix4(matrix4_current);
vs.setColor(0,0,1);

is_dragging=false;
is_touching=false;

old_d=100;

do_redraw=true;
interaction_button=VRInteractionButton;

vc=vr_cursor(vi);
vc.setup_ray_preset();   

while 1
    [event,button,matrix4]=vi.get_button_event();  
    
    wand_for_dir=matrix4*[0 0 -1 1]' - matrix4*[0 0 0 1]';
    wand_for_dir=wand_for_dir(1:3)';
    wand_pos=matrix4([13 14 15]); %%extract pos3
    sphere_pos=matrix4_current([13 14 15]);
    
    %%http://www.mathworks.com/matlabcentral/newsreader/view_thread/164048
    Q2=wand_pos+wand_for_dir;
    Q1=wand_pos;
    P=sphere_pos;   
    
    d = norm(cross(Q2-Q1,P-Q1))/norm(Q2-Q1); %distance of P to line
    
    if is_dragging
        matrix4_current=matrix4*drag_diff*stored_transform;
        vs.setMatrix4(matrix4_current);
        do_redraw=true;
    else
        if(d<=sphere_size && old_d>sphere_size)
            vs.setColor(0,1,0);
            is_touching=true;
            fprintf('started touching at: %f\n',cputime);
            do_redraw=true;

        end
        if(d>sphere_size && old_d<=sphere_size)
            vs.setColor(0,0,1);
            is_touching=false;
            fprintf('stopped touching at: %f\n',cputime);
            do_redraw=true;
        end
    end
    
    if is_touching==true && event==1 && button==interaction_button %% user touched and clicked
        is_dragging=true;
        stored_transform=matrix4_current;
        drag_diff=inv(matrix4);
        vs.setColor(1,0,0);
        fprintf('drag starting    at: %f\n',cputime);
        do_redraw=true;
    end
    
    if is_dragging==true && event==2 && button==interaction_button %% user has released object
        is_dragging=false;
        matrix4_current=matrix4*drag_diff*stored_transform;
        vs.setMatrix4(matrix4_current);
        vs.setColor(0,1,0);
        fprintf('drag ending      at: %f\n',cputime);
        do_redraw=true;
    end
    
    if do_redraw
        clf;
        vr_cam(5);
        hold on;
        vs.render();
        drawnow;
        do_redraw=false;
    end
    
    old_d=d;
    
    if vi.is_q_pressed()==true
        break;
    end
end

delete(vi); %% close the connection