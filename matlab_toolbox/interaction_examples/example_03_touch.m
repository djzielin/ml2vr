% example_03_touch.m 
% when the user touches sphere, the color of the sphere is changed.

addpath('..');
vr_config
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('pos3');

close all;
figure('Renderer','OpenGL');

vs=vr_sphere(1);
pos_target=[0 5 0]';
vs.setColor(0,1,0);
do_redraw=true;
prev_d=100;

while 1
    [event,button,pos3]=vi.get_button_event();
        
    pinput=[pos_target';pos3'];
    d=pdist(pinput);
      
    vs.setPos(pos_target(1),pos_target(2),pos_target(3));

    if((d<=1.0) && (prev_d > 1.0)) %% just started touching
        vs.setColor(1,0,0);
        do_redraw=true;
        fprintf('started touch at: %f\n',cputime);
    end
    if ((d>1.0) && (prev_d<=1.0)) %% just stopped touching
        vs.setColor(0,1,0);
        do_redraw=true;
        fprintf('stopped touch at: %f\n',cputime);
    end
   
    if(vi.get_is_simulator())
        do_redraw=true;
    end
    
    if do_redraw
        clf;
        vr_cam(5);
        hold on;
        vs.render();
        
        if(vi.get_is_simulator())
           vi.draw_simulator();
        end
        
        drawnow;
        do_redraw=false;
    end
    
    prev_d=d;
    
    if vi.is_q_pressed()==true
        break;
    end
end

delete(vi); %% close the connection

    
    

