% example_05_drag_matrix4.m 
% sphere follows position and orientation of user's wand

addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('matrix4');

close all;
figure('Renderer','OpenGL');

vs=vr_sphere(0.25);

while 1
    [event,button,matrix4]=vi.get_button_event();
    
    clf;
    vr_cam(5);
    hold on;
    vs.setMatrix4(matrix4);
    vs.render();
    drawnow;
    
    if vi.is_q_pressed()==true
       break;
    end
end

delete(vi); %% close the connection

    
    

