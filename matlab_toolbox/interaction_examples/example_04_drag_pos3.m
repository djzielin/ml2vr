% example_04_drag_pos3.m 
% sphere follows position of user's wand

addpath('..');

vr_config
vi=vr_interface(ServerIP,ServerPort); 
vi.set_return_type('pos3');

close all;
figure('Renderer','OpenGL');

vs=vr_sphere(0.25);

while 1
    [event,button,pos3]=vi.get_button_event();
    
    clf;
    vr_cam(5);
    hold on;
    vs.setPos(pos3(1),pos3(2),pos3(3));
    vs.render();
    drawnow;
    
    if vi.is_q_pressed()==true
        break;
    end
end

delete(vi); %% close the connection

    
    

