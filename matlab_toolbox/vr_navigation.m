% functions to set framework parameters of the navigation

classdef vr_navigation < handle
    properties (Access=private,Hidden=true)
        vr_inter;
    end
    
    methods (Access=public)
        
        function obj=vr_navigation(vi) %% constructor
            obj.vr_inter=vi;
        end
        
        function delete(obj) %% destructor
        end
        
        function setup_fly_preset(obj, travel_speed, turning_speed) %TODO use nargin to give default values
            if(nargin<3)
                turning_speed=10;
            end
            if(nargin<2)
                travel_speed=1;
            end
            
            obj.set_axis_x_func('yaw');
            obj.set_axis_x_mult(turning_speed);
            obj.set_axis_y_func('trans_forward_back');
            obj.set_axis_y_mult(travel_speed);
            obj.set_constrain('');
        end
        
        function setup_walk_preset(obj, travel_speed, turning_speed)
            if(nargin<3)
                turning_speed=10;
            end
            if(nargin<2)
                travel_speed=1;
            end
            
            obj.set_axis_x_func('yaw');
            obj.set_axis_x_mult(turning_speed);
            obj.set_axis_y_func('trans_forward_back');
            obj.set_axis_y_mult(travel_speed);
            obj.set_constrain('Y');
        end
        
        function set_axis_x_func(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(4,'uchar'); %% size
            obj.vr_inter.send_data(0,'uchar'); %% set navigation
            obj.vr_inter.send_data(0,'uchar'); %% set axis func
            obj.vr_inter.send_data(0,'uchar'); %% axis id (x)
            
            v=obj.get_trans_func_as_uchar(value);
            obj.vr_inter.send_data(v,'uchar');
        end
        
        function set_axis_x_mult(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(7,'uchar'); %% size
            obj.vr_inter.send_data(0,'uchar'); %% set navigation
            obj.vr_inter.send_data(1,'uchar'); %% set axis mult
            obj.vr_inter.send_data(0,'uchar'); %% axis id (x)
            obj.vr_inter.send_data(value,'float32');
        end
        
        
        function set_axis_y_func(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(4,'uchar'); %% size
            obj.vr_inter.send_data(0,'uchar'); %% set navigation
            obj.vr_inter.send_data(0,'uchar'); %% set axis func
            obj.vr_inter.send_data(1,'uchar'); %% axis id (x)
            
            v=obj.get_trans_func_as_uchar(value);
            obj.vr_inter.send_data(v,'uchar');
        end
        function set_axis_y_mult(obj, value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(7,'uchar'); %% size
            obj.vr_inter.send_data(0,'uchar'); %% set navigation
            obj.vr_inter.send_data(1,'uchar'); %% set axis mult
            obj.vr_inter.send_data(1,'uchar'); %% axis id (x)
            obj.vr_inter.send_data(value,'float32');
        end
        
        function set_constrain(obj,value)
            obj.vr_inter.send_data(5,'uchar');
            obj.vr_inter.send_data(5,'uchar'); %% size
            obj.vr_inter.send_data(0,'uchar'); %% set navigation
            obj.vr_inter.send_data(2,'uchar'); %% set constraint
            
            x_constrain=0;
            y_constrain=0;
            z_constrain=0;
            
            if ~isempty(strfind(value,'X'))
                x_constrain=1;
            end
            if ~isempty(strfind(value,'Y'))
                y_constrain=1;
            end
            if ~isempty(strfind(value,'Z'))
                z_constrain=1;
            end
            
            obj.vr_inter.send_data(x_constrain,'uchar');
            obj.vr_inter.send_data(y_constrain,'uchar');
            obj.vr_inter.send_data(z_constrain,'uchar');
        end
    end
    
    methods (Access=private)
        
        function [v]=get_trans_func_as_uchar(obj,value)
            v=0;
            if strcmp(value, 'none') ==1
                v=0;
            end
            if strcmp(value, 'trans_right_left') ==1
                v=1;
            end
            if strcmp(value, 'trans_forward_back')==1
                v=2;
            end
            if strcmp(value, 'trans_up_down')==1
                v=3;
            end
            if strcmp(value, 'yaw')==1
                v=4;
            end
            if strcmp(value, 'pitch')==1
                v=5;
            end
            if strcmp(value, 'roll')==1
                v=6;
            end
        end
    end
end
