%viz_01_coneplot.m - djzielin
%demo of coneplay and stream line.
%joystick x axis modifies the vectors 'v' values.
%buttons 0 and 1 toggle the cone and lines on/off
%use button 5 for virtual hand interaction
%
%Based on the demo at:
%http://www.mathworks.com/help/matlab/examples/volume-visualization.html
clear;
addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort);
vi.set_return_type('pos3');


close all;
figure('Renderer','OpenGL');

do_redraw=true;


%mz = [1 0 0 0; 0 1 0 0; 0 0 -1 0; 0 0 0 1]; % z flip
m1 = makehgtform('translate',[-10 2.25 2]);
m2 = makehgtform('xrotate',-pi/2.0);
m3 = makehgtform('scale',0.1);
mf=m1*m2*m3;
vi.set_transform_matrix(mf);

vvh=vr_virtual_hand(vi);
vvh.enable(true);
vvh.set_hand_button(5);

load wind
[cx cy cz] = meshgrid(linspace(71,134,10),linspace(18,59,10),3:4:15);
[sx sy sz] = meshgrid(80, 20:10:50, 0:5:15);

miny=18; %for floor
maxy=59;
minx=71;
maxx=134;

fx=[minx minx maxx maxx];
fy=[miny maxy maxy miny];
fz=[0 0 0 0 ];

v_new=v; %user modifies this one with joystick

show_cones=true;
show_lines=false;

while 1
    [event,button,pos3]=vi.get_button_event();
    
    if event==1 && button==1
        if(show_cones==true)
            show_cones=false;
        else
            show_cones=true;
        end
        do_redraw=true;
    end
    
    if event==1 && button==0
        if(show_lines==true)
            show_lines=false;
        else
            show_lines=true;
        end
        do_redraw=true;
    end
    
    
    [axis0]=vi.get_joystick_state(0);
    
    if axis0~=0
        v_new=v_new+axis0*0.25;
        do_redraw=true;
    end
    
    if do_redraw
        clf;
        
        axis([minx maxx miny maxy 0 15])
        campos([175 10 85]);
        camtarget([105 40 0]);
        hold on;
        
        fill3(fx,fy,fz,'black'); %%render the floor
        
        if show_cones
            h2 = coneplot(x,y,z,u,v_new,w,cx,cy,cz,y,3);
            colormap(hsv)
            set(h2,'EdgeColor', 'none')
        end
        
        if show_lines
            h = streamline(x,y,z,u,v_new,w,sx,sy,sz);
            set(h, 'Color', 'cyan')
        end
        
        drawnow;
        do_redraw=false;
    end
end


