%viz_02_surf.m - djzielin
%show the demo surf (from the MATLAB documentation)
%user can modify surface parameters with joystick x axis
%use button 5 for virtual hand interaction

addpath('..');
vr_config;
vi=vr_interface(ServerIP,ServerPort);
vi.set_return_type('pos3');

close all;
figure('Renderer','OpenGL');

do_redraw=true;

mz = [1 0 0 0; 0 1 0 0; 0 0 -1 0; 0 0 0 1]; % z flip
m1 = makehgtform('translate',[0 3 2]);
m2 = makehgtform('xrotate',-pi/2.0);
m3 = makehgtform('scale',1);
mf=mz*m1*m2*m3;
vi.set_transform_matrix(mf);

vvh=vr_virtual_hand(vi);
vvh.enable(true);
vvh.set_hand_button(5);

[X,Y] = meshgrid(-1.5:.05:1.5);
do_redraw=true;

scalef=10;

while 1
    [axis0]=vi.get_joystick_state(0);
    
    if axis0~=0
        scalef=scalef+axis0*0.05;
        if(scalef<1)
            scalef=1;
        end
        do_redraw=true;
    end
    
    if do_redraw
        R = sqrt((X*scalef).^2 + (Y*scalef).^2) + eps;
        Z = sin(R)./R * 1.5;
        
        clf;
        axis equal;
        view([2,3,2]);
        
        hold on;
        surf(X,Y,Z,'FaceColor','interp','EdgeColor','none');
        drawnow;
        do_redraw=false;
    end
end
