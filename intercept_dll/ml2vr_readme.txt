This project utilized source code from the opensource project glTrace.
I have included the relevant files in this distribution.  
The full original version (if you want it) can be downloaded from:
http://hawksoft.com/gltrace/

Building:
You'll need to get boost. 
You should be able to go into the subdirectory for your target (win32 or linux)
and issue the make command. You will have to edit the Makefile in ./make/ to
properly set the paths to boost.

-------------------------------------

Compiling for Win32:
The build process is setup for mingw. You will probably have to edit the 
Makefile to point to the proper locations for the boost libraries. Also, you
need the 2 compiled boost libraries to have been built with the same version 
mingw gcc compiler you are using. 

Running on Win32:
copy the opengl32.dll we compiled to 
C:\Program Files (x86)\MATLAB\R2011b\bin\win32
make c:\ml2vr
copy the intercept.cfg file to c:\ml2vr
edit the intercept.cfg file
Then just clicking the MATLAB icon works for me

-------------------------------------

Compiling for Linux:
Since MATLAB also uses boost, I found to get things working I had to use the 
same version of boost (for R2012a it is boost 1.44). Also, I linked against 
the same .so's that are distributed with MATLAB. (see the Makefile.intercept
file in ./make). This was tested on MATLAB R2012a

Running on Linux:
#set environment variable to we can find our new libGL.so.1
#put the intercept.cfg file in /etc/ml2vr [you'll need to create this!] use sudo
#edit intercept.cfg
export LD_LIBRARY_PATH=/home/djzielin/ml2vr/dev/intercept/linux
cd /usr/local/MATLAB/R2012a/bin
./matlab 

Note, the intercept looks for the "real" opengl.so as libGL.so
If its something else, ie, something64.so you'll have to change that in
the intercept code. 

-------------------------------------

Config Files:
interface.cfg needs to be in /etc/ml2vr or C:\ml2vr

-------------------------------------

TODO: 
* dump errors during startup on original glTrace side to log file
* get rid of original config file code on glTrace side
* make config file parameter for location of real opengl.dll


