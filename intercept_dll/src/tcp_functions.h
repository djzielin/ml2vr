#include "GL/gl.h"

extern void process_glMatrixMode(GLuint value);
extern void connect_to_server();
extern void process_glBegin(GLushort value);
extern void process_glEnd();
extern void process_glVertex3(GLfloat v1, GLfloat v2, GLfloat v3);
extern void process_glColor4(GLfloat v1, GLfloat v2, GLfloat v3, GLfloat v4);
extern void process_end_of_frame();
extern void process_glPushMatrix();
extern void process_glPopMatrix();
extern void proesss_glLoadIdentity();
extern void process_glScalef(const GLfloat v1, const GLfloat v2, const GLfloat v3);
extern void process_glTranslatef(const GLfloat v1, const GLfloat v2, const GLfloat v3);
extern void process_glRotatef(const GLfloat v1, const GLfloat v2, const GLfloat v3, const GLfloat v4);
extern void process_glLoadMatrixf(const GLfloat *v);
extern void process_glMultMatrixf(const GLfloat *v);
extern void process_glRenderMode(GLenum value);
extern void process_glGenLists (GLsizei range,GLuint result);
extern void process_glNewList (GLuint list, GLenum mode);
extern void process_glCallList (GLuint list);
extern void process_glEndList();
extern void process_glGetDoublev();

