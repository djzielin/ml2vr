/* gltrace.c                             -*- mode: C; tab-width: 4 -*-
 *
 * GLTrace OpenGL debug_logger/trace utlility
 * Version:  2.3
 * Copyright (C) 1999-2002
 *		Phil Frisbie, Jr. (phil@hawksoft.com)
 *
 * With original function counting code contributed by
 *		Keith Harrison (keithh@netcomuk.co.uk)
 * GLX wrapper code contributed by
 *		Bert Freudenberg (bert@freudenbergs.de)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include "gltrace.h"
#include "dll.h"

#include "tcp_functions.h"

#ifndef PATH_MAX
#define PATH_MAX 1024
#endif

/* Misc. globals */

extern int GetEXTProcIndex(LPCSTR name);

static void	*OpenGL_provider;

GLboolean needInit = GL_TRUE;

#include "gltrace.hpp" //names of opengl functions and enums

/* //do we use this?
int show_status[ARY_CNT(GLN)];
extern int show_status_EXT[];

typedef	enum
{
	NONE,
	CFG,
    KEY,
	IMP,
	EXC,
	OPT,
	OUTP,
	PORT,
	IP
} section;
*/

/*  gltraceInit() where the actual initialization take place */

FILE *debug_log;

extern FILE *read_config(char *fname);

GLboolean init_log_file()
{  
   #ifndef WIN32
      debug_log=read_config("/etc/ml2vr/intercept.cfg");
   #else
      debug_log=read_config("C:/ml2vr/intercept.cfg");
   #endif 
   
	if(debug_log==NULL)
		return GL_FALSE;  
     
   time_t t = time(0);   // get time now
   struct tm * now = localtime( & t );
   
   fprintf(debug_log, "Debug Log Started at: %d-%d-%d %d:%d:%d\n",    (now->tm_year + 1900),(now->tm_mon + 1),now->tm_mday,now->tm_hour,now->tm_min,now->tm_sec);
   fflush(debug_log);
   
   return GL_TRUE; 
}

GLboolean gltraceInit()
{
	int			i;
	char		INI_pathname[PATH_MAX];
	char		OGL_pathname[PATH_MAX];
	FILE		*INI;
	time_t		elapstime;
	struct tm	*adjtime;
	char		loctime[32];
	
	if(needInit == GL_FALSE)
   {
      return GL_TRUE;
   }
   
   if(init_log_file()==GL_FALSE)
	{
      fprintf(debug_log,"FAILED to init log file\n"); 
      fflush(debug_log);
   
	   Beep(1000,1000);
      return GL_FALSE;
	}
	   
   fprintf(debug_log,"[gltraceInit]\n");
	fflush(debug_log);

	fprintf(debug_log,"  about to setup enum names\n");
	fflush(debug_log);
	

	/* Build index for GLenum name lookup */
	
	for (i=0; i < ARY_CNT(GLenum_names); i++)
	{
		GLenum_names[i] = NULL;
	}
	
	for (i=0; i < ARY_CNT(GLenum_list); i++)
	{
		GLenum_names[GLenum_list[i].tag] = GLenum_list[i].name;
	}

    /* set the default OpenGL library path */
#if defined WIN32 || defined WIN64
    (void)GetSystemDirectory(OGL_pathname, PATH_MAX);
	strcat(OGL_pathname, "\\opengl32.dll"); 
#else
	strcpy(OGL_pathname,"/usr/lib/libGL.so");
#endif

    fprintf(debug_log,"real opengl is hopefully located at: %s\n",OGL_pathname);
    fflush(debug_log);
	
	fprintf(debug_log,"trying to connect to server!\n");
   fflush(debug_log);
		
	connect_to_server();

	
	
	/* Load specified OpenGL provider asked for in the INI file */

	//fprintf(debug_log,"trying to load: %s\n",OGL_pathname);

	fprintf(debug_log,"about to load real dll from: %s!\n",OGL_pathname);
   fflush(debug_log);
	
	
	OpenGL_provider = dllLoad(OGL_pathname);
	
	if(OpenGL_provider == NULL)
	{
		fprintf(debug_log,"ERROR: we couldn't load the real opengl dll, exiting!\n");
		fflush(debug_log);
		return GL_FALSE;
	}
	
	fprintf(debug_log,"setting up the jump table (this is where things usually fail)!\n");
   fflush(debug_log);
	
	/* Map OpenGL calls to our jump table */
	for (i=0; i < ARY_CNT(GLN); i++)
	{
		//fprintf(debug_log,"trying to load: %s\n",GLN[i]);

		FUNCTION OpenGL_proc = dllGetFunction(OpenGL_provider, GLN[i]);
		
		if (OpenGL_proc == NULL)
		{
         fprintf(debug_log,"Couldn't GetProcAddress for %s\n", GLN[i]);
			fprintf(debug_log,"  going to be stupid and continue anyway!\n");
			fflush(debug_log);
            //return GL_FALSE;
		}
      //else 
      //   fprintf(debug_log,"real opengl %s is at %X\n",GLN[i],OpenGL_proc);
         
		((FUNCTION *) (&GLV))[i] = OpenGL_proc;
	}
	
	needInit = GL_FALSE;
 	
	fprintf(debug_log,"  init is complete!!\n");
   fflush(debug_log);

    return GL_TRUE;
}

extern void *GLfuncs[];


void *get_opengl_function(LPCSTR a)
{
   
   
   int i;
   for (i=0; i < ARY_CNT(GLN); i++) //weird behavior of latest MATLAB, asks for all functions via wglGetProcAddress
   {
      if(strcmp(a,GLN[i])==0)  
      {
         fprintf(debug_log,"MATLAB want to know the location of function: %s %d\n",a,GLfuncs[i]); fflush(debug_log);
         //fprintf(debug_log,"  found it! %d\n",GLfuncs[i]); fflush(debug_log);
         return GLfuncs[i];
      }
   }
   //fprintf(debug_log,"  never found it\n"); fflush(debug_log);
   return NULL;
}
         
         

#if defined WIN32 || defined WIN64
BOOL WINAPI DllMain(/*@unused@*/HINSTANCE hinstDLL, DWORD fdwReason, /*@unused@*/LPVOID lpvReserved)
{

	if (fdwReason == DLL_PROCESS_DETACH)
	{
		if (debug_log != NULL)
		{
            //PrintCallCounts();
			//print("Closing output file\n");
			(void)fclose(debug_log);
			debug_log = NULL;
		}
        dllUnload(OpenGL_provider);
	}
	
	gltraceInit(); //new - on attach try to init
		
	
    return (BOOL)TRUE;
}

#else
void init(void) __attribute__ ((constructor));
void init(void)
{
    gltraceInit();
}

void fini(void) __attribute__ ((destructor));
void fini(void)
{
	if (debug_log != NULL)
	{
        //PrintCallCounts();
	    //print("Closing output file\n");
		fclose(debug_log);
		debug_log = NULL;
	}
    dllUnload(OpenGL_provider);
}

#endif /* WIN32 */

