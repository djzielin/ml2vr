/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/asio.hpp>
#include <boost/thread/thread.hpp> 

#define MODELS_TO_SKIP 3

using boost::asio::ip::tcp;

#include "gl.h"

//extern void print(const char *fmt, ...);

tcp::socket *s;
bool _connected_to_server;

boost::asio::io_service io_service;
//boost::asio::io_service::work work(io_service); //to make sure run() doesn't exit

bool _is_in_model_view;

unsigned char massive_buffer[10000000];
unsigned char massive_buffer2[10000000];
int current_bnum=0;

unsigned char *current_buffer;

unsigned int buffer_pos=0;
#include "arLock.h"

#include<vector>
#include<string>
using namespace std;

vector<tcp::socket *> socket_vec;

bool seen_glGetDouble;
int model_view_count=0;
bool _is_render=true;

string debug_file;
bool do_debug=false;
string opengl_port;
vector<string> connection_list; //[6]={"10.0.0.9","10.0.0.10","10.0.0.11","10.0.0.12","10.0.0.13","10.0.0.14"};
FILE *debug;


bool all_sent=true;

// http://www.cplusplus.com/faq/sequences/strings/trim/
std::string& trim_right_inplace(
  std::string&       s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return s.erase( s.find_last_not_of( delimiters ) + 1 );
}

std::string& trim_left_inplace(
  std::string&       s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return s.erase( 0, s.find_first_not_of( delimiters ) );
}

std::string& trim(
  std::string&       s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return trim_left_inplace( trim_right_inplace( s, delimiters ), delimiters );
}


extern "C" FILE *read_config(char *fname)
{
   string file_name=fname;
   FILE *f=fopen(file_name.c_str(),"rt");

   if(f==NULL)
   {     
      exit(1);
   }

   while (feof(f)==0 ) //TODO - use actual configuration file reading library
   {
      char line[1000];
      char *result=fgets(line,1000,f);
      if(result==NULL) continue;     
   
      if(strlen(line)==0) continue;
      if(line[0]=='#') continue;

      char * tok;
      tok = strtok (line,"\t ");
 
      if(strlen(tok)==0) continue;

      if(strcmp(tok,"debug_file")==0)
      {
         tok = strtok (NULL, "\t ");
         debug_file=tok;
         trim(debug_file); 

         debug=fopen(debug_file.c_str(),"wt");
         if(debug==NULL)
            exit(1);
         
         fprintf(debug,"[read_config]\n");
	      fflush(debug);
         do_debug=true;
         continue;
      }
      if(strcmp(tok,"client")==0)
      {
         tok = strtok (NULL, "\t ");
         string c=tok;
         trim(c);
         connection_list.push_back(c);

         fprintf(debug,"read_config - adding client at: %s\n",c.c_str());
         continue;
      }
     if(strcmp(tok,"opengl_port")==0)
      {
         tok = strtok (NULL, "\t ");
         opengl_port=tok;
         trim(opengl_port);
         fprintf(debug,"read_config - setting opengl_port: %s\n",opengl_port.c_str());
         continue;
      }
   }

   fclose(f);
   
   fprintf(debug,"finished reading config file\n");
   return debug;
}





extern "C" void connect_to_server()
{
   _connected_to_server=false;
   _is_in_model_view=false;

   try
   {
      tcp::resolver resolver(io_service);

      for(int i=0;i<connection_list.size();i++) 
      {
         fprintf(debug,"trying to connect to: %d\n",i);
         fflush(debug);

         tcp::resolver::query query(tcp::v4(), connection_list[i], opengl_port);
         tcp::resolver::iterator iterator = resolver.resolve(query);
         s=new tcp::socket(io_service);

         tcp::resolver::iterator end;

         // Try each endpoint until we successfully establish a connection.
         boost::system::error_code error = boost::asio::error::host_not_found;
         while (error && iterator != end)
         {
            s->close();
            s->connect(*iterator++, error);
         }
         if (error)
         {
            fprintf(debug,"unable to connect to client\n");
            fflush(debug);
       
            throw boost::system::system_error(error);
         }
         //boost::asio::connect(*s, iterator); //only valid for boost after version 1.46

         s->set_option(tcp::no_delay(true));   
         socket_vec.push_back(s);
      }
      _connected_to_server=true;
   }

   catch (std::exception& e)
   {
      fprintf(debug,"error connecting to server!: %s\n",e.what());
      fflush(debug);
   }

   if(massive_buffer==NULL)
   {
      _connected_to_server=false;
   }
   current_buffer=massive_buffer;

   if(_connected_to_server)
   {
      fprintf(debug,"SUCCESS: connected to all servers\n");
      fflush(debug);
   }

   else
   {
      fprintf(debug,"FAILURE: unable to connect to server\n"); fflush(debug);
   }

   buffer_pos=4;
}

int frame=0;

void dump_packet(unsigned char *data, unsigned int size)
{
	char filename[100];
#ifndef WIN32
	sprintf(filename,"/tmp/intercept_frame_%d.txt",frame);
#else
	sprintf(filename,"C:/tmp/intercept_frame_%d.txt",frame);
#endif

	FILE *out=fopen(filename,"w");

	int i=0;
	unsigned int *data_length=(unsigned int *)data;
	fprintf(out,"data_length=%d\n",data_length[0]); fflush(out);
	i+=4;

	GLfloat *v;
    unsigned int *u;

	for(;i<size;)
	{
		//fprintf(out,"at pos: %d\n",i); fflush(out);
		unsigned int command_length=data[i];
		unsigned int command=data[i+1];
		
		//fprintf(out,"command length: %d\n",command_length); fflush(out);
		//fprintf(out,"command is: %d\n",command); fflush(out);
		switch(command)
		{
		case 0: //glbegin
			fprintf(out,"  glBegin: %d\n",data[i+2]); fflush(out);
			break;
		case 1: //glend
			fprintf(out,"  glEnd\n"); fflush(out);
			break;
		case 2: //glVertex3f 
			v=((GLfloat *)&data[i+2]);
			fprintf(out,"  glVertex3f: %f %f %f \n",v[0],v[1],v[2]); fflush(out);
			break;
		case 3: //glColor4f
			v=((GLfloat *)&data[i+2]);
			fprintf(out,"  glColor4f: %f %f %f %f\n",v[0],v[1],v[2],v[3]); fflush(out);
			break;
		case 4:
			fprintf(out,"glPopMatrix\n");
			break;
		case 5:   
			fprintf(out,"glPushMatrix\n");
			break;
		case 6:
			fprintf(out,"glLoadIdentity\n");
			break;
		case 7:
			v=(GLfloat *)&data[i+2];
			fprintf(out,"glScalef %f %f %f\n",v[0],v[1],v[2]);
			break;        
		case 8:
			v=(GLfloat *)&data[i+2];
			fprintf(out,"glTranslatef %f %f %f\n",v[0],v[1],v[2]);
			break;
		case 9:
			v=(GLfloat *)&data[i+2];
			fprintf(out, "glRotatef: %f %f %f %f\n",v[0],v[1],v[2],v[3]);
			break; 
		case 10:
			v=(GLfloat *)&data[i+2];
			fprintf(out, "glLoadMatrixf %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15]);
			break;
		case 11:
			v=(GLfloat *)&data[i+2];
			fprintf(out, "glMultMatrixf %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15]);
			break;
		case 12:
			u=(unsigned int *)&data[i+2];
			fprintf(out, "glGenLists %d. master list: %d\n",u[0],u[1]);
			break;
		case 13:
			u=(unsigned int *)&data[i+2];
			fprintf(out, "glNewList %d mode: %d",u[0],u[1]);
			break;
		case 14:
			u=(unsigned int *)&data[i+2];
			fprintf(out, "glCallList %d\n",u[0]);
			break;
		case 15:
			fprintf(out, "glEndList\n");
			break;
		case 0xff:
			fprintf(out, " SWAP\n");
			break;
		default:
			fprintf(out,"  COMMAND: %d\n",command);
			
		}
		i+=(1+command_length);
	}
	fclose(out);
}

double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter()
{
    LARGE_INTEGER li;
    if(!QueryPerformanceFrequency(&li))
	cout << "QueryPerformanceFrequency failed!\n";

    PCFreq = double(li.QuadPart)/1000.0;

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;
}
double GetCounter()
{
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    return double(li.QuadPart-CounterStart)/PCFreq;
}

void send_thread(unsigned char *b, unsigned int s)
{   
   
   
   try
   {
      for(int i=0;i<socket_vec.size();i++)
      {
         size_t length=write(*socket_vec[i],boost::asio::buffer(b,s));       
         if(length<s)
         {
            fprintf(debug,"lost connection to client: %d\n",i); fflush(debug);
            _connected_to_server=false;
            break;
         }
      }
   }
   catch (std::exception& e)
   {
      fprintf(debug,"error during send (endpoint probably closed): %s\n",e.what());
      fflush(debug);
      _connected_to_server=false;
   }

   all_sent=true;
   
   
}

void send_frame_buffer()
{
 //  StartCounter();
 
   while(all_sent==false)
   {
      fprintf(debug,"didn't finish sending previous buffer. sleeping for 5 milliseconds\n"); fflush(debug);
      boost::this_thread::sleep(boost::posix_time::milliseconds(5)); 
   }
      
   buffer_pos-=4;
   memcpy(current_buffer,(unsigned char *)&buffer_pos,4);

   unsigned int buffer_length=buffer_pos+4;
   unsigned char *buffer=current_buffer;

   if(buffer_length==0) return;

   fprintf(debug,"send_frame_buffer - buffer size %d\n",buffer_length); fflush(debug);

   
   
   //dump_packet(buffer,buffer_length); //TODO: making dumping a config option
   frame++;

   if(_connected_to_server)
   {
      try
      {
          if(_connected_to_server==false)
            return;

          all_sent=false;
          //boost::thread t( boost::bind( &send_thread, buffer, buffer_length));  //TODO we could dynamically switch between threaded and non-threaded based on transfer size?   
          send_thread(buffer,buffer_length); //no threading version
          
      }
      catch (std::exception& e)
      {
         fprintf(debug,"error: %s\n",e.what());
         fflush(stdout);
         _connected_to_server=false;
      }
   }
   
   if(current_bnum==0) //rotate buffers 
   {
      current_buffer=massive_buffer2;
      current_bnum=1; 
   }
   else
   {
      current_buffer=massive_buffer;
      current_bnum=0;
   }
     
//   fprintf(debug,"Time to send buffer to all clients: %f ms\n",GetCounter()); fflush(debug);
}

void add_to_buffer(int buffer_length, unsigned char *buffer)
{
   if(_connected_to_server)
    {
      memcpy(&current_buffer[buffer_pos],buffer,buffer_length);
      buffer_pos+=buffer_length;      
      //TODO check for buffer overflow and grow buffer
   }
}

extern "C" void process_glGetDoublev()
{ 
   if(_is_in_model_view)
      seen_glGetDouble=true;

}


extern "C" void process_glMatrixMode(GLuint value)
{
   if(value==GL_MODELVIEW)
   {
      _is_in_model_view=true;
      model_view_count++;
	  //fprintf(debug,"entering model view mode. model view count: %d\n",model_view_count);
		 
   }
   else
   {
      _is_in_model_view=false;
   }
}

extern "C" void process_glRenderMode(GLenum value)
{
   if(value==GL_RENDER)
      _is_render=true;
   else
      _is_render=false;

   _is_render=true;
}

extern "C" void process_glBegin(GLushort value)
{
   if(!_is_in_model_view ) return;
  
   unsigned char packet[3];
   packet[0]=2; //size
   packet[1]=0; 
   packet[2]=value;

   add_to_buffer(3,packet);
}

extern "C" void process_glEnd()
{
   if(!_is_in_model_view ) return;

   unsigned char packet[2];
   packet[0]=1; //size
   packet[1]=1; 

   add_to_buffer(2,packet);
}

extern "C" void process_glVertex3(GLfloat v1, GLfloat v2, GLfloat v3)
{
   //fprintf(debug,"got a vertex: %f %f %f\n",v1,v2,v3);

   if(!_is_in_model_view ) return;

   unsigned char packet[14];
   
   packet[0]=13; //size
   packet[1]=2; 
   memcpy(&packet[2], (unsigned char *)&v1,4);
   memcpy(&packet[6], (unsigned char *)&v2,4);
   memcpy(&packet[10],(unsigned char *)&v3,4);

   add_to_buffer(14,packet);
}

extern "C" void process_glColor4(GLfloat v1, GLfloat v2, GLfloat v3, GLfloat v4)
{ 
   if(!_is_in_model_view ) return;

   unsigned char packet[18];

   packet[0]=17; //size
   packet[1]=3; 
   memcpy(&packet[2],(unsigned char *)&v1,4);
   memcpy(&packet[6],(unsigned char *)&v2,4);
   memcpy(&packet[10],(unsigned char *)&v3,4);
   memcpy(&packet[14],(unsigned char *)&v4,4);
   
   add_to_buffer(18,packet);
}
 
extern "C" void process_end_of_frame()
{
   unsigned char packet[2];
   packet[0]=1; //size
   packet[1]=0xFF; 

   add_to_buffer(2,packet);

   send_frame_buffer();
 
   buffer_pos=4;
   
   //fprintf(debug,"ending frame: model view count: %d\n",model_view_count);
   model_view_count=0;
   seen_glGetDouble=false;
   _is_render=true;
}

extern "C" void process_glPopMatrix()
{
   if(!_is_in_model_view || !seen_glGetDouble) return;

   unsigned char packet[2];
   packet[0]=1; //size
   packet[1]=4; 

   add_to_buffer(2,packet);
}

extern "C" void process_glPushMatrix()
{
   if(!_is_in_model_view || !seen_glGetDouble ) return;

   unsigned char packet[2];
   packet[0]=1; //size
   packet[1]=5; 
  
   add_to_buffer(2,packet);
}

extern "C" void process_glLoadIdentity()
{
   if(!_is_in_model_view || !seen_glGetDouble ) return;

   unsigned char packet[2];
   packet[0]=1; //size
   packet[1]=6; 

   add_to_buffer(2,packet);
}

extern "C" void process_glScalef(const GLfloat v1, const GLfloat v2, const GLfloat v3)
{ 
   if(!_is_in_model_view || !seen_glGetDouble) return;

   unsigned char packet[14];

   packet[0]=13; //size
   packet[1]=7; 

   memcpy(&packet[2],(unsigned char *)&v1,4);
   memcpy(&packet[6],(unsigned char *)&v2,4);
   memcpy(&packet[10],(unsigned char *)&v3,4);
   
   add_to_buffer(14,packet);
}

extern "C" void process_glTranslatef(const GLfloat v1, const GLfloat v2, const GLfloat v3)
{ 
   if(!_is_in_model_view || !seen_glGetDouble) return;

   unsigned char packet[14];

   packet[0]=13; //size
   packet[1]=8;
 
   memcpy(&packet[2],(unsigned char *)&v1,4);
   memcpy(&packet[6],(unsigned char *)&v2,4);
   memcpy(&packet[10],(unsigned char *)&v3,4);
   
   add_to_buffer(14,packet);
}

extern "C" void process_glRotatef(const GLfloat v1, const GLfloat v2, const GLfloat v3, const GLfloat v4)
{ 
   if(!_is_in_model_view || !seen_glGetDouble ) return;

   unsigned char packet[18];

   packet[0]=17; //size
   packet[1]=9;

   memcpy(&packet[2],(unsigned char *)&v1,4);
   memcpy(&packet[6],(unsigned char *)&v2,4);
   memcpy(&packet[10],(unsigned char *)&v3,4);
   memcpy(&packet[14],(unsigned char *)&v4,4);

   add_to_buffer(18,packet);
}

extern "C" void process_glLoadMatrixf(const GLfloat *v)
{ 
   if(!_is_in_model_view || !seen_glGetDouble ) return;

   unsigned int s=sizeof(GLfloat)*16+2;
   unsigned char packet[s];

   packet[0]=s-1; //size
   packet[1]=10;
   memcpy(&packet[2],v,sizeof(GLfloat)*16);

   add_to_buffer(s,packet);
}

extern "C" void process_glMultMatrixf(const GLfloat *v)
{ 
   if(!_is_in_model_view || !seen_glGetDouble ) return;

   unsigned int s=sizeof(GLfloat)*16+2;
   unsigned char packet[s];

   packet[0]=s-1; //size
   packet[1]=11;
   memcpy(&packet[2],v,sizeof(GLfloat)*16);

   add_to_buffer(s,packet);
}

extern "C" void process_glGenLists (GLsizei range,GLuint result)
{
   if(!_is_in_model_view) return;

   unsigned char packet[10];

   unsigned int r1=range;
   unsigned int r2=result;

   packet[0]=9; //size
   packet[1]=12;
   memcpy(&packet[2],&r1,4);
   memcpy(&packet[6],&r2,4);

   add_to_buffer(10,packet);
}
 

extern "C" void process_glNewList (GLuint list, GLenum mode)
{
   if(!_is_in_model_view) return;

   unsigned char packet[10];

   unsigned int r1=list;
   unsigned int r2=mode;

   packet[0]=9; //size
   packet[1]=13;
   memcpy(&packet[2],&r1,4);
   memcpy(&packet[6],&r2,4);

   add_to_buffer(10,packet);

}

extern "C" void process_glCallList (GLuint list)
{
   if(!_is_in_model_view) return;

   unsigned char packet[6];

   unsigned int r1=list;

   packet[0]=5; //size
   packet[1]=14;
   memcpy(&packet[2],&r1,4);

   add_to_buffer(6,packet);
}

extern "C" void process_glEndList()
{
   if(!_is_in_model_view) return;

   unsigned char packet[2];

   packet[0]=1; //size
   packet[1]=15;

   add_to_buffer(2,packet);
}
