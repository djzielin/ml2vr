/* glwrapper.c                            -*- mode: C; tab-width: 4 -*-
 *
 * GLTrace OpenGL debugger/trace utlility
 * Version:  2.3
 * Copyright (C) 1999-2002
 *		Phil Frisbie, Jr. (phil@hawksoft.com)
 *
 * With original function counting code contributed by
 *		Keith Harrison (keithh@netcomuk.co.uk)
 * GLX wrapper code contributed by
 *		Bert Freudenberg (bert@freudenbergs.de)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gltrace.h"
#include "tcp_functions.h"
#include <GL/glu.h>

extern unsigned int nCallCount[];
extern GLboolean gltraceInit();
extern int GetEXTProcIndex(LPCSTR name);
extern int fontBitmapString(char *str);
extern GLboolean bFSPenable;        /* enable the FSP counter */
extern GLboolean bLogEnable;        /* enable normal logging */
extern GLboolean bDisableExt;       /* disable extensions */
extern GLfloat *fpsColor;           /* color of the FPS counter */

//#include "ext.h"

//
// OpenGL function handlers
//

extern FILE *debug_log;

void GLAPIENTRY glAccum (GLenum op, GLfloat value)
{
	GLV.glAccum (op,value);
}

void GLAPIENTRY glAlphaFunc (GLenum func, GLclampf ref)
{
	GLV.glAlphaFunc (func, ref);
}

GLboolean GLAPIENTRY glAreTexturesResident (GLsizei n, const GLuint *textures, GLboolean *residences)
{
	GLboolean   result;

	result = GLV.glAreTexturesResident(n,textures,residences);

	return result;
}

void GLAPIENTRY glArrayElement (GLint i)
{
	GLV.glArrayElement (i);
}

void GLAPIENTRY glBegin (GLenum mode)
{
   //fprintf(debug_log,"glBegin\n");
	process_glBegin(mode);
	GLV.glBegin (mode);
}

void GLAPIENTRY glBindTexture (GLenum target, GLuint texture)
{
	GLV.glBindTexture (target, texture);
}

void GLAPIENTRY glBitmap (GLsizei width, GLsizei height, GLfloat xorig, GLfloat yorig, GLfloat xmove, GLfloat ymove, const GLubyte *bitmap)
{
	GLV.glBitmap (width, height, xorig, yorig, xmove, ymove, bitmap);
}

void GLAPIENTRY glBlendFunc (GLenum sfactor, GLenum dfactor)
{
	GLV.glBlendFunc (sfactor, dfactor);
}

void GLAPIENTRY glCallList (GLuint list)
{
   process_glCallList(list);
	GLV.glCallList (list);
}

void GLAPIENTRY glCallLists (GLsizei n, GLenum type, const GLvoid *lists)
{
	GLV.glCallLists (n, type, lists);
}

void GLAPIENTRY glClear (GLbitfield mask)
{
	GLV.glClear (mask);
}

void GLAPIENTRY glClearAccum (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)
{
	GLV.glClearAccum (red, green, blue, alpha);
}

void GLAPIENTRY glClearColor (GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha)
{
	GLV.glClearColor (red, green, blue, alpha);
}

void GLAPIENTRY glClearDepth (GLclampd depth)
{
	GLV.glClearDepth (depth);
}

void GLAPIENTRY glClearIndex (GLfloat c)
{
	GLV.glClearIndex (c);
}

void GLAPIENTRY glClearStencil (GLint s)
{
	GLV.glClearStencil (s);
}

void GLAPIENTRY glClipPlane (GLenum plane, const GLdouble *equation)
{
	GLV.glClipPlane (plane, equation);
}

void GLAPIENTRY glColor3b (GLbyte red, GLbyte green, GLbyte blue)
{
	GLV.glColor3b (red, green, blue);
}

void GLAPIENTRY glColor3bv (const GLbyte *v)
{
	GLV.glColor3bv (v);
}

void GLAPIENTRY glColor3d (GLdouble red, GLdouble green, GLdouble blue)
{
	GLV.glColor3d (red, green, blue);
}

void GLAPIENTRY glColor3dv (const GLdouble *v)
{
	GLV.glColor3dv (v);
}

void GLAPIENTRY glColor3f (GLfloat red, GLfloat green, GLfloat blue)
{
  process_glColor4(red, green, blue, 1);
	GLV.glColor3f (red, green, blue);
}

void GLAPIENTRY glColor3fv (const GLfloat *v)
{
	GLV.glColor3fv (v);
  process_glColor4(v[0],v[1],v[2],v[3]);
}

void GLAPIENTRY glColor3i (GLint red, GLint green, GLint blue)
{
	GLV.glColor3i (red, green, blue);
}

void GLAPIENTRY glColor3iv (const GLint *v)
{
	GLV.glColor3iv (v);
}

void GLAPIENTRY glColor3s (GLshort red, GLshort green, GLshort blue)
{
	GLV.glColor3s (red, green, blue);
}

void GLAPIENTRY glColor3sv (const GLshort *v)
{
	GLV.glColor3sv (v);
}

void GLAPIENTRY glColor3ub (GLubyte red, GLubyte green, GLubyte blue)
{
	GLV.glColor3ub (red, green, blue);
}

void GLAPIENTRY glColor3ubv (const GLubyte *v)
{
	GLV.glColor3ubv (v);
}

void GLAPIENTRY glColor3ui (GLuint red, GLuint green, GLuint blue)
{
	GLV.glColor3ui (red, green, blue);
}

void GLAPIENTRY glColor3uiv (const GLuint *v)
{
	GLV.glColor3uiv (v);
}

void GLAPIENTRY glColor3us (GLushort red, GLushort green, GLushort blue)
{
	GLV.glColor3us (red, green, blue);
}

void GLAPIENTRY glColor3usv (const GLushort *v)
{
	GLV.glColor3usv (v);
}

void GLAPIENTRY glColor4b (GLbyte red, GLbyte green, GLbyte blue, GLbyte alpha)
{
	GLV.glColor4b (red, green, blue, alpha);
}

void GLAPIENTRY glColor4bv (const GLbyte *v)
{
	GLV.glColor4bv (v);
}

void GLAPIENTRY glColor4d (GLdouble red, GLdouble green, GLdouble blue, GLdouble alpha)
{
    process_glColor4(red, green, blue, alpha);
	GLV.glColor4d (red, green, blue, alpha);
}

void GLAPIENTRY glColor4dv (const GLdouble *v)
{
	process_glColor4(v[0],v[1],v[2],v[3]);
	GLV.glColor4dv (v);
}

void GLAPIENTRY glColor4f (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)
{
	process_glColor4(red, green, blue, alpha);
	GLV.glColor4f (red, green, blue, alpha);
}

void GLAPIENTRY glColor4fv (const GLfloat *v)
{
	process_glColor4(v[0],v[1],v[2],v[3]);
	GLV.glColor4fv (v);
}

void GLAPIENTRY glColor4i (GLint red, GLint green, GLint blue, GLint alpha)
{
	GLV.glColor4i (red, green, blue, alpha);
}

void GLAPIENTRY glColor4iv (const GLint *v)
{                  
	GLV.glColor4iv (v);
}

void GLAPIENTRY glColor4s (GLshort red, GLshort green, GLshort blue, GLshort alpha)
{
	GLV.glColor4s (red, green, blue, alpha);
}

void GLAPIENTRY glColor4sv (const GLshort *v)
{
	GLV.glColor4sv (v);
}

void GLAPIENTRY glColor4ub (GLubyte red, GLubyte green, GLubyte blue, GLubyte alpha)
{
	GLV.glColor4ub (red, green, blue, alpha);
}

void GLAPIENTRY glColor4ubv (const GLubyte *v)
{
	GLV.glColor4ubv (v);
}

void GLAPIENTRY glColor4ui (GLuint red, GLuint green, GLuint blue, GLuint alpha)
{
	GLV.glColor4ui (red, green, blue, alpha);
}

void GLAPIENTRY glColor4uiv (const GLuint *v)
{
	GLV.glColor4uiv (v);
}

void GLAPIENTRY glColor4us (GLushort red, GLushort green, GLushort blue, GLushort alpha)
{
	GLV.glColor4us (red, green, blue, alpha);
}

void GLAPIENTRY glColor4usv (const GLushort *v)
{
	GLV.glColor4usv (v);
}

void GLAPIENTRY glColorMask (GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)
{
	GLV.glColorMask (red, green, blue, alpha);
}

void GLAPIENTRY glColorMaterial (GLenum face, GLenum mode)
{
	GLV.glColorMaterial (face, mode);
}

void GLAPIENTRY glColorPointer (GLint size, GLenum type, GLsizei stride, const GLvoid *pointer)
{
	GLV.glColorPointer (size, type, stride, pointer);
}

void GLAPIENTRY glCopyPixels (GLint x, GLint y, GLsizei width, GLsizei height, GLenum type)
{
	GLV.glCopyPixels (x, y, width, height, type);
}

void GLAPIENTRY glCopyTexImage1D (GLenum target, GLint level, GLenum internalFormat, GLint x, GLint y, GLsizei width, GLint border)
{
	GLV.glCopyTexImage1D (target, level, internalFormat, x, y, width, border);
}

void GLAPIENTRY glCopyTexImage2D (GLenum target, GLint level, GLenum internalFormat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border)
{
	GLV.glCopyTexImage2D (target, level, internalFormat, x, y, width, height, border);
}

void GLAPIENTRY glCopyTexSubImage1D (GLenum target, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width)
{
	GLV.glCopyTexSubImage1D (target, level, xoffset, x, y, width);
}

void GLAPIENTRY glCopyTexSubImage2D (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height)
{
	GLV.glCopyTexSubImage2D (target, level, xoffset, yoffset, x, y, width, height);
}

void GLAPIENTRY glCullFace (GLenum mode)
{
	GLV.glCullFace (mode);
}

void GLAPIENTRY glDeleteLists (GLuint list, GLsizei range)
{
	GLV.glDeleteLists (list, range);
}

void GLAPIENTRY glDeleteTextures (GLsizei n, const GLuint *textures)
{
	GLV.glDeleteTextures (n, textures);
}

void GLAPIENTRY glDepthFunc (GLenum func)
{
	GLV.glDepthFunc (func);
}

void GLAPIENTRY glDepthMask (GLboolean flag)
{
	GLV.glDepthMask (flag);
}

void GLAPIENTRY glDepthRange (GLclampd zNear, GLclampd zFar)
{
	GLV.glDepthRange (zNear, zFar);
}

void GLAPIENTRY glDisable (GLenum cap)
{
	GLV.glDisable (cap);
}

void GLAPIENTRY glDisableClientState (GLenum array)
{
	GLV.glDisableClientState (array);
}

void GLAPIENTRY glDrawArrays (GLenum mode, GLint first, GLsizei count)
{
	GLV.glDrawArrays (mode, first, count);
}

void GLAPIENTRY glDrawBuffer (GLenum mode)
{
	GLV.glDrawBuffer (mode);
}

void GLAPIENTRY glDrawElements (GLenum mode, GLsizei count, GLenum type, const GLvoid *indices)
{
	GLV.glDrawElements (mode, count, type, indices);
}

void GLAPIENTRY glDrawPixels (GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels)
{
	GLV.glDrawPixels (width, height, format, type, pixels);
}

void GLAPIENTRY glEdgeFlag (GLboolean flag)
{
	GLV.glEdgeFlag (flag);
}

void GLAPIENTRY glEdgeFlagPointer (GLsizei stride, const GLvoid *pointer)
{
	GLV.glEdgeFlagPointer (stride, pointer);
}

void GLAPIENTRY glEdgeFlagv (const GLboolean *flag)
{
	GLV.glEdgeFlagv (flag);
}

void GLAPIENTRY glEnable (GLenum cap)
{
	GLV.glEnable (cap);
}

void GLAPIENTRY glEnableClientState (GLenum array)
{
	GLV.glEnableClientState (array);
}

void GLAPIENTRY glEnd ()
{
	process_glEnd();
	GLV.glEnd();
}

void GLAPIENTRY glEndList ()
{
	GLV.glEndList();
   process_glEndList();
}

void GLAPIENTRY glEvalCoord1d (GLdouble u)
{
	GLV.glEvalCoord1d (u);
}

void GLAPIENTRY glEvalCoord1dv (const GLdouble *u)
{
	GLV.glEvalCoord1dv (u);
}

void GLAPIENTRY glEvalCoord1f (GLfloat u)
{
	GLV.glEvalCoord1f (u);
}

void GLAPIENTRY glEvalCoord1fv (const GLfloat *u)
{
	GLV.glEvalCoord1fv (u);
}

void GLAPIENTRY glEvalCoord2d (GLdouble u, GLdouble v)
{
	GLV.glEvalCoord2d (u,v);
}

void GLAPIENTRY glEvalCoord2dv (const GLdouble *u)
{
	GLV.glEvalCoord2dv (u);
}

void GLAPIENTRY glEvalCoord2f (GLfloat u, GLfloat v)
{
	GLV.glEvalCoord2f (u,v);
}

void GLAPIENTRY glEvalCoord2fv (const GLfloat *u)
{
	GLV.glEvalCoord2fv (u);
}

void GLAPIENTRY glEvalMesh1 (GLenum mode, GLint i1, GLint i2)
{
	GLV.glEvalMesh1 (mode, i1, i2);
}

void GLAPIENTRY glEvalMesh2 (GLenum mode, GLint i1, GLint i2, GLint j1, GLint j2)
{
	GLV.glEvalMesh2 (mode, i1, i2, j1, j2);
}

void GLAPIENTRY glEvalPoint1 (GLint i)
{
	GLV.glEvalPoint1 (i);
}

void GLAPIENTRY glEvalPoint2 (GLint i, GLint j)
{
	GLV.glEvalPoint2 (i, j);
}

void GLAPIENTRY glFeedbackBuffer (GLsizei size, GLenum type, GLfloat *buffer)
{
	GLV.glFeedbackBuffer (size, type, buffer);
}

void GLAPIENTRY glFinish ()
{
	GLV.glFinish();
}

void GLAPIENTRY glFlush ()
{
	GLV.glFlush();
}

void GLAPIENTRY glFogf (GLenum pname, GLfloat param)
{
	GLV.glFogf (pname, param);
}

void GLAPIENTRY glFogfv (GLenum pname, const GLfloat *params)
{
	GLV.glFogfv (pname, params);
}

void GLAPIENTRY glFogi (GLenum pname, GLint param)
{
	GLV.glFogi (pname, param);
}

void GLAPIENTRY glFogiv (GLenum pname, const GLint *params)
{
	GLV.glFogiv (pname, params);
}

void GLAPIENTRY glFrontFace (GLenum mode)
{
	GLV.glFrontFace (mode);
}

void GLAPIENTRY glFrustum (GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar)
{
	GLV.glFrustum (left, right, bottom, top, zNear, zFar);
}

GLuint GLAPIENTRY glGenLists (GLsizei range)
{
	GLuint result;

    result = GLV.glGenLists(range);

   process_glGenLists(range,result);
	return result;
}

void GLAPIENTRY glGenTextures (GLsizei n, GLuint *textures)
{
	GLV.glGenTextures (n, textures);
}

void GLAPIENTRY glGetBooleanv (GLenum pname, GLboolean *params)
{
	GLV.glGetBooleanv (pname, params);
}

void GLAPIENTRY glGetClipPlane (GLenum plane, GLdouble *equation)
{
	GLV.glGetClipPlane (plane, equation);
}

void GLAPIENTRY glGetDoublev (GLenum pname, GLdouble *params)
{
    process_glGetDoublev();
	GLV.glGetDoublev (pname, params);
}

GLenum GLAPIENTRY glGetError ()
{
	GLenum result;

    result = GLV.glGetError();
	return result;
}

void GLAPIENTRY glGetFloatv (GLenum pname, GLfloat *params)
{
	GLV.glGetFloatv (pname, params);
}

void GLAPIENTRY glGetIntegerv (GLenum pname, GLint *params)
{
   /*fprintf(debug_log,"MATLAB wants to know an integer value of: 0x%X\n",pname);
   if(pname==0x821B)
     fprintf(debug_log,"  MATLAB wants to know GL_MAJOR_VERSION\n");
   if(pname==0x821C)
     fprintf(debug_log,"  MATLAB wants to know GL_MINOR_VERSION\n");
	*/
   GLV.glGetIntegerv (pname, params);
   /*if(pname==0x821B) //if we want to overtide reported version of opengl
   {
      fprintf(debug_log,"  value is: %d\n",params[0]);
      fprintf(debug_log,"  however we will hijack\n");
     
      params[0]=1;
      fprintf(debug_log,"  value is now: %d\n",params[0]);
   }
   if(pname==0x821C)
   {
      fprintf(debug_log,"  value is: %d\n",params[0]);
      fprintf(debug_log,"  however we will hijack\n");
      params[0]=1;
      fprintf(debug_log,"  value is now: %d\n",params[0]);

   }
   fflush(debug_log);
   */
}

void GLAPIENTRY glGetLightfv (GLenum light, GLenum pname, GLfloat *params)
{
	GLV.glGetLightfv (light, pname, params);
}

void GLAPIENTRY glGetLightiv (GLenum light, GLenum pname, GLint *params)
{
	GLV.glGetLightiv (light, pname, params);
}

void GLAPIENTRY glGetMapdv (GLenum target, GLenum query, GLdouble *v)
{
	GLV.glGetMapdv (target, query, v);
}

void GLAPIENTRY glGetMapfv (GLenum target, GLenum query, GLfloat *v)
{
	GLV.glGetMapfv (target, query, v);
}

void GLAPIENTRY glGetMapiv (GLenum target, GLenum query, GLint *v)
{
	GLV.glGetMapiv (target, query, v);
}

void GLAPIENTRY glGetMaterialfv (GLenum face, GLenum pname, GLfloat *params)
{
	GLV.glGetMaterialfv (face, pname, params);
}

void GLAPIENTRY glGetMaterialiv (GLenum face, GLenum pname, GLint *params)
{
	GLV.glGetMaterialiv (face, pname, params);
}

void GLAPIENTRY glGetPixelMapfv (GLenum map, GLfloat *values)
{
	GLV.glGetPixelMapfv (map, values);
}

void GLAPIENTRY glGetPixelMapuiv (GLenum map, GLuint *values)
{
	GLV.glGetPixelMapuiv (map, values);
}

void GLAPIENTRY glGetPixelMapusv (GLenum map, GLushort *values)
{
	GLV.glGetPixelMapusv (map, values);
}

void GLAPIENTRY glGetPointerv (GLenum pname, GLvoid* *params)
{
	GLV.glGetPointerv (pname, params);
}

void GLAPIENTRY glGetPolygonStipple (GLubyte *mask)
{
	GLV.glGetPolygonStipple (mask);
}

const GLubyte* GLAPIENTRY glGetString (GLenum name)
{
	const GLubyte* result;
   
   //fprintf(debug_log,"MATLAB is interested in a string: 0x%X\n",name);
   //fflush(debug_log);

	result = GLV.glGetString(name);
   
   /*fprintf(debug_log,"  result is: %s\n",result);
   fflush(debug_log);
   
   if(name== 7936)
      return ""; //"Duke University"; //if we want to override
   if(name==7937)
      return ""; //"ML2VR Interception Driver";
   if(name==7938)
       return "1.1.0";
   if(name==7939)
       return ""; //EXT and ARB
 */

	return result;
}

void GLAPIENTRY glGetTexEnvfv (GLenum target, GLenum pname, GLfloat *params)
{
	GLV.glGetTexEnvfv (target, pname, params);
}

void GLAPIENTRY glGetTexEnviv (GLenum target, GLenum pname, GLint *params)
{
	GLV.glGetTexEnviv (target, pname, params);
}

void GLAPIENTRY glGetTexGendv (GLenum coord, GLenum pname, GLdouble *params)
{
	GLV.glGetTexGendv (coord, pname, params);
}

void GLAPIENTRY glGetTexGenfv (GLenum coord, GLenum pname, GLfloat *params)
{
	GLV.glGetTexGenfv (coord, pname, params);
}

void GLAPIENTRY glGetTexGeniv (GLenum coord, GLenum pname, GLint *params)
{
	GLV.glGetTexGeniv (coord, pname, params);
}

void GLAPIENTRY glGetTexImage (GLenum target, GLint level, GLenum format, GLenum type, GLvoid *pixels)
{
	GLV.glGetTexImage (target, level, format, type, pixels);
}

void GLAPIENTRY glGetTexLevelParameterfv (GLenum target, GLint level, GLenum pname, GLfloat *params)
{
	GLV.glGetTexLevelParameterfv (target, level, pname, params);
}

void GLAPIENTRY glGetTexLevelParameteriv (GLenum target, GLint level, GLenum pname, GLint *params)
{
	GLV.glGetTexLevelParameteriv (target, level, pname, params);
}

void GLAPIENTRY glGetTexParameterfv (GLenum target, GLenum pname, GLfloat *params)
{
	GLV.glGetTexParameterfv (target, pname, params);
}

void GLAPIENTRY glGetTexParameteriv (GLenum target, GLenum pname, GLint *params)
{
	GLV.glGetTexParameteriv (target, pname, params);
}

void GLAPIENTRY glHint (GLenum target, GLenum mode)
{
	GLV.glHint (target, mode);
}

void GLAPIENTRY glIndexMask (GLuint mask)
{
	GLV.glIndexMask (mask);
}

void GLAPIENTRY glIndexPointer (GLenum type, GLsizei stride, const GLvoid *pointer)
{
	GLV.glIndexPointer (type, stride, pointer);
}

void GLAPIENTRY glIndexd (GLdouble c)
{
	GLV.glIndexd (c);
}

void GLAPIENTRY glIndexdv (const GLdouble *c)
{
	GLV.glIndexdv (c);
}

void GLAPIENTRY glIndexf (GLfloat c)
{
	GLV.glIndexf (c);
}

void GLAPIENTRY glIndexfv (const GLfloat *c)
{
	GLV.glIndexfv (c);
}

void GLAPIENTRY glIndexi (GLint c)
{
	GLV.glIndexi (c);
}

void GLAPIENTRY glIndexiv (const GLint *c)
{
	GLV.glIndexiv (c);
}

void GLAPIENTRY glIndexs (GLshort c)
{
	GLV.glIndexs (c);
}

void GLAPIENTRY glIndexsv (const GLshort *c)
{
	GLV.glIndexsv (c);
}

void GLAPIENTRY glIndexub (GLubyte c)
{
	GLV.glIndexub (c);
}

void GLAPIENTRY glIndexubv (const GLubyte *c)
{
	GLV.glIndexubv (c);
}

void GLAPIENTRY glInitNames ()
{
	GLV.glInitNames();
}

void GLAPIENTRY glInterleavedArrays (GLenum format, GLsizei stride, const GLvoid *pointer)
{
	GLV.glInterleavedArrays (format, stride, pointer);
}

GLboolean GLAPIENTRY glIsEnabled (GLenum cap)
{
	GLboolean result;

	result = GLV.glIsEnabled(cap);
	return result;
}

GLboolean GLAPIENTRY glIsList (GLuint list)
{
	GLboolean result;

	result = GLV.glIsList(list);
	return result;
}

GLboolean GLAPIENTRY glIsTexture (GLuint texture)
{
	GLboolean result;

	result = GLV.glIsTexture(texture);
	return result;
}

void GLAPIENTRY glLightModelf (GLenum pname, GLfloat param)
{
	GLV.glLightModelf (pname, param);
}

void GLAPIENTRY glLightModelfv (GLenum pname, const GLfloat *params)
{
	GLV.glLightModelfv (pname,params);
}

void GLAPIENTRY glLightModeli (GLenum pname, GLint param)
{
	GLV.glLightModeli (pname, param);
}

void GLAPIENTRY glLightModeliv (GLenum pname, const GLint *params)
{
	GLV.glLightModeliv (pname,params);
}

void GLAPIENTRY glLightf (GLenum light, GLenum pname, GLfloat param)
{
	GLV.glLightf (light, pname, param);
}

void GLAPIENTRY glLightfv (GLenum light, GLenum pname, const GLfloat *params)
{
	GLV.glLightfv (light, pname, params);
}

void GLAPIENTRY glLighti (GLenum light, GLenum pname, GLint param)
{
	GLV.glLighti (light, pname, param);
}

void GLAPIENTRY glLightiv (GLenum light, GLenum pname, const GLint *params)
{
	GLV.glLightiv (light, pname, params);
}

void GLAPIENTRY glLineStipple (GLint factor, GLushort pattern)
{
	GLV.glLineStipple (factor, pattern);
}

void GLAPIENTRY glLineWidth (GLfloat width)
{
	GLV.glLineWidth (width);
}

void GLAPIENTRY glListBase (GLuint base)
{
	GLV.glListBase (base);
}

void GLAPIENTRY glLoadIdentity ()
{
   process_glLoadIdentity();
	GLV.glLoadIdentity();
}

void GLAPIENTRY glLoadMatrixd (const GLdouble *m)
{
   GLfloat v[16];
   int i;
   for(i=0;i<16;i++)
      v[i]=m[i];

   process_glLoadMatrixf(v);
	GLV.glLoadMatrixd(m);
}

void GLAPIENTRY glLoadMatrixf (const GLfloat *m)
{
   process_glLoadMatrixf(m);
	GLV.glLoadMatrixf (m);
}

void GLAPIENTRY glLoadName (GLuint name)
{
	GLV.glLoadName (name);
}

void GLAPIENTRY glLogicOp (GLenum opcode)
{
	GLV.glLogicOp (opcode);
}

void GLAPIENTRY glMap1d (GLenum target, GLdouble u1, GLdouble u2, GLint stride, GLint order, const GLdouble *points)
{
	GLV.glMap1d (target, u1, u2, stride, order, points);
}

void GLAPIENTRY glMap1f (GLenum target, GLfloat u1, GLfloat u2, GLint stride, GLint order, const GLfloat *points)
{
	GLV.glMap1f (target, u1, u2, stride, order, points);
}

void GLAPIENTRY glMap2d (GLenum target, GLdouble u1, GLdouble u2, GLint ustride, GLint uorder, GLdouble v1, GLdouble v2, GLint vstride, GLint vorder, const GLdouble *points)
{
	GLV.glMap2d (target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points);
}

void GLAPIENTRY glMap2f (GLenum target, GLfloat u1, GLfloat u2, GLint ustride, GLint uorder, GLfloat v1, GLfloat v2, GLint vstride, GLint vorder, const GLfloat *points)
{
	GLV.glMap2f (target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points);
}

void GLAPIENTRY glMapGrid1d (GLint un, GLdouble u1, GLdouble u2)
{
	GLV.glMapGrid1d (un, u1, u2);
}

void GLAPIENTRY glMapGrid1f (GLint un, GLfloat u1, GLfloat u2)
{
	GLV.glMapGrid1f (un, u1, u2);
}

void GLAPIENTRY glMapGrid2d (GLint un, GLdouble u1, GLdouble u2, GLint vn, GLdouble v1, GLdouble v2)
{
	GLV.glMapGrid2d (un, u1, u2, vn, v1, v2);
}

void GLAPIENTRY glMapGrid2f (GLint un, GLfloat u1, GLfloat u2, GLint vn, GLfloat v1, GLfloat v2)
{
	GLV.glMapGrid2f (un, u1, u2, vn, v1, v2);
}

void GLAPIENTRY glMaterialf (GLenum face, GLenum pname, GLfloat param)
{
	GLV.glMaterialf (face, pname, param);
}

void GLAPIENTRY glMaterialfv (GLenum face, GLenum pname, const GLfloat *params)
{
	GLV.glMaterialfv (face, pname, params);
}

void GLAPIENTRY glMateriali (GLenum face, GLenum pname, GLint param)
{
	GLV.glMateriali (face, pname, param);
}

void GLAPIENTRY glMaterialiv (GLenum face, GLenum pname, const GLint *params)
{
	GLV.glMaterialiv (face, pname, params);
}

void GLAPIENTRY glMatrixMode (GLenum mode)
{
	process_glMatrixMode(mode);
	GLV.glMatrixMode (mode);
}

void GLAPIENTRY glMultMatrixd (const GLdouble *m)
{
   GLfloat v[16];
   int i;
   for(i=0;i<16;i++)
      v[i]=m[i];

   process_glMultMatrixf(v);

	GLV.glMultMatrixd (m);
}

void GLAPIENTRY glMultMatrixf (const GLfloat *m)
{
   process_glMultMatrixf(m);
	GLV.glMultMatrixf (m);
}

void GLAPIENTRY glNewList (GLuint list, GLenum mode)
{
	GLV.glNewList (list,mode);
   process_glNewList(list,mode);
}

void GLAPIENTRY glNormal3b (GLbyte nx, GLbyte ny, GLbyte nz)
{
	GLV.glNormal3b (nx, ny, nz);
}

void GLAPIENTRY glNormal3bv (const GLbyte *v)
{
	GLV.glNormal3bv (v);
}

void GLAPIENTRY glNormal3d (GLdouble nx, GLdouble ny, GLdouble nz)
{
	GLV.glNormal3d (nx, ny, nz);
}

void GLAPIENTRY glNormal3dv (const GLdouble *v)
{
	GLV.glNormal3dv (v);
}

void GLAPIENTRY glNormal3f (GLfloat nx, GLfloat ny, GLfloat nz)
{
	GLV.glNormal3f (nx, ny, nz);
}

void GLAPIENTRY glNormal3fv (const GLfloat *v)
{
	GLV.glNormal3fv (v);
}

void GLAPIENTRY glNormal3i (GLint nx, GLint ny, GLint nz)
{
	GLV.glNormal3i (nx, ny, nz);
}

void GLAPIENTRY glNormal3iv (const GLint *v)
{
	GLV.glNormal3iv (v);
}

void GLAPIENTRY glNormal3s (GLshort nx, GLshort ny, GLshort nz)
{
	GLV.glNormal3s (nx, ny, nz);
}

void GLAPIENTRY glNormal3sv (const GLshort *v)
{
	GLV.glNormal3sv (v);
}

void GLAPIENTRY glNormalPointer (GLenum type, GLsizei stride, const GLvoid *pointer)
{
	GLV.glNormalPointer (type, stride, pointer);
}

void GLAPIENTRY glOrtho (GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar)
{
	GLV.glOrtho (left, right, bottom, top, zNear, zFar);
}

void GLAPIENTRY glPassThrough (GLfloat token)
{
	GLV.glPassThrough (token);
}

void GLAPIENTRY glPixelMapfv (GLenum map, GLsizei mapsize, const GLfloat *values)
{
    int i;

    GLV.glPixelMapfv (map, mapsize, values);
}

void GLAPIENTRY glPixelMapuiv (GLenum map, GLsizei mapsize, const GLuint *values)
{
    int i;

	GLV.glPixelMapuiv (map, mapsize, values);
}

void GLAPIENTRY glPixelMapusv (GLenum map, GLsizei mapsize, const GLushort *values)
{
    int i;

	GLV.glPixelMapusv (map, mapsize, values);
}

void GLAPIENTRY glPixelStoref (GLenum pname, GLfloat param)
{
	GLV.glPixelStoref (pname, param);
}

void GLAPIENTRY glPixelStorei (GLenum pname, GLint param)
{
	GLV.glPixelStorei (pname, param);
}

void GLAPIENTRY glPixelTransferf (GLenum pname, GLfloat param)
{
	GLV.glPixelTransferf (pname, param);
}

void GLAPIENTRY glPixelTransferi (GLenum pname, GLint param)
{
	GLV.glPixelTransferi (pname, param);
}

void GLAPIENTRY glPixelZoom (GLfloat xfactor, GLfloat yfactor)
{
	GLV.glPixelZoom (xfactor, yfactor);
}

void GLAPIENTRY glPointSize (GLfloat size)
{
	GLV.glPointSize (size);
}

void GLAPIENTRY glPolygonMode (GLenum face, GLenum mode)
{
	GLV.glPolygonMode (face, mode);
}

void GLAPIENTRY glPolygonOffset (GLfloat factor, GLfloat units)
{
	GLV.glPolygonOffset (factor, units);
}

void GLAPIENTRY glPolygonStipple (const GLubyte *mask)
{
	GLV.glPolygonStipple (mask);
}

void GLAPIENTRY glPopAttrib ()
{
	GLV.glPopAttrib();
}

void GLAPIENTRY glPopClientAttrib ()
{
	GLV.glPopClientAttrib();
}

void GLAPIENTRY glPopMatrix ()
{
   process_glPopMatrix();
	GLV.glPopMatrix();
}

void GLAPIENTRY glPopName ()
{
	GLV.glPopName();
}

void GLAPIENTRY glPrioritizeTextures (GLsizei n, const GLuint *textures, const GLclampf *priorities)
{
    int i;

	GLV.glPrioritizeTextures (n, textures, priorities);
}

void GLAPIENTRY glPushAttrib (GLbitfield mask)
{
	GLV.glPushAttrib (mask);
}

void GLAPIENTRY glPushClientAttrib (GLbitfield mask)
{
	GLV.glPushClientAttrib (mask);
}

void GLAPIENTRY glPushMatrix ()
{
   process_glPushMatrix();
	GLV.glPushMatrix();
}

void GLAPIENTRY glPushName (GLuint name)
{
	GLV.glPushName (name);
}

void GLAPIENTRY glRasterPos2d (GLdouble x, GLdouble y)
{
	GLV.glRasterPos2d (x, y);
}

void GLAPIENTRY glRasterPos2dv (const GLdouble *v)
{
	GLV.glRasterPos2dv (v);
}

void GLAPIENTRY glRasterPos2f (GLfloat x, GLfloat y)
{
	GLV.glRasterPos2f (x, y);
}

void GLAPIENTRY glRasterPos2fv (const GLfloat *v)
{
	GLV.glRasterPos2fv (v);
}

void GLAPIENTRY glRasterPos2i (GLint x, GLint y)
{
	GLV.glRasterPos2i (x, y);
}

void GLAPIENTRY glRasterPos2iv (const GLint *v)
{
	GLV.glRasterPos2iv (v);
}

void GLAPIENTRY glRasterPos2s (GLshort x, GLshort y)
{
	GLV.glRasterPos2s (x, y);
}

void GLAPIENTRY glRasterPos2sv (const GLshort *v)
{
	GLV.glRasterPos2sv (v);
}

void GLAPIENTRY glRasterPos3d (GLdouble x, GLdouble y, GLdouble z)
{
	GLV.glRasterPos3d (x, y, z);
}

void GLAPIENTRY glRasterPos3dv (const GLdouble *v)
{
	GLV.glRasterPos3dv (v);
}

void GLAPIENTRY glRasterPos3f (GLfloat x, GLfloat y, GLfloat z)
{
	GLV.glRasterPos3f (x, y, z);
}

void GLAPIENTRY glRasterPos3fv (const GLfloat *v)
{
	GLV.glRasterPos3fv (v);
}

void GLAPIENTRY glRasterPos3i (GLint x, GLint y, GLint z)
{
	GLV.glRasterPos3i (x, y, z);
}

void GLAPIENTRY glRasterPos3iv (const GLint *v)
{
	GLV.glRasterPos3iv (v);
}

void GLAPIENTRY glRasterPos3s (GLshort x, GLshort y, GLshort z)
{
	GLV.glRasterPos3s (x, y, z);
}

void GLAPIENTRY glRasterPos3sv (const GLshort *v)
{
	GLV.glRasterPos3sv (v);
}

void GLAPIENTRY glRasterPos4d (GLdouble x, GLdouble y, GLdouble z, GLdouble w)
{
	GLV.glRasterPos4d (x, y, z, w);
}

void GLAPIENTRY glRasterPos4dv (const GLdouble *v)
{
	GLV.glRasterPos4dv (v);
}

void GLAPIENTRY glRasterPos4f (GLfloat x, GLfloat y, GLfloat z, GLfloat w)
{
	GLV.glRasterPos4f (x, y, z, w);
}

void GLAPIENTRY glRasterPos4fv (const GLfloat *v)
{
	GLV.glRasterPos4fv (v);
}

void GLAPIENTRY glRasterPos4i (GLint x, GLint y, GLint z, GLint w)
{
	GLV.glRasterPos4i (x, y, z, w);
}

void GLAPIENTRY glRasterPos4iv (const GLint *v)
{
	GLV.glRasterPos4iv (v);
}

void GLAPIENTRY glRasterPos4s (GLshort x, GLshort y, GLshort z, GLshort w)
{
	GLV.glRasterPos4s (x, y, z, w);
}

void GLAPIENTRY glRasterPos4sv (const GLshort *v)
{
	GLV.glRasterPos4sv (v);
}

void GLAPIENTRY glReadBuffer (GLenum mode)
{
	GLV.glReadBuffer (mode);
}

void GLAPIENTRY glReadPixels (GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid *pixels)
{
	GLV.glReadPixels (x, y, width, height, format, type, pixels);
}

void GLAPIENTRY glRectd (GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2)
{
	GLV.glRectd (x1, y1, x2, y2);
}

void GLAPIENTRY glRectdv (const GLdouble *v1, const GLdouble *v2)
{
	GLV.glRectdv (v1, v2);
}

void GLAPIENTRY glRectf (GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2)
{
	GLV.glRectf (x1, y1, x2, y2);
}

void GLAPIENTRY glRectfv (const GLfloat *v1, const GLfloat *v2)
{
	GLV.glRectfv (v1, v2);
}

void GLAPIENTRY glRecti (GLint x1, GLint y1, GLint x2, GLint y2)
{
	GLV.glRecti (x1, y1, x2, y2);
}

void GLAPIENTRY glRectiv (const GLint *v1, const GLint *v2)
{
	GLV.glRectiv (v1, v2);
}

void GLAPIENTRY glRects (GLshort x1, GLshort y1, GLshort x2, GLshort y2)
{
	GLV.glRects (x1, y1, x2, y2);
}

void GLAPIENTRY glRectsv (const GLshort *v1, const GLshort *v2)
{
	GLV.glRectsv (v1, v2);
}

GLint GLAPIENTRY glRenderMode (GLenum mode)
{
	GLint result;
   process_glRenderMode(mode);
	result = GLV.glRenderMode(mode);
	return result;
}

void GLAPIENTRY glRotated (GLdouble angle, GLdouble x, GLdouble y, GLdouble z)
{
   process_glRotatef(angle,x,y,z);
	GLV.glRotated (angle, x, y, z);
}

void GLAPIENTRY glRotatef (GLfloat angle, GLfloat x, GLfloat y, GLfloat z)
{
   process_glRotatef(angle,x,y,z);
	GLV.glRotatef (angle, x, y, z);
}

void GLAPIENTRY glScaled (GLdouble x, GLdouble y, GLdouble z)
{
   process_glScalef(x,y,z);
	GLV.glScaled (x, y, z);
}

void GLAPIENTRY glScalef (GLfloat x, GLfloat y, GLfloat z)
{
   process_glScalef(x,y,z);
	GLV.glScalef (x, y, z);
}

void GLAPIENTRY glScissor (GLint x, GLint y, GLsizei width, GLsizei height)
{
	GLV.glScissor (x, y, width, height);
}

void GLAPIENTRY glSelectBuffer (GLsizei size, GLuint *buffer)
{
	GLV.glSelectBuffer (size, buffer);
}

void GLAPIENTRY glShadeModel (GLenum mode)
{
	GLV.glShadeModel (mode);
}

void GLAPIENTRY glStencilFunc (GLenum func, GLint ref, GLuint mask)
{
	GLV.glStencilFunc (func, ref, mask);
}

void GLAPIENTRY glStencilMask (GLuint mask)
{
	GLV.glStencilMask (mask);
}

void GLAPIENTRY glStencilOp (GLenum fail, GLenum zfail, GLenum zpass)
{
	useZeroOne = GL_TRUE;
	GLV.glStencilOp (fail, zfail, zpass);
	useZeroOne = GL_FALSE;
}

void GLAPIENTRY glTexCoord1d (GLdouble s)
{
	GLV.glTexCoord1d (s);
}

void GLAPIENTRY glTexCoord1dv (const GLdouble *v)
{
	GLV.glTexCoord1dv (v);
}

void GLAPIENTRY glTexCoord1f (GLfloat s)
{
	GLV.glTexCoord1f (s);
}

void GLAPIENTRY glTexCoord1fv (const GLfloat *v)
{
	GLV.glTexCoord1fv (v);
}

void GLAPIENTRY glTexCoord1i (GLint s)
{
	GLV.glTexCoord1i (s);
}

void GLAPIENTRY glTexCoord1iv (const GLint *v)
{
	GLV.glTexCoord1iv (v);
}

void GLAPIENTRY glTexCoord1s (GLshort s)
{
	GLV.glTexCoord1s (s);
}

void GLAPIENTRY glTexCoord1sv (const GLshort *v)
{
	GLV.glTexCoord1sv (v);
}

void GLAPIENTRY glTexCoord2d (GLdouble s, GLdouble t)
{
	GLV.glTexCoord2d (s, t);
}

void GLAPIENTRY glTexCoord2dv (const GLdouble *v)
{
	GLV.glTexCoord2dv (v);
}

void GLAPIENTRY glTexCoord2f (GLfloat s, GLfloat t)
{
	GLV.glTexCoord2f (s, t);
}

void GLAPIENTRY glTexCoord2fv (const GLfloat *v)
{
	GLV.glTexCoord2fv (v);
}

void GLAPIENTRY glTexCoord2i (GLint s, GLint t)
{
	GLV.glTexCoord2i (s, t);
}

void GLAPIENTRY glTexCoord2iv (const GLint *v)
{
	GLV.glTexCoord2iv (v);
}

void GLAPIENTRY glTexCoord2s (GLshort s, GLshort t)
{
	GLV.glTexCoord2s (s, t);
}

void GLAPIENTRY glTexCoord2sv (const GLshort *v)
{
	GLV.glTexCoord2sv (v);
}

void GLAPIENTRY glTexCoord3d (GLdouble s, GLdouble t, GLdouble r)
{
	GLV.glTexCoord3d (s, t, r);
}

void GLAPIENTRY glTexCoord3dv (const GLdouble *v)
{
	GLV.glTexCoord3dv (v);
}

void GLAPIENTRY glTexCoord3f (GLfloat s, GLfloat t, GLfloat r)
{
	GLV.glTexCoord3f (s, t, r);
}

void GLAPIENTRY glTexCoord3fv (const GLfloat *v)
{
	GLV.glTexCoord3fv (v);
}

void GLAPIENTRY glTexCoord3i (GLint s, GLint t, GLint r)
{
	GLV.glTexCoord3i (s, t, r);
}

void GLAPIENTRY glTexCoord3iv (const GLint *v)
{
	GLV.glTexCoord3iv (v);
}

void GLAPIENTRY glTexCoord3s (GLshort s, GLshort t, GLshort r)
{
	GLV.glTexCoord3s (s, t, r);
}

void GLAPIENTRY glTexCoord3sv (const GLshort *v)
{
	GLV.glTexCoord3sv (v);
}

void GLAPIENTRY glTexCoord4d (GLdouble s, GLdouble t, GLdouble r, GLdouble q)
{
	GLV.glTexCoord4d (s, t, r, q);
}

void GLAPIENTRY glTexCoord4dv (const GLdouble *v)
{
	GLV.glTexCoord4dv (v);
}

void GLAPIENTRY glTexCoord4f (GLfloat s, GLfloat t, GLfloat r, GLfloat q)
{
	GLV.glTexCoord4f (s, t, r, q);
}

void GLAPIENTRY glTexCoord4fv (const GLfloat *v)
{
	GLV.glTexCoord4fv (v);
}

void GLAPIENTRY glTexCoord4i (GLint s, GLint t, GLint r, GLint q)
{
	GLV.glTexCoord4i (s, t, r, q);
}

void GLAPIENTRY glTexCoord4iv (const GLint *v)
{
	GLV.glTexCoord4iv (v);
}

void GLAPIENTRY glTexCoord4s (GLshort s, GLshort t, GLshort r, GLshort q)
{
	GLV.glTexCoord4s (s, t, r, q);
}

void GLAPIENTRY glTexCoord4sv (const GLshort *v)
{
	GLV.glTexCoord4sv (v);
}

void GLAPIENTRY glTexCoordPointer (GLint size, GLenum type, GLsizei stride, const GLvoid *pointer)
{
	GLV.glTexCoordPointer (size, type, stride, pointer);
}

void GLAPIENTRY glTexEnvf (GLenum target, GLenum pname, GLfloat param)
{
    GLenum p = (GLenum)param;

	GLV.glTexEnvf (target, pname, param);
}

void GLAPIENTRY glTexEnvfv (GLenum target, GLenum pname, const GLfloat *params)
{
	GLV.glTexEnvfv (target, pname, params);
}

void GLAPIENTRY glTexEnvi (GLenum target, GLenum pname, GLint param)
{
    GLenum p = (GLenum)param;

	GLV.glTexEnvi (target, pname, param);
}

void GLAPIENTRY glTexEnviv (GLenum target, GLenum pname, const GLint *params)
{
	GLV.glTexEnviv (target, pname, params);
}

void GLAPIENTRY glTexGend (GLenum coord, GLenum pname, GLdouble param)
{
    GLenum p = (GLenum)param;

	GLV.glTexGend (coord, pname, param);
}

void GLAPIENTRY glTexGendv (GLenum coord, GLenum pname, const GLdouble *params)
{
	GLV.glTexGendv (coord, pname, params);
}

void GLAPIENTRY glTexGenf (GLenum coord, GLenum pname, GLfloat param)
{
    GLenum p = (GLenum)param;

	GLV.glTexGenf (coord, pname, param);
}

void GLAPIENTRY glTexGenfv (GLenum coord, GLenum pname, const GLfloat *params)
{
	GLV.glTexGenfv (coord, pname, params);
}

void GLAPIENTRY glTexGeni (GLenum coord, GLenum pname, GLint param)
{
    GLenum p = (GLenum)param;

	GLV.glTexGeni (coord, pname, param);
}

void GLAPIENTRY glTexGeniv (GLenum coord, GLenum pname, const GLint *params)
{
	GLV.glTexGeniv (coord, pname, params);
}

void GLAPIENTRY glTexImage1D (GLenum target, GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type, const GLvoid *pixels)
{
	GLV.glTexImage1D (target, level, internalformat, width, border, format, type, pixels);
}

void GLAPIENTRY glTexImage2D (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid *pixels)
{
	GLV.glTexImage2D (target, level, internalformat, width, height, border, format, type, pixels);
}

/*
void GLAPIENTRY glTexImage3D (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const GLvoid *pixels )
{
	GLV.glTexImage3D (target, level, internalformat, width, height, depth, border, format, type, pixels);
}*/


void GLAPIENTRY glTexParameterf (GLenum target, GLenum pname, GLfloat param)
{
    GLenum p = (GLenum)param;

	GLV.glTexParameterf (target, pname, param);
}

void GLAPIENTRY glTexParameterfv (GLenum target, GLenum pname, const GLfloat *params)
{
	GLV.glTexParameterfv (target, pname, params);
}

void GLAPIENTRY glTexParameteri (GLenum target, GLenum pname, GLint param)
{
    GLenum p = (GLenum)param;

	GLV.glTexParameteri (target, pname, param);
}

void GLAPIENTRY glTexParameteriv (GLenum target, GLenum pname, const GLint *params)
{
	GLV.glTexParameteriv (target, pname, params);
}

void GLAPIENTRY glTexSubImage1D (GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const GLvoid *pixels)
{
	GLV.glTexSubImage1D (target, level, xoffset, width, format, type, pixels);
}

void GLAPIENTRY glTexSubImage2D (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels)
{
	GLV.glTexSubImage2D (target, level, xoffset, yoffset, width, height, format, type, pixels);
}

void GLAPIENTRY glTranslated (GLdouble x, GLdouble y, GLdouble z)
{
   process_glTranslatef(x,y,z);

	GLV.glTranslated (x, y, z);
}

void GLAPIENTRY glTranslatef (GLfloat x, GLfloat y, GLfloat z)
{
   process_glTranslatef(x,y,z);
	GLV.glTranslatef (x, y, z);
}

void GLAPIENTRY glVertex2d (GLdouble x, GLdouble y)
{
	GLV.glVertex2d (x, y);
}

void GLAPIENTRY glVertex2dv (const GLdouble *v)
{
	GLV.glVertex2dv (v);
}

void GLAPIENTRY glVertex2f (GLfloat x, GLfloat y)
{
	GLV.glVertex2f (x, y);
}

void GLAPIENTRY glVertex2fv (const GLfloat *v)
{
	GLV.glVertex2fv (v);
}

void GLAPIENTRY glVertex2i (GLint x, GLint y)
{
	GLV.glVertex2i (x, y);
}

void GLAPIENTRY glVertex2iv (const GLint *v)
{
	GLV.glVertex2iv (v);
}

void GLAPIENTRY glVertex2s (GLshort x, GLshort y)
{
	GLV.glVertex2s (x, y);
}

void GLAPIENTRY glVertex2sv (const GLshort *v)
{
	GLV.glVertex2sv (v);
}

void GLAPIENTRY glVertex3d (GLdouble x, GLdouble y, GLdouble z)
{
	process_glVertex3(x,y,z);
	GLV.glVertex3d (x, y, z);
}

void GLAPIENTRY glVertex3dv (const GLdouble *v)
{
	process_glVertex3(v[0],v[1],v[2]);
	GLV.glVertex3dv (v);
}

void GLAPIENTRY glVertex3f (GLfloat x, GLfloat y, GLfloat z)
{
	process_glVertex3(x,y,z);
	GLV.glVertex3f (x, y, z);
}

void GLAPIENTRY glVertex3fv (const GLfloat *v)
{
	process_glVertex3(v[0],v[1],v[2]);
	GLV.glVertex3fv (v);
}

void GLAPIENTRY glVertex3i (GLint x, GLint y, GLint z)
{
	GLV.glVertex3i (x, y, z);
}

void GLAPIENTRY glVertex3iv (const GLint *v)
{
	GLV.glVertex3iv (v);
}

void GLAPIENTRY glVertex3s (GLshort x, GLshort y, GLshort z)
{
	GLV.glVertex3s (x, y, z);
}

void GLAPIENTRY glVertex3sv (const GLshort *v)
{
	GLV.glVertex3sv (v);
}

void GLAPIENTRY glVertex4d (GLdouble x, GLdouble y, GLdouble z, GLdouble w)
{
	GLV.glVertex4d (x, y, z, w);
}

void GLAPIENTRY glVertex4dv (const GLdouble *v)
{
	GLV.glVertex4dv (v);
}

void GLAPIENTRY glVertex4f (GLfloat x, GLfloat y, GLfloat z, GLfloat w)
{
	GLV.glVertex4f (x, y, z, w);
}

void GLAPIENTRY glVertex4fv (const GLfloat *v)
{
	GLV.glVertex4fv (v);
}

void GLAPIENTRY glVertex4i (GLint x, GLint y, GLint z, GLint w)
{
	GLV.glVertex4i (x, y, z, w);
}

void GLAPIENTRY glVertex4iv (const GLint *v)
{
	GLV.glVertex4iv (v);
}

void GLAPIENTRY glVertex4s (GLshort x, GLshort y, GLshort z, GLshort w)
{
	GLV.glVertex4s (x, y, z, w);
}

void GLAPIENTRY glVertex4sv (const GLshort *v)
{
	GLV.glVertex4sv (v);
}

void GLAPIENTRY glVertexPointer (GLint size, GLenum type, GLsizei stride, const GLvoid *pointer)
{
	GLV.glVertexPointer (size, type, stride, pointer);
}

void GLAPIENTRY glViewport (GLint x, GLint y, GLsizei width, GLsizei height)
{
	GLV.glViewport (x, y, width, height);
}

#if defined WIN32 || defined WIN64

/* WGL functions */
int GLAPIENTRY wglChoosePixelFormat (HDC a, CONST PIXELFORMATDESCRIPTOR *b)
{
   //fprintf(debug_log,"wglChoosePixelFormat: %X %X\n",a,b); fflush(debug_log);
	int result; 

	(void)gltraceInit();
	result = GLV.wglChoosePixelFormat(a,b);
    return result;
}

BOOL GLAPIENTRY wglCopyContext (HGLRC a, HGLRC b, UINT c)
{
	BOOL result;
   //fprintf(debug_log,"wglCopyContext: %X %X %X\n",a,b,c); fflush(debug_log);
	(void)gltraceInit();
	result = GLV.wglCopyContext(a,b,c);
	return result;
}

HGLRC GLAPIENTRY wglCreateContext (HDC a)
{
	HGLRC result;
   //fprintf(debug_log,"wglCreateContext: %X\n",a); fflush(debug_log);
	(void)gltraceInit();
	result = GLV.wglCreateContext(a);
    return result;
}

HGLRC GLAPIENTRY wglCreateLayerContext (HDC a, int b)
{
	HGLRC result;

	(void)gltraceInit();
	result = GLV.wglCreateLayerContext(a,b);
	return result;
}

BOOL GLAPIENTRY wglDeleteContext (HGLRC a)
{
	BOOL result;
   //fprintf(debug_log,"wglDeleteContext: %X\n",a); fflush(debug_log);
	(void)gltraceInit();
	result = GLV.wglDeleteContext(a);
	return result;
}

BOOL GLAPIENTRY wglDescribeLayerPlane (HDC a, int b, int c, UINT d, LPLAYERPLANEDESCRIPTOR e)
{
	BOOL result;

	(void)gltraceInit();
	result = GLV.wglDescribeLayerPlane(a,b,c,d,e);
	return result;
}


void dump_error()
{
   while(1)
   {
     GLenum errorCode=glGetError();
   
      if (errorCode!=GL_NO_ERROR)
      {
         fprintf(debug_log,"opengl error: %d %s\n",errorCode,gluErrorString(errorCode));
         fflush(debug_log);
      }
      else  
      {
         fprintf(debug_log,"no opengl error\n");
         fflush(debug_log);
         break;
      }  
   }
}



int GLAPIENTRY wglDescribePixelFormat (HDC a, int b, UINT c, LPPIXELFORMATDESCRIPTOR d)
{
   //fprintf(debug_log,"wglDescribePixelFormat: %X %X %X %X\n",a,b,c,d); fflush(debug_log);

	int result;
	
	(void)gltraceInit();
   //dump_error();
	result = GLV.wglDescribePixelFormat(a,b,c,d);
   //dump_error();
   //fprintf(debug_log,"  result of wglDescribePixelFormat (should be number of formats: %d\n",result);
   //fprintf(debug_log,"  wglDescribePixelFormat: %X %X %X %X\n",a,b,c,d); fflush(debug_log);
   
	return result;
}

HGLRC GLAPIENTRY wglGetCurrentContext (void)
{
	HGLRC result;
   //fprintf(debug_log,"wglGetCurrentContext"); fflush(debug_log);
	(void)gltraceInit();
	result = GLV.wglGetCurrentContext();
	return result;
}

HDC GLAPIENTRY wglGetCurrentDC (void)
{
	HDC result;
   //fprintf(debug_log,"wglGetCurrentDC"); fflush(debug_log);
	(void)gltraceInit();
	result = GLV.wglGetCurrentDC();
	return result;
}

PROC GLAPIENTRY wglGetDefaultProcAddress (LPCSTR a)
{
	PROC result;

	(void)gltraceInit();
	result = GLV.wglGetDefaultProcAddress(a);
	return result;
}

int GLAPIENTRY wglGetLayerPaletteEntries (HDC a, int b, int c, int d, COLORREF *e)
{
	int result;

	result = GLV.wglGetLayerPaletteEntries(a,b,c,d,e);
	return result;
}

int GLAPIENTRY wglGetPixelFormat (HDC a)
{
	int result;
   //fprintf(debug_log,"wglGetPixelFormat: %X\n",a); fflush(debug_log);

	(void)gltraceInit();
	result = GLV.wglGetPixelFormat(a);
	return result;
}

PROC  GLAPIENTRY wglGetProcAddress (LPCSTR a)
{
	int	index;
	PROC result;
	
	(void)gltraceInit();
    
   return (PROC)get_opengl_function(a); //MATLAB has new weird behavior that gets all functions via wglGetProcAddres   
    
    //if(bDisableExt == GL_TRUE)
    //{
        //return NULL;
   // }
	/*result = GLV.wglGetProcAddress(a);
	index = GetEXTProcIndex(a);
	if (index >= 0 && result != NULL)
	{
		PROC result2;
		
		((unsigned long *) (&GLV_EXT))[index] = (unsigned long)result;
		result2 = (PROC)(((unsigned long *) (&GLV_EXT_RETURN))[index]);
		if (result2 != NULL)
		{
			result = result2;
			//print("%s extension found\n", a);
		}
	}
    else
    {
        ;//print("%s extension NOT found\n", a);
    }
	return result;
	*/
}

BOOL  GLAPIENTRY wglMakeCurrent (HDC a, HGLRC b)
{
	BOOL result;
   //fprintf(debug_log,"wglMakeCurrent: %X %X\n",a,b); fflush(debug_log);
   
	(void)gltraceInit();
	result = GLV.wglMakeCurrent(a,b);
	return result;
}

BOOL  GLAPIENTRY wglRealizeLayerPalette (HDC a, int b, BOOL c)
{
	BOOL result;
   //fprintf(debug_log,"wglRealizeLayerPalette\n",a); fflush(debug_log);
	result = GLV.wglRealizeLayerPalette(a,b,c);
	return result;
}

int GLAPIENTRY wglSetLayerPaletteEntries (HDC a, int b, int c, int d, CONST COLORREF *e)
{
	int result;
   //fprintf(debug_log,"wglSetLayerPaletteEntries\n",a); fflush(debug_log);
	result = GLV.wglSetLayerPaletteEntries(a,b,c,d,e);
	return result;
}

BOOL  GLAPIENTRY wglSetPixelFormat (HDC a, int b, CONST PIXELFORMATDESCRIPTOR *c)
{
	BOOL result;
	//fprintf(debug_log,"wglSetPixelFormat: %X %X %X\n",a,b,c); fflush(debug_log);
	(void)gltraceInit();
	result = GLV.wglSetPixelFormat(a,b,c);
	return result;
}

BOOL  GLAPIENTRY wglShareLists (HGLRC a, HGLRC b)
{
	BOOL result;
   //fprintf(debug_log,"wglShareLists\n",a); fflush(debug_log);
	result = GLV.wglShareLists(a,b);
	return result;
}

BOOL  GLAPIENTRY wglSwapBuffers (HDC a)
{
	BOOL result;
   //fprintf(debug_log,"wglSwapBuffers\n",a); fflush(debug_log);	
    process_end_of_frame();
	result = GLV.wglSwapBuffers(a);

    
	return result;
}

BOOL  GLAPIENTRY wglSwapLayerBuffers (HDC a, UINT b)
{
	BOOL result;
   //fprintf(debug_log,"wglSwapLayerBuffers\n",a); fflush(debug_log);	
   	result = GLV.wglSwapLayerBuffers(a,b);

    
	return result;
}



BOOL GLAPIENTRY wglSwapMultipleBuffers(GLuint a, const void *b)
{
   //fprintf(debug_log,"wglSwapMultipleBuffers\n");
   BOOL result;
   result=GLV.wglSwapMultipleBuffers(a,b);
   return result;
   

} 

BOOL  GLAPIENTRY wglUseFontBitmapsA (HDC a, DWORD b, DWORD c, DWORD d)
{
	BOOL result;

	result = GLV.wglUseFontBitmapsA(a,b,c,d);
	return result;
}

BOOL  GLAPIENTRY wglUseFontBitmapsW (HDC a, DWORD b, DWORD c, DWORD d)
{
	BOOL result;

	result = GLV.wglUseFontBitmapsW(a,b,c,d);
	return result;
}

BOOL  GLAPIENTRY wglUseFontOutlinesA (HDC a, DWORD b, DWORD c, DWORD d, FLOAT e, FLOAT f, int g, LPGLYPHMETRICSFLOAT h)
{
	BOOL result;

	result = GLV.wglUseFontOutlinesA(a,b,c,d,e,f,g,h);
	return result;
}

BOOL  GLAPIENTRY wglUseFontOutlinesW (HDC a, DWORD b, DWORD c, DWORD d, FLOAT e, FLOAT f, int g, LPGLYPHMETRICSFLOAT h)
{
	BOOL result;

	result = GLV.wglUseFontOutlinesW(a,b,c,d,e,f,g,h);
	return result;
}

#else

/* GLX functions */

#include <X11/Xutil.h>

XVisualInfo* glXChooseVisual (Display *dpy, int screen, int *attribList)
{
	XVisualInfo* result;
	int *attrib;

	(void)gltraceInit();
    
	result = GLV.glXChooseVisual(dpy, screen, attribList);
    return result;
}

void glXCopyContext (Display *dpy, GLXContext src, GLXContext dst, unsigned long mask)
{
	(void)gltraceInit();
	GLV.glXCopyContext(dpy, src, dst, mask);
}

GLXContext glXCreateContext (Display *dpy, XVisualInfo *vis, GLXContext shareList, Bool direct)
{
	GLXContext result;

	(void)gltraceInit();
    
	result = GLV.glXCreateContext(dpy, vis, shareList, direct);
    return result;
}

GLXPixmap glXCreateGLXPixmap (Display *dpy, XVisualInfo *vis, Pixmap pixmap)
{
	GLXPixmap result;

	(void)gltraceInit();

	result = GLV.glXCreateGLXPixmap(dpy, vis, pixmap);
    return result;
}

void glXDestroyContext (Display *dpy, GLXContext ctx)
{
	(void)gltraceInit();

	GLV.glXDestroyContext(dpy, ctx);
}

void glXDestroyGLXPixmap (Display *dpy, GLXPixmap pix)
{
	(void)gltraceInit();

	GLV.glXDestroyGLXPixmap(dpy, pix);
}

int glXGetConfig (Display *dpy, XVisualInfo *vis, int attrib, int *value)
{
	int result;

	(void)gltraceInit();
	result = GLV.glXGetConfig(dpy, vis, attrib, value);
    return result;
}

GLXContext glXGetCurrentContext (void)
{
	GLXContext result;

	(void)gltraceInit();
	result = GLV.glXGetCurrentContext();
    return result;
}

GLXDrawable glXGetCurrentDrawable (void)
{
	GLXDrawable result;

	(void)gltraceInit();
	result = GLV.glXGetCurrentDrawable();
    return result;
}

Bool glXIsDirect (Display *dpy, GLXContext ctx)
{
	Bool result;

	(void)gltraceInit();
	result = GLV.glXIsDirect(dpy, ctx);
    return result;
}

Bool glXMakeCurrent (Display *dpy, GLXDrawable drawable, GLXContext ctx)
{
	Bool result;

	(void)gltraceInit();
	result = GLV.glXMakeCurrent(dpy, drawable, ctx);
    return result;
}


Bool glXQueryExtension (Display *dpy, int *errorBase, int *eventBase)
{
	Bool result;

	(void)gltraceInit();
	result = GLV.glXQueryExtension(dpy, errorBase, eventBase);
    return result;
}


Bool glXQueryVersion (Display *dpy, int *major, int *minor)
{
	Bool result;

	(void)gltraceInit();
	result = GLV.glXQueryVersion(dpy, major, minor);
    return result;
}


void glXSwapBuffers (Display *dpy, GLXDrawable drawable)
{
	(void)gltraceInit();
    process_end_of_frame();
	GLV.glXSwapBuffers(dpy, drawable);
}

void glXUseXFont (Font font, int first, int count, int listBase)
{
	(void)gltraceInit();
	GLV.glXUseXFont(font, first, count, listBase);
}

void glXWaitGL (void)
{
	(void)gltraceInit();
	GLV.glXWaitGL();
}

void glXWaitX (void)
{
	(void)gltraceInit();
	GLV.glXWaitX();
}

const char * glXGetClientString (Display *dpy, int name )
{
	const char *result;

	(void)gltraceInit();
	result = GLV.glXGetClientString(dpy, name);

    return result;
}

const char * glXQueryServerString (Display *dpy, int screen, int name )
{
	const char *result;

	(void)gltraceInit();
	result = GLV.glXQueryServerString(dpy, screen, name);
	
    return result;
}

const char * glXQueryExtensionsString (Display *dpy, int screen)
{
	const char *result;

	(void)gltraceInit();
	result = GLV.glXQueryExtensionsString(dpy, screen);
    return result;
}

GLXFBConfig * glXGetFBConfigs (Display *dpy, int screen, int *nelements)
{
	GLXFBConfig *result;

	(void)gltraceInit();
	result = GLV.glXGetFBConfigs(dpy, screen, nelements);
    return result;
}

GLXFBConfig * glXChooseFBConfig (Display *dpy, int screen, const int *attrib_list, int *nelements)
{
	GLXFBConfig *result;

	(void)gltraceInit();
	result = GLV.glXChooseFBConfig(dpy, screen, attrib_list, nelements);
    return result;
}

int glXGetFBConfigAttrib (Display *dpy, GLXFBConfig config, int attribute, int *value)
{
	int result;

	(void)gltraceInit();
	result = GLV.glXGetFBConfigAttrib(dpy, config, attribute, value);
    return result;
}

XVisualInfo * glXGetVisualFromFBConfig (Display *dpy, GLXFBConfig config)
{
	XVisualInfo *result;

	(void)gltraceInit();
	result = GLV.glXGetVisualFromFBConfig(dpy, config);
    return result;
}

GLXWindow glXCreateWindow (Display *dpy, GLXFBConfig config, Window win, const int *attrib_list)
{
	GLXWindow result;

	(void)gltraceInit();

	result = GLV.glXCreateWindow(dpy, config, win, attrib_list);
    return result;
}

void glXDestroyWindow (Display *dpy, GLXWindow win)
{
	(void)gltraceInit();
	GLV.glXDestroyWindow(dpy, win);
}

GLXPixmap glXCreatePixmap (Display *dpy, GLXFBConfig config, Pixmap pixmap, const int *attrib_list)
{
	GLXPixmap result;

	(void)gltraceInit();

	result = GLV.glXCreatePixmap(dpy, config, pixmap, attrib_list);
    return result;
}

void glXDestroyPixmap (Display *dpy, GLXPixmap pixmap)
{
	(void)gltraceInit();
	GLV.glXDestroyPixmap(dpy, pixmap);
}

GLXPbuffer glXCreatePbuffer (Display *dpy, GLXFBConfig config, const int *attrib_list)
{
	GLXPbuffer result;

	(void)gltraceInit();

	result = GLV.glXCreatePbuffer(dpy, config, attrib_list);
    return result;
}

void glXDestroyPbuffer (Display *dpy, GLXPbuffer pbuf)
{
	(void)gltraceInit();
	GLV.glXDestroyPbuffer(dpy, pbuf);
}


void glXQueryDrawable (Display *dpy, GLXDrawable draw, int attribute, unsigned int *value)
{
	(void)gltraceInit();
	GLV.glXQueryDrawable(dpy, draw, attribute, value);
}

GLXContext glXCreateNewContext (Display *dpy, GLXFBConfig config, int render_type, GLXContext share_list, Bool direct)
{
	GLXContext result;

	(void)gltraceInit();
	result = GLV.glXCreateNewContext(dpy, config, render_type, share_list, direct);
    return result;
}

Bool glXMakeContextCurrent (Display *dpy, GLXDrawable draw, GLXDrawable read, GLXContext ctx)
{
	Bool result;

	(void)gltraceInit();
	result = GLV.glXMakeContextCurrent(dpy,  draw, read, ctx);
    return result;
}


GLXDrawable glXGetCurrentReadDrawable (void)
{
	GLXDrawable result;

	(void)gltraceInit();
	result = GLV.glXGetCurrentReadDrawable();
    return result;
}

Display * glXGetCurrentDisplay (void)
{
	Display * result;

	(void)gltraceInit();
	result = GLV.glXGetCurrentDisplay();
    return result;
}

int glXQueryContext (Display *dpy, GLXContext ctx, int attribute, int *value)
{
	int result;

	(void)gltraceInit();
	result = GLV.glXQueryContext(dpy, ctx, attribute, value);
    return result;
}

void glXSelectEvent (Display *dpy, GLXDrawable draw, unsigned long event_mask)
{
	(void)gltraceInit();
	GLV.glXSelectEvent(dpy, draw, event_mask);
}

void glXGetSelectedEvent (Display *dpy, GLXDrawable draw, unsigned long *event_mask)
{
	(void)gltraceInit();
	GLV.glXGetSelectedEvent(dpy, draw, event_mask);
}

GLXContextID glXGetContextIDEXT (const GLXContext ctx)
{
	GLXContextID result;

	(void)gltraceInit();
	result = GLV.glXGetContextIDEXT(ctx);
    return result;
}

GLXDrawable glXGetCurrentDrawableEXT (void)
{
	GLXDrawable result;

	(void)gltraceInit();
	result = GLV.glXGetCurrentDrawableEXT();
    return result;
}

GLXContext glXImportContextEXT (Display *dpy, GLXContextID contextID)
{
	GLXContext result;

	(void)gltraceInit();
	result = GLV.glXImportContextEXT(dpy, contextID);
    return result;
}

void glXFreeContextEXT (Display *dpy, GLXContext ctx)
{
	(void)gltraceInit();
	GLV.glXFreeContextEXT(dpy, ctx);
}

int glXQueryContextInfoEXT (Display *dpy, GLXContext ctx, int attribute, int *value)
{
	int result;

	(void)gltraceInit();
	result = GLV.glXQueryContextInfoEXT(dpy, ctx, attribute, value);
    return result;
}

void (*glXGetProcAddressARB(const GLubyte *procName))(void)
{
	void (*result)(void);

	(void)gltraceInit();

    //if(bDisableExt == GL_TRUE)
    //{
    //    return NULL;
    //}

	result = GLV.glXGetProcAddressARB(procName);
    return result;
}

#endif

#include "gl_funcs.h"
