# ML2VR

ML2VR allows MATLAB scripts to run on immersive Virtual Reality systems (especially clustered CAVE-type systems).

./intercept_dll 
Contains the code for the opengl intercept. This obtains the graphical output from from MATLAB and sends it over to our syzygy app. 

./vr_application. 
This app receives the opengl content and properly displays it. It also handles sending event/device data out to the user's MATLAB scripts

./matlab_toolbox. 
Here is the vr_interface, and also many examples of what you can do with the system. 

Reminder: there are 3 configuration files that need to be configured 
and located in the proper locations.
(1) intercept.cfg - for intercept dll
(2) ml2vr.cfg - for szg app
(3) vr_config.m - for MATLAB code

Notice: the most recent version of MATLAB that this will work with is MATLAB 2014A 32-bit, as the rendering engine switched after that. 

TODO:
* rewrite intercept to handle new render engine of 2014B and above
* oculus support (modify glut example)
* support 64-bit versions of MATLAB
