//********************************************************
// Syzygy is licensed under the BSD license v2
// see the file SZG_CREDITS for details
//********************************************************
// http://syzygy.isl.uiuc.edu/szg/index.html

#ifndef ARLOCK_H_SZG
#define ARLOCK_H_SZG

#ifdef WIN32
   #include <windows.h>
#else
   #include <pthread.h>
   #include <time.h>
#endif

class arLock {
  public:
    arLock();
    virtual ~arLock();
    virtual void lock();
    virtual bool tryLock();
    virtual void unlock();
  protected:
#ifdef WIN32
    HANDLE _mutex;
#else
    pthread_mutex_t _mutex;
#endif
};

#endif
