#ifndef DJZ_OPENGL_RECEIVER
#define DJZ_OPENGL_RECEIVER

#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio/read.hpp>
using boost::asio::ip::tcp;

#ifndef FREEVR
  #include "arThread.h"
  #include "arMasterSlaveFramework.h"
#else
  #include "szgLock.h"
#endif

#include <vector>

#include "buffer_queue.h"

class opengl_receiver
{
public:
   opengl_receiver(int port, string swap_server_ip, string swap_server_port);

   buffer_queue *_bq;

private:  

   arLock *data_lock;

   void session(tcp::socket* sock);
   void server(short port);
   bool send_buffer(tcp::socket* sock, int buffer_length, unsigned char *buffer);

   boost::asio::io_service io_service;

   FILE *_log;

   unsigned char data[10000000];

   tcp::socket* _result_socket;
   bool _connected_to_result_server;

   void connect_to_result_server(string ip, string port);

};

#endif
