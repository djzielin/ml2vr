#ifndef DJZ_JOYSTICKS_H
#define DJZ_JOYSTICKS_H

class joysticks
{
public:
   void set_joystick(int which, float value)
   {
      int vec_size=_joystick_vec.size();
      while(vec_size<=which)
      {
         _joystick_vec.push_back(0.0);
         vec_size++;   
         cout << "joysticks - expanding joystick vec. size now: " << vec_size << endl;  
      }
      _joystick_vec[which]=value;
   }

   float get_joystick(int which) 
   {
      if(which>=_joystick_vec.size()) 
         return 0.0;

      return _joystick_vec[which];
   }

private:
   vector <float> _joystick_vec;
};

#endif
