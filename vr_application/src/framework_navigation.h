#ifndef JOYSTICK_NAVIGATION_DJZ_H
#define JOYSTICK_NAVIGATION_DJZ_H

#ifdef USE_SZG
  #include "arPrecompiled.h"
  #include "arMasterSlaveFramework.h"
  #include "arMath.h"
#else
  #include "szgMath.h"
#endif

#include "joysticks.h"

class app_parameters;

class single_axis_navigation_parameters
{
public:
   enum nav_func_type
   {
      NAV_NONE,
      TRANS_RIGHT_LEFT,
      TRANS_FORWARD_BACK,
      TRANS_UP_DOWN,
      YAW,
      PITCH,
      ROLL,
   };

   nav_func_type navigation_func;
   float navigation_mult;
   int axis;
   bool navigation_constrain[3];

   single_axis_navigation_parameters(int a)
   {
      axis=a;
      navigation_func=NAV_NONE;
      navigation_mult=1.0;
   
      for(int i=0;i<3;i++)
         navigation_constrain[i]=false;
   }

   void set_func(unsigned char v)
   {
      switch(v)
      {
         case 0:
            navigation_func=NAV_NONE;
            break;
         case 1:
            navigation_func=TRANS_RIGHT_LEFT;
            break;
         case 2:
            navigation_func=TRANS_FORWARD_BACK;
            break;
         case 3:
            navigation_func=TRANS_UP_DOWN;
            break;
         case 4:
            navigation_func=YAW;
            break;
         case 5:
            navigation_func=PITCH;
            break;
         case 6:
            navigation_func=ROLL;
            break;
      }
   }
 
};

arVector3 get_direction_vector(arMatrix4 sensor, arVector3 direction);

void framework_navigation(double delta_time, arMatrix4 wand_sensor, joysticks *jsticks, app_parameters *ap);

#endif
