#ifndef WAND_DJZ_H
#define WAND_DJZ_H

// precompiled header include MUST appear as the first non-comment line

#ifndef FREEVR
  #include "arPrecompiled.h"
  #include "arMasterSlaveFramework.h"
#endif

#include "app_parameters.h"
#include <GL/gl.h>
#include "opengl_shapes.h"

class wand
{
public:   
   void draw(app_parameters *ap)
   {
      if(ap->wand_visable==false)
         return;

      glColor3f(ap->wand_color[0], ap->wand_color[1], ap->wand_color[2]);

      glPushMatrix();
         glMultMatrixf(_raw.v);
         glTranslatef(0.0f, 0.0f, ap->wand_length*-0.5f-ap->wand_push_forward);
         glScalef(ap->wand_width*0.5,  ap->wand_width*0.5, ap->wand_length*0.5); 
         if(ap->wand_model==0) draw_display_list_cube();
         if(ap->wand_model==1) draw_display_list_pyramid();
      glPopMatrix();
   }

   void update(app_parameters *ap, arMatrix4 m4)
   {   
      _raw=m4;
      _pos_full=_raw; 

      if(ap->wand_position_at_tip==true)
         _pos_full=_pos_full*ar_translationMatrix(0.0f,0.0f,-ap->wand_length-ap->wand_push_forward);

      #ifndef FREEVR
        _pos_full=ar_matrixToNavCoords(_pos_full); //get wands position in world coordinates
      #else
        //TODO freevr travel support here!
      #endif

    _pos=ar_extractTranslation(_pos_full);
   }

   arVector3 get_position() { return _pos; }
   arMatrix4 get_pos_full() { return _pos_full; }

private:
   arVector3 _pos;
   arMatrix4 _pos_full;
   arMatrix4 _raw;
};

typedef vector<wand *> WandVec;

#endif





