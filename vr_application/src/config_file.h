int swap_port=9001;
int interface_port=9999;
int opengl_port=9000;
string swap_ip="127.0.0.1";
int swap_clients=6;

// http://www.cplusplus.com/faq/sequences/strings/trim/
std::string& trim_right_inplace(
  std::string&       s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return s.erase( s.find_last_not_of( delimiters ) + 1 );
}

std::string& trim_left_inplace(
  std::string&       s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return s.erase( 0, s.find_first_not_of( delimiters ) );
}

std::string& trim(
  std::string&       s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return trim_left_inplace( trim_right_inplace( s, delimiters ), delimiters );
}

string itoa(int i)
{
    std::string s;
    std::stringstream out;
    out << i;
    s = out.str();
    return s;
}

void read_config(string file_name)
{
   printf("read_config - attempting to open config file: %s\n",file_name.c_str());

   FILE *f=fopen(file_name.c_str(),"r");

   if(f==NULL)
   {
      printf("read_config - unable to open configuration file: %s\n",file_name.c_str());
      exit(1);
   }

   while (feof(f)==0 ) //TODO - use actual configuration file reading library
   {
      char line[100];
      char *result=fgets(line,100,f);
      if(result==NULL) continue;

      if(strlen(line)==0) continue;
      if(line[0]=='#') continue;

      char * tok;
      tok = strtok (line," ");
 
      if(strlen(tok)==0) continue;

      if(strcmp(tok,"swap_port")==0)
      {
         tok = strtok (NULL, " ");
         swap_port=atoi(tok);
         printf("read_config - setting swap_port: %d\n",swap_port);
      }
      if(strcmp(tok,"opengl_port")==0)
      {
         tok = strtok (NULL, " ");
         opengl_port=atoi(tok);
         printf("read_config - setting opengl_port: %d\n",opengl_port);
      }
      if(strcmp(tok,"interface_port")==0)
      {
         tok = strtok (NULL, " ");
         interface_port=atoi(tok);
         printf("read_config - setting interface_port: %d\n",interface_port);
      }
      if(strcmp(tok,"swap_clients")==0)
      {
         tok = strtok (NULL, " ");
         swap_clients=atoi(tok);
         printf("read_config - setting swap_clients: %d\n",swap_clients);
      }
      if(strcmp(tok,"swap_ip")==0)
      {
         tok = strtok (NULL, " ");
         swap_ip=tok;
         trim(swap_ip);

         printf("read_config - setting swap_ip: %s\n",swap_ip.c_str());
      }
   }

   fclose(f);
}
