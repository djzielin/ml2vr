//********************************************************
// Syzygy is licensed under the BSD license v2
// see the file SZG_CREDITS for details
//********************************************************
// http://syzygy.isl.uiuc.edu/szg/index.html

#ifndef DJZ_TIME
#define DJZ_TIME

void ar_usleep(int microseconds);

class ar_timeval {
 public:
  int sec;
  int usec;
  ar_timeval() : sec(0), usec(0) {}
  ar_timeval(int s, int u) : sec(s), usec(u) {}
};

ar_timeval ar_time();

double ar_difftime(const ar_timeval& a, const ar_timeval& b);

#endif
