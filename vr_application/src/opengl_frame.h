#ifndef DJZ_OPENGL_FRAME
#define DJZ_OPENGL_FRAME

#ifndef FREEVR
   #include "arThread.h" //for arLock
   #include "arMath.h"
#else
   #include "szgLock.h"
   #include "szgMath.h"
#endif


#include "wand.h"
#include "app_parameters.h"
#include "buttons.h"

#ifdef DJZ_SLICE_PLANE
  #include "slice_plane.h"
#endif

class opengl_frame
{
public:
   opengl_frame();
   void render(app_parameters *ap);
   void process_wand(wand *w, buttons *btns, app_parameters *ap, double delta_seconds);
   arMatrix4 get_matrix4_in_ref() { return _m4_in_ref; } 
   void swap_new_frame(unsigned char *b, unsigned int length);
   void reset();

private:  
	void _rend();
   void _rend_with_fancy_drag(app_parameters *ap);
   void _rend_with_local_slice(app_parameters *ap);

   void dump_packet(unsigned char *data, unsigned int size);
   arLock *data_lock;
   bool in_model_mode;

   bool new_vec_created;
    
   arMatrix4 m4; //TODO: should have underscore

   bool _is_dragging;
   bool _show_bounding;
   arMatrix4 _transform;
   arVector3 _original_position;
   arVector3 _current_position;
   float _bounding_scale;

   GLfloat _post_transform_matrix[16];

   arMatrix4 _final_transform;
   arMatrix4 _stored_transform;
   arMatrix4 _drag_offset;
   arMatrix4 _m4_in_ref;
   
   bool _is_clip_dragging;
   int _current_clip;
   arVector3 _clip_diff;
   arVector3 _clip_start;

   unsigned char *data;  //TODO: change to underscore ie, _
   unsigned char *_data1;
   unsigned char *_data2;

   unsigned int data_size;
   unsigned int _data_index;
   unsigned int _frame;
   unsigned int _content_frame;
   unsigned int _old_content_frame;

   arMatrix4 _fancy_drag_offset;
   arMatrix4 _m4_when_triggered;
   
   bool _started_fancy_drag;
   bool _fancy_drag_button_history[2];
   bool _fancy_drag_release;
   bool _fancy_drag_trigger;
   arMatrix4 _fancy_recent;

   unsigned int display_list_map[1000]; //TODO: make more elegant, less hardcoded. 



   FILE *_log;
   FILE *_log2;
};

#endif



  
