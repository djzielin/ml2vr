/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "swap_controller.h"
#include <iostream> 

swap_controller::swap_controller(short port, int number_clients)
{
   data_lock=new arLock();
   _all_connected=false;
   num_frames_ready_for_swap=0;
   _num_clients=number_clients;

   boost::thread t( boost::bind( &swap_controller::result_server, this, port));
   boost::thread t2( boost::bind( &swap_controller::listen_thread, this));
}

extern unsigned int post_frame; //over in main_szg.cpp

void swap_controller::listen_thread()
{
   while(_all_connected==false)
      ar_usleep(10000);

   printf("swap_controller - started listen_thread\n");

   while(_all_connected)
   {
      //printf("swap_controller - waiting for signal to come back from all clients\n");

      for(int i=0;i<all_clients.size();i++)
      {
         unsigned char d1;
      
         boost::system::error_code error;

         //printf("swap_controller - waiting for message from node: %d\n",i);

         unsigned int length = boost::asio::read(*all_clients[i], boost::asio::buffer(&d1,1), boost::asio::transfer_all(), error);
         if (error == boost::asio::error::eof || length==0 || error)
         {
            printf("connection closed by client\n");
            _all_connected=false;
            break; // Connection closed cleanly by peer.
         }
         //printf("swap_controller - got message from node: %d\n",i);
      }

  
      data_lock->lock();
        num_frames_ready_for_swap++;
      data_lock->unlock();
      printf("swap_controller - frames ready for swap has been incremented. now: %d at post_frame: %d\n",num_frames_ready_for_swap,post_frame);
   }
}

unsigned int swap_controller::get_num_frames_ready_for_swap()
{
   data_lock->lock();
   unsigned int val=num_frames_ready_for_swap;
   data_lock->unlock();

   return val;
}

void swap_controller::reduce_num_frames_ready_for_swap(int how_many)
{
   data_lock->lock();
   num_frames_ready_for_swap-=how_many;
   data_lock->unlock();
}

void swap_controller::result_server(short port)
{
   try
   {
      tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
      for (int i=0;i<_num_clients;i++) 
      {
         socket_ptr sock(new tcp::socket(io_service));
         a.accept(*sock);
         sock->set_option(tcp::no_delay(true));
         all_clients.push_back(sock);

         printf("swap_controller - total clients for swap_controler now: %d\n",all_clients.size());
     }
   }
   catch (std::exception& e)
   {
      std::cerr << "Exception: " << e.what() << "\n";
   }
   printf("swap_controller - all clients connected to swap_controller\n");
   _all_connected=true;
}

