/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// precompiled header include MUST appear as the first non-comment line
#include "arPrecompiled.h"
#include "arMasterSlaveFramework.h"

#include <iostream>
#include <fstream>
#include <string>
#include <arThread.h>

#include "opengl_shapes.h"
#include "headlamp.h"
#include "wand.h"
#include "opengl_receiver.h"
#include "opengl_frame.h"
#include "interface_server.h"
#include "app_parameters.h"
#include "swap_controller.h"
#include "config_file.h"
#include "buttons.h"
#include "joysticks.h"
#include "framework_navigation.h"

#include <GL/glut.h>

using namespace std;

interface_server *interface_s;
opengl_receiver  *opengl_r;
opengl_frame     *opengl_fr;
app_parameters   *ap;
swap_controller  *swap_c;
WandVec          wand_vec;

GLfloat  position_0[4] = { 0.0, -1.0, 0.0, 1.0 }, // 0 = directional, 1 = location
         ambient_0[4] =  { 0.4, 0.4, 0.4, 1.0 },
         diffuse_0[4] =  { 1.0, 1.0, 1.0, 1.0 },
         specular_0[4] = { 0.0, 0.0, 0.0, 0.0 };

//FILE *fps_output; //logging
buttons *btns;
joysticks *jsticks;

void reset_navigation()
{
   ar_setNavMatrix(ar_identityMatrix());
   opengl_fr->reset();
}

bool start(arMasterSlaveFramework &fw, arSZGClient &cli)
{
   printf("start - setting up opengl_receiver, swap_controller, interface_server\n");

   read_config("ml2vr.cfg"); //TODO, there should really be one config file for all 3
 
   ap=new app_parameters();
   btns=new buttons();
   jsticks=new joysticks();

   if(fw.getMaster())
	  swap_c=new swap_controller(swap_port,swap_clients);
   
   opengl_r=new opengl_receiver(opengl_port,swap_ip,itoa(swap_port)); //every client has a receiver
  
   if(fw.getMaster())
   {      
      interface_s=new interface_server(interface_port); 
	  
      //fps_output=fopen("C:/tmp/fps_output.txt","w");
      //if(fps_output==NULL)
      //{
      //   printf("unable to open fps output!\n");
      //   exit(1);
      //}
   }
   ar_usleep(100000);

   return true;
}

void init(arMasterSlaveFramework &fw,  arGUIWindowInfo* )
{
   glLightfv(GL_LIGHT0, GL_POSITION, position_0);
   glLightfv(GL_LIGHT0, GL_AMBIENT,  ambient_0);  
   glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuse_0);  
   glLightfv(GL_LIGHT0, GL_SPECULAR, specular_0); 
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glShadeModel(GL_SMOOTH);
   glColorMaterial(GL_FRONT, GL_DIFFUSE);
   glEnable(GL_COLOR_MATERIAL);
   glEnable(GL_LINE_SMOOTH);
   glEnable(GL_POINT_SMOOTH);
   glEnable(GL_NORMALIZE);
   
   //glLightModeli(GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE);
   //glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,GL_TRUE);

   opengl_fr=new opengl_frame();

   fw.addInternalTransferField("opengl_swap", AR_CHAR, 1);

   fw.addInternalTransferField("app_parameter", AR_CHAR, 1);
   if(fw.getMaster())
      fw.setInternalTransferFieldSize("app_parameter", AR_CHAR, 0);

   init_display_list_cube();
   init_display_list_sphere(1);
   init_display_list_pyramid();
}

////////////////////////////////////////////////////////////////////////////////
// main draw-render loop.
////////////////////////////////////////////////////////////////////////////////

void display(arMasterSlaveFramework& fw)
{
   //printf("main display loop\n");
   
   //////////// CLEAR BUFFERS /////////////////////////
   glClearColor(ap->background_color[0],ap->background_color[1],ap->background_color[2],1); //world background color
   glClearDepth(1.0);
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

   glEnable(GL_LIGHTING);

   glColor3f(0,1,0);
   if(wand_vec.size()>0)
      wand_vec[0]->draw(ap);
  
   fw.loadNavMatrix(); //allows us to "drive" around our world
     
					 
   opengl_fr->render(ap);
   
}

int mat_frame=0;
unsigned int post_frame=0;
unsigned int pre_frame=0;

void preExchange(arMasterSlaveFramework& fw) //happens on master
{
//cout << "Head is at: " << ar_extractTranslationMatrix(fw.getMatrix(0)) << endl;

   ar_timeval a=ar_time();

   double delta_milliseconds=fw.getLastFrameTime();
   double delta_seconds=delta_milliseconds/(double)1e3;

   for(int i=0;i<2;i++)
      jsticks->set_joystick(i,fw.getAxis(i));
   
   framework_navigation(delta_seconds,fw.getMatrix(ap->wand_sensor_id),jsticks,ap); 

   bool is_nav_waiting=true;

   while(is_nav_waiting) //consume all waiting nav events
   {
      navigation_event *ne;
      is_nav_waiting=interface_s->get_waiting_navigation_event(ne);
   
      if(is_nav_waiting)
      {
         if(ne->type==0)
            ar_navTranslate(ne->v1);   
         if(ne->type==1)
            ar_navRotate(ne->v1,ne->a);

         delete ne;
      }
   }

   //printf("in pre exchange, getting the transfer field\n"); fflush(stdout);
   int ssize;
   unsigned char* swap=(unsigned char *)fw.getTransferField("opengl_swap",AR_CHAR,ssize );
   //printf("transfer field has size: %d\n",ssize);

   //if(mat_frame<1)
     swap[0]=swap_c->get_num_frames_ready_for_swap();
   //else 
   //  swap[0]=0;

   //printf("swap frames ready: %d\n",swap[0]); fflush(stdout);

   if(swap[0]>0)
   {
	   printf("preExchange - number of swap frames on the transfer field: %d at frame: %d\n",swap[0],pre_frame);
   }

   single_app_param *sd;
   bool result=interface_s->get_waiting_app_parameter(sd);
   if(result)
   {
      //printf("we got a parameter!\n");
      int ssize=sd->size;
      //printf("we have a single waiting app parameter of size: %d\n",ssize);
   
       fw.setInternalTransferFieldSize("app_parameter",AR_CHAR,ssize);
       int ssize2;
       unsigned char *buf=(unsigned char*)fw.getTransferField("app_parameter",AR_CHAR,ssize2);
       if(ssize2!=ssize)
       {
          printf("error happened in memeory alloc for app_parameter transfer field!\n");
          exit(1);
       }
       memcpy(buf,sd->data,ssize);
       delete sd;
   }

   //printf("all done\n");
   ar_timeval b=ar_time();
   //cout << "pre time: " << ar_difftime(b,a)/1e6 << endl;
   pre_frame++;
}


int szg_frame=0;

bool done_with_first=false;
ar_timeval old_time;



void postExchange(arMasterSlaveFramework& fw)
{
   ar_timeval a=ar_time();

   //printf("---------------frame----------------------\n");
 
   double delta_milliseconds=fw.getLastFrameTime();
   double delta_seconds=delta_milliseconds/(double)1e3;

   //TODO: do we need to handle wands dissapearing?  

   int num_wands=fw.getNumberMatrices()-1;
   int wand_vec_size=wand_vec.size();
   while(wand_vec_size<num_wands )
   {
      wand *w=new wand();
      wand_vec.push_back(w);
      wand_vec_size++;
      printf("postExchange - expanding wand_vec. size now: %d\n",wand_vec_size);
   }
 
   //printf("updating wands...\n"); fflush(stdout);
   for(int i=0;i<wand_vec.size();i++)
      wand_vec[i]->update(ap,fw.getMatrix(i+1));
 
   //printf("updating buttons\n"); fflush(stdout);
   for(int i=0;i<6;i++) //TODO: why does fw.getNumberButtons() return 0?
      btns->set_button(i,fw.getButton(i));
 
   if(wand_vec.size()>0)
   {
      //printf("calling opengl_frame->process_wand\n"); fflush(stdout);
      opengl_fr->process_wand(wand_vec[0],btns,ap,delta_seconds); //TODO: which wand shouldn't be hardcoded. 
   }

   ar_timeval b=ar_time();

   //printf("getting the transfer field\n");
   int ssize; 
   unsigned char* swap=(unsigned char *)fw.getTransferField("opengl_swap",AR_CHAR,ssize );

   if(swap[0]>0) //are all clients ready to swap in the new glvec?
   {
     //int actual_swap=1; //force us to process every frame so we don't miss any display list compiles, ect..
	  int actual_swap=swap[0]; //allow us to skip frames for greater performance
     
     printf("postExchange - we have determined that %d frames need to be swapped at frame: %d\n",actual_swap,post_frame);
	  unsigned int bsize;
	  unsigned char *d=opengl_r->_bq->pop_buffers(actual_swap,bsize);

	  opengl_fr->swap_new_frame(d,bsize);   

     if(fw.getMaster())
     {
       swap_c->reduce_num_frames_ready_for_swap(actual_swap);
       mat_frame++;
     }
   }

   unsigned char* buf=(unsigned char *)fw.getTransferField("app_parameter",AR_CHAR,ssize );
   
   if(ssize>0) //do we have any waiting app_parameters that need to be processed?
   {
	   printf("PostExchange - we received an app_parameter update of size: %d\n",ssize);
      ap->apply_single_parameter(buf);

      if(fw.getMaster())
	   {
		  fw.setInternalTransferFieldSize("app_parameter",AR_CHAR,0); 
	  }
   }

   ar_timeval c=ar_time();

   if(fw.getMaster() && wand_vec.size()>0) //let the event server know our current status (wand pos, buttons, axis)
   {
      if(fw.getNumberMatrices()>0)
      {
         arMatrix4 head_fixed=ar_matrixToNavCoords(fw.getMatrix(0));
         interface_s->update_wand_matrix4(0,head_fixed);
      }
      if(fw.getNumberMatrices()>1)
      {
         arMatrix4 wp=wand_vec[0]->get_pos_full();
         interface_s->update_wand_matrix4(1,wp);  
      }
      for(int i=0;i<6;i++) //fw.getNumberButtons()
         interface_s->update_button(i,fw.getButton(i));

      for(int i=0;i<2;i++) //fw.getNumberAxes()
         interface_s->update_joystick(i,fw.getAxis(i));
   }   

   ar_timeval d=ar_time();

   post_frame++;

  /* if(fw.getMaster()) //logging
   {
     szg_frame++;
     //printf("frame: %d\n",szg_frame);

	 if(szg_frame==60)
	 {
		ar_timeval current_time=ar_time();
      //printf("dumping log\n");

	    if(done_with_first)
		 {
		   float elapsed=ar_difftime(current_time,old_time)/1e6;
           fprintf(fps_output,"%f %d %d %f %f\n",elapsed,szg_frame,mat_frame,(float)szg_frame/elapsed,(float)mat_frame/elapsed);
		   fflush(fps_output);
		 }

        szg_frame=0;
        mat_frame=0;
		  done_with_first=true;
        old_time=current_time;
	}
  }*/
}

int main(int argc, char** argv)
{      
   printf("starting up the Syzygy ML2VR Viewer!!!\n");

   arMasterSlaveFramework framework;
 
   framework.setStartCallback(start);
   framework.setWindowStartGLCallback(init);
   framework.setPreExchangeCallback(preExchange);
   framework.setPostExchangeCallback(postExchange);
   framework.setDrawCallback(display);

   framework.setClipPlanes(0.1, 2000);
   framework.setUnitConversion(1.0);
   framework.usePredeterminedHarmony(); //don't start running draw/pre/post until all nodes connected
    
   if (!framework.init(argc, argv))    return 1;
   if (!framework.start())             return 1;

   return 0; // Never get here.
}





