/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// precompiled header include MUST appear as the first non-comment line

#include <iostream>
#include <fstream>
#include <string>

#include "opengl_shapes.h"

#ifdef DJZ_JOYSTICK_NAGIVATION
   #include "joystick_navigation.h"
#endif


#include "wand.h"
#include "opengl_receiver.h"
#include "opengl_frame.h"
#include "interface_server.h"
#include "app_parameters.h"
#include "swap_controller.h"
#include "config_file.h"
#include "buttons.h"

#include <GL/glut.h>

using namespace std;

#include "freevr.h"	/* needed for FreeVR functions & values */
#define	WAND_SENSOR	1	/* TODO: this should really be based on the concept of props */


interface_server *interface_s;
opengl_receiver  *opengl_r;
opengl_frame     *opengl_fr;
app_parameters   *ap;
swap_controller  *swap_c;
WandVec          wand_vec;

GLfloat  position_0[4] = { 0.0, 10, 10.0, 1.0 }, // 0 = directional, 1 = location
         ambient_0[4] =  { 0.2, 0.2, 0.2, 0.2 },
         diffuse_0[4] =  { 1.0, 1.0, 1.0, 1.0 },
         specular_0[4] = { 0.0, 0.0, 0.0, 0.0 };


buttons *btns;

void reset_navigation() //need to have defined
{
	vrUserTravelReset(VR_ALLUSERS);
   opengl_fr->reset();
}

bool start_ml2vr()
{
   printf("start - setting up opengl_receiver, swap_controller, interface_server\n");

   read_config("ml2vr.cfg");
 
   ap=new app_parameters();
   btns=new buttons();

   //if(fw.getMaster())
	  swap_c=new swap_controller(swap_port,swap_clients); //TODO, not hardcoded
   
   opengl_r=new opengl_receiver(opengl_port,swap_ip,itoa(swap_port)); //every client has a receiver
  
   //if(fw.getMaster())
   {      
      interface_s=new interface_server(interface_port); 
   }
   ar_usleep(100000);

   opengl_fr=new opengl_frame();

   return true;
}

void init_gfx_ml2vr()
{
   glLightfv(GL_LIGHT0, GL_POSITION, position_0);
   glLightfv(GL_LIGHT0, GL_AMBIENT,  ambient_0);  
   glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuse_0);  
   glLightfv(GL_LIGHT0, GL_SPECULAR, specular_0); 
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glShadeModel(GL_SMOOTH);
   glColorMaterial(GL_FRONT, GL_DIFFUSE);
   glEnable(GL_COLOR_MATERIAL);
   glEnable(GL_LINE_SMOOTH);
   glEnable(GL_POINT_SMOOTH);
   glEnable(GL_NORMALIZE);
   
   init_display_list_cube();
   init_display_list_sphere(1);
   init_display_list_pyramid();
}

/* Define a structure for holding a database of the virtual world. */
typedef struct {

		vrLock		lock;				/* to avoid bad multiprocess accesses */
	} WorldDataType;


/*************************************************************************************/
/* These function declarations are very important -- they will be used as arguments. */
void	exit_application(int signal);		/* cleanly close down operations   */

void	init_gfx(void);				/* initialize the graphics         */
void	draw_world(WorldDataType *wd, vrRenderInfo *rendinfo); /* render the world */
void	init_world(WorldDataType *wd);		/* initialize the world parameters */
void	update_world(WorldDataType *wd);	/* perform the world simulation    */


/***********************************************************************/
main(int argc, char* argv[])
{
	WorldDataType	*wd;

	/**********************************/
	/*** initialize FreeVR routines ***/
	/**********************************/
	signal(SIGINT, SIG_IGN);
	vrConfigure(&argc, argv, NULL);		/* allow CLAs to affect config */
	vrStart();
	signal(SIGINT, exit_application);

	/********************/
	/* create the world */
	wd = (WorldDataType *)vrShmemAlloc0(sizeof(WorldDataType));
	wd->lock = vrLockCreate();
	init_world(wd);

	/***************************************/
	/* set up the FreeVR library functions */
	vrFunctionSetCallback(VRFUNC_DISPLAY_INIT, vrCallbackCreate(init_gfx_ml2vr, 0));
	vrFunctionSetCallback(VRFUNC_DISPLAY, vrCallbackCreate(draw_world, 1, wd));

	/********************************************************/
	/* define the application name, authors, controls, etc. */
	vrSystemSetName("ML2VR");
	vrSystemSetAuthors("David J. Zielinski");
	vrSystemSetExtraInfo("An example to demonstrate traveling relative to the virtual world");

	/***************************/
	/*** do world simulation ***/
	/***************************/
	vrUserTravelReset(VR_ALLUSERS);
	/* run until terminate key is pressed (Escape by default) */
	while(!vrGet2switchValue(0)) {
		vrFrame();
		update_world(wd);
	}

	/*********************/
	/*** close up shop ***/
	/*********************/
	exit_application(0);
}


/********************************************************************/
/* exit_application(): clean up anything started by the application */
/*   (forked processes, open files, open sockets, etc.)             */
/********************************************************************/
void exit_application(int signal)
{
	vrExit();
	exit(0);
}

/**************************************************************/
/* init_world(): create the initial conditions of the dynamic */
/*   simulation.                                              */
/**************************************************************/
void init_world(WorldDataType *wd)
{
	vrLockWriteSet(wd->lock);

	start_ml2vr();

	vrLockWriteRelease(wd->lock);
}

vrTime last_time;


/*********************************************************************/
/* update_world(): In this version, we handle the user interactions. */
/*   Both selecting, and moving the selected object.                 */
/*********************************************************************/
void update_world(WorldDataType *wd)
{
   vrMatrix sensor_matrix;

	vrTime		sim_time = vrCurrentSimTime();	/* the current time of the simulation  */
   double delta_seconds;

	vrLockWriteSet(wd->lock);

	/*****************************************************/
	/** Determine delta time from last simulation frame **/
	/*****************************************************/
	if (last_time == -1.0)
		delta_seconds = 0.0;
	else	delta_seconds = sim_time - last_time;
	last_time = sim_time;				/* now that delta_time has been calculated, we won't use last_time until next time */


   //////////////////////
   // NAVIGATION
   /////////////////////
   bool is_nav_waiting=true;

   while(is_nav_waiting) //consume all waiting nav events
   {
      navigation_event *ne;
      is_nav_waiting=interface_s->get_waiting_navigation_event(ne);
   
      if(is_nav_waiting)
      {
         if(ne->type==0)
            vrUserTravelTranslate3d(VR_ALLUSERS,ne->v1[0],ne->v1[1],ne->v1[2]);

         if(ne->type==1)
         {
            if(ne->v1==arVector3(1,0,0))
            		vrUserTravelRotateId(VR_ALLUSERS, VR_X, ne->a);
            if(ne->v1==arVector3(0,1,0))
            		vrUserTravelRotateId(VR_ALLUSERS, VR_Y, ne->a);
            if(ne->v1==arVector3(0,0,1))
            		vrUserTravelRotateId(VR_ALLUSERS, VR_Z, ne->a);
         }

         delete ne;
      }
   }

   /////////////////////////
   // Swap Processing 
   /////////////////////////
   int swap_frames=swap_c->get_num_frames_ready_for_swap();
 
   if(swap_frames>0)
	{
      printf("update_world - number of swap frames on the transfer field: %d\n",swap_frames);

	   unsigned int bsize;
	   unsigned char *d=opengl_r->_bq->pop_buffers(swap_frames,bsize);

	   opengl_fr->swap_new_frame(d,bsize);   

      swap_c->reduce_num_frames_ready_for_swap(swap_frames);
   }

   /////////////////////////////////
   // App Parameter Processing
   /////////////////////////////////
 
   single_app_param *sd;
   bool result=interface_s->get_waiting_app_parameter(sd);

   if(result)
   {
      ap->apply_single_parameter(sd->data);
   }
  
   /////////////////////////////////////
   // Wand Processing
   /////////////////////////////////////

   //TODO: do we need to handle wands dissapearing?  
   int num_wands=1; //fw.getNumberMatrices()-1; //TODO: how to query number of available sensors in FREE VR?
   int wand_vec_size=wand_vec.size();
   while(wand_vec_size<num_wands )
   {
      wand *w=new wand();
      wand_vec.push_back(w);
      wand_vec_size++;
      printf("update_world - expanding wand_vec. size now: %d\n",wand_vec_size);
   }
 
   //printf("updating wands...\n"); fflush(stdout);
   for(int i=0;i<wand_vec.size();i++)
   {       	
      vrMatrixGet6sensorValues(&sensor_matrix, i+1);

      wand_vec[i]->update(ap,arMatrix4(sensor_matrix.v[0],sensor_matrix.v[4],sensor_matrix.v[8],sensor_matrix.v[12],
                                       sensor_matrix.v[1],sensor_matrix.v[5],sensor_matrix.v[9],sensor_matrix.v[13],
                                       sensor_matrix.v[2],sensor_matrix.v[6],sensor_matrix.v[10],sensor_matrix.v[14],
                                       sensor_matrix.v[3],sensor_matrix.v[7],sensor_matrix.v[11],sensor_matrix.v[15]));
   }

   //printf("updating buttons\n"); fflush(stdout);
   for(int i=0;i<6;i++) //TODO: how to query number of buttons in freevr?
      btns->set_button(i,vrGet2switchValue(i+1));
 
   if(wand_vec.size()>0)
   {
      //printf("calling opengl_frame->process_wand\n"); fflush(stdout);
      opengl_fr->process_wand(wand_vec[0],btns,ap,delta_seconds); //TODO: which wand shouldn't be hardcoded. 
   }

   ar_timeval b=ar_time();

	vrLockWriteRelease(wd->lock);

   ///////////////////////////////////////////////////////////////////////////
   // let the event server know our current status (wand pos, buttons, axis)
   ///////////////////////////////////////////////////////////////////////////

   if(wand_vec.size()>0) 
   {
      if(1) //TODO: how query number of sensors available in FreeVR ?
      { 
         vrMatrixGet6sensorValues(&sensor_matrix, 0);

         //TODO do Navigate coordinate conversion in FreeVR. how?

         arMatrix4 head_fixed=arMatrix4(sensor_matrix.v[0],sensor_matrix.v[4],sensor_matrix.v[8],sensor_matrix.v[12],
                                       sensor_matrix.v[1],sensor_matrix.v[5],sensor_matrix.v[9],sensor_matrix.v[13],
                                       sensor_matrix.v[2],sensor_matrix.v[6],sensor_matrix.v[10],sensor_matrix.v[14],
                                       sensor_matrix.v[3],sensor_matrix.v[7],sensor_matrix.v[11],sensor_matrix.v[15]);

         interface_s->update_wand_matrix4(0,head_fixed);
      }
      if(wand_vec.size()>0)
      {
         arMatrix4 wp=wand_vec[0]->get_pos_full();
         interface_s->update_wand_matrix4(1,wp);  
      }
      for(int i=0;i<6;i++)
         interface_s->update_button(i,vrGet2switchValue(i+1));

      for(int i=0;i<2;i++)
         interface_s->update_joystick(i,vrGetValuatorValue(i));
   }   

  
	/*******************************************/
	/* allow other processes access to the CPU */
	vrSleep(0);
}

void draw_world(WorldDataType *wd, vrRenderInfo *rendinfo)
{
   //////////// CLEAR BUFFERS /////////////////////////
   glClearColor(ap->background_color[0],ap->background_color[1],ap->background_color[2],1); //world background color
   glClearDepth(1.0);
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

   glEnable(GL_LIGHTING);

   glColor3f(0,1,0);
   if(wand_vec.size()>0)
      wand_vec[0]->draw(ap);
  
   glPushMatrix();		/* save the real-world coordinate system */
      vrRenderTransformUserTravel(rendinfo);
      opengl_fr->render(ap);
   glPopMatrix();

	vrLockReadRelease(wd->lock);
}









