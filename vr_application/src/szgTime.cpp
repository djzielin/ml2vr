//********************************************************
// Syzygy is licensed under the BSD license v2
// see the file SZG_CREDITS for details
//********************************************************
// http://syzygy.isl.uiuc.edu/szg/index.html


#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <string>
#include <time.h>

#ifdef WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif

#include "szgTime.h"

void ar_usleep(int microseconds)
{
#if defined(WIN32)
  Sleep(microseconds/1000);
#else
  usleep(microseconds);
#endif
}


// Cross-platform clock.
#ifdef WIN32
// todo: assumes that ticks/second fits in a single int, not true in a few years.
ar_timeval ar_time(){
  LARGE_INTEGER clockTicks;
  LARGE_INTEGER ticksPerSecond;
  QueryPerformanceCounter(&clockTicks);
  QueryPerformanceFrequency(&ticksPerSecond);
  ar_timeval result;
  result.sec = clockTicks.QuadPart/ticksPerSecond.QuadPart;
  result.usec = (int)(1.e6*((clockTicks.QuadPart/(double)ticksPerSecond.QuadPart)
                            - (double)result.sec));
  return result;
}

#else

ar_timeval ar_time(){
  struct timeval tNow;
  gettimeofday(&tNow, 0);
  return ar_timeval(tNow.tv_sec, tNow.tv_usec);
}

#endif


double ar_difftime(const ar_timeval& a, const ar_timeval& b){
  return 1e6*(a.sec-b.sec) + (a.usec-b.usec);
}

