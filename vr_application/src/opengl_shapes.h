#ifndef CUBE_DZ_H
#define CUBE_DZ_H

#include <GL/gl.h>
#include <stdio.h>
#include <stdlib.h>

void GL_draw_cube();
void GL_draw_cube_wireframe();

void init_display_list_cube();
void draw_display_list_cube();
void draw_display_list_wireframe_cube();

#endif



#ifndef PYRAMID_DZ_H
#define PYRAMID_DZ_H

#include <GL/gl.h>
#include <stdio.h>
#include <stdlib.h>

void GL_draw_pyramid();
void GL_draw_pyramid_wireframe();

void init_display_list_pyramid();
void draw_display_list_pyramid();
void draw_display_list_wireframe_pyramid();

#endif



#ifndef SPHERE_DJZ_H
#define SPHERE_DJZ_H

#ifndef FREEVR
   #include "arGraphicsHeader.h" //include gl.h
#else
   #include <GL/gl.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


class sphere
{

public:
   sphere(int subdivision_factor=0);
   ~sphere();
   
   void draw();
   void draw_outline();

private:
   void alloc_memory_for_vertices();
   void calc_sphere_lines();
   unsigned int eliminate_duplicate_lines(GLfloat *vertices, unsigned int count);
   void push_vertex(GLfloat *v);
   void pop_vertex(float *ret);
   void add_subdivided(int current_level, int end_level);
   void add_original_triangle(int which_triangle, int end_level);

   unsigned int _vert_count;
   unsigned int _face_count;
   unsigned int _line_vert_count;
   unsigned int _current_pos; //used when calc'ing data
   int          _factor;

   GLfloat *_vertices;
   GLfloat *_line_vertices;

};

void init_display_list_sphere(int division);
void draw_display_list_sphere();
void draw_display_list_wireframe_sphere();

#endif
