#ifndef DJZ_SLICE_PLANE_H
#define DJZ_SLICE_PLANE_H

#ifndef FREEVR
   #include "arMath.h"
#else
   #include "szgMath.h"
#endif

#include "buttons.h"

class framework_slice
{
public:
   bool enable;
   float size; 
   arVector3 color;
   int button_id;
   bool is_global;

   arVector3 pos3;
   arVector3 plane_up;
   arVector3 plane_right;

   bool was_activated;
   int gl_plane;
   int _index;

   bool _is_clip_dragging;
   arVector3 _clip_start;
   arVector3 _clip_diff;

   double plane_parameters[4];
   arVector3 outline_points[4];

   framework_slice(int index);
   void begin();
   void end();
   void update(arVector3 p3_in_ref, buttons *btns);
};



#endif
