#ifndef DJZ_BUTTONS_H
#define DJZ_BUTTONS_H

class buttons
{
public:
   void set_button(int which, bool value)
   {
      int button_size=_button_vec.size();
      while(button_size<=which)
      {
         _button_vec.push_back(false);
         button_size++;   
         cout << "buttons - expanding button vec. size now: " << button_size << endl;  
      }
      _button_vec[which]=value;
   }

   bool get_button(int which) 
   {
      if(which>=_button_vec.size()) 
         return false;

      return _button_vec[which];
   }

private:
   vector <bool> _button_vec;
};

#endif