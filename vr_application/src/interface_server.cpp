/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "interface_server.h"

#if defined NO_SZG || defined FREEVR
  #include "szgTime.h"
#else
  #include "arMasterSlaveFramework.h"
#endif

bool interface_server::get_uchar(socket_ptr sock, unsigned char &value)
{
   boost::system::error_code error;
   unsigned char incoming_command;

   try
   {
      size_t length = boost::asio::read(*sock, boost::asio::buffer(&incoming_command,1), boost::asio::transfer_all(), error);

      if (error == boost::asio::error::eof || length==0)
      {
         printf("connection close by client\n");
         return false;
      }
      else if (error)
         throw boost::system::system_error(error); // Some other error.
   }
   catch (std::exception& e)
   {
     std::cerr << "Exception in thread: " << e.what() << "\n";
	 return false;
   }

   value=incoming_command;
   return true;
}

bool interface_server::send_uchar(socket_ptr sock, unsigned char val)
{
   boost::system::error_code error;
   
   try
   { 
      size_t length = boost::asio::write(*sock,boost::asio::buffer(&val,1), boost::asio::transfer_all(), error); 

      if (error == boost::asio::error::eof || length==0)
      {
         printf("connection close by client\n");
         return false;
      }
   }
   catch (std::exception& e)
   {
      printf("networking exception: %s",e.what());
      return false;
   }
   return true;
}

float interface_server::char_to_float(unsigned char *c)
{
   unsigned char reversed[4];

   for(int i=0;i<4;i++)
      reversed[i]=c[3-i];   
 
   float *val_p=(float *)reversed;
   return val_p[0];
}

bool interface_server::get_float(socket_ptr sock, float &val)
{
   boost::system::error_code error;

   unsigned char receive[4];

   try
   { 
      size_t length = boost::asio::read(*sock,boost::asio::buffer(receive,4), boost::asio::transfer_all(), error); 

      if (error == boost::asio::error::eof || length==0)
      {
         printf("connection close by client\n"); fflush(stdout);
         return false;
      }
   }
   catch (std::exception& e)
   {
      printf("networking exception: %s",e.what()); fflush(stdout);
      return false;
   }

   val=char_to_float(receive);

   return true;
}



bool interface_server::send_float(socket_ptr sock, float val)
{
   //printf("  sending float: %f\n",val);

   boost::system::error_code error;

   unsigned char *as_uchar=(unsigned char *)&val;
   unsigned char reverse[4];

   for(int i=0;i<4;i++) //TODO make byte order a config option
   {
      //printf("as uchar: %d\n",as_uchar[i]);
      reverse[3-i]=as_uchar[i];
   }
   try
   { 
      size_t length = boost::asio::write(*sock,boost::asio::buffer(reverse,4), boost::asio::transfer_all(), error); 

      if (error == boost::asio::error::eof || length==0)
      {
         printf("connection close by client\n"); fflush(stdout);
         return false;
      }
   }
   catch (std::exception& e)
   {
      printf("networking exception: %s",e.what()); fflush(stdout);
      return false;
   }

   //printf("  float send successfully!\n");
   return true;
}

void interface_server::update_wand_matrix4(int sensor_id, arMatrix4 m)
{
   sensor_lock->lock();
   recent_matrix4[sensor_id]=m;
   //cout << "in update_wand_matrix4: " << sensor_id << "matrix: " << m << endl;
   sensor_lock->unlock();
}

void interface_server::update_button(int button_num, bool pressed)
{
   button_lock->lock();

   current_button[button_num]=pressed; //TODO bounds check on button num
   bool current_button=previous_button[button_num]; //TODO bounds check on button_num
   int event;

   //printf("%d %d %d\n",button_num,pressed,current_button);

   if(current_button!=pressed)
   {
      printf("interface_server - we received a button change for %d. status: %d\n",button_num,pressed);
 
      if(pressed)
        event=1; 
      else
        event=2; //released


      button_list.push_back(new button_event(recent_matrix4[wand_sensor_id],button_num,event));


      previous_button[button_num]=pressed;
   }
   button_lock->unlock();
}

void interface_server::update_joystick(int axis, float val)
{
   joystick_lock->lock();

   current_joystick[axis]=val;

   float current_val=previous_joystick[axis];
 
   float mult_val=1.0/joystick_threshold;

   float expanded_val=val*mult_val; 
   if(val>0)
      expanded_val=floor(expanded_val);
   else
      expanded_val=ceil(expanded_val);

   float thresholded_val=expanded_val*joystick_threshold;

   //printf("axis: %d incoming value: %f thresholded to: %f\n",axis,val,thresholded_val);

   if(thresholded_val!=current_val)
   {
      printf("interface_server - previous %f != current %f: adding event to joystick queue\n",current_val,thresholded_val);

      joystick_list.push_back(new joystick_event(recent_matrix4[wand_sensor_id],axis,1,thresholded_val));


      previous_joystick[axis]=thresholded_val;
   }
   joystick_lock->unlock();
}

int frame=0;

bool interface_server::send_back_wand_location(int desired_return_type, arMatrix4 &matrix4, socket_ptr sock)
{
   if(desired_return_type==0) //send back pos3
   {
      //printf("sending back pos3\n");
      arVector3 pos3=ar_extractTranslation(matrix4);
      for (int i=0;i<3;i++)
      {
         if(!send_float(sock,pos3[i])) 
            return false;
      }
   }

   if(desired_return_type==1) //send back matrix4
   {
      //printf("sending back matrix4\n");
      for (int i=0;i<16;i++)
      {
         if(!send_float(sock,matrix4[i])) 
            return false;
      }
   }

   return true;
}     

bool interface_server::get_waiting_navigation_event(navigation_event *&ne)
{
   navigation_lock->lock();
   
   if(navigation_list.size()==0)
   {
      navigation_lock->unlock();
      return false;
   }

   ne=navigation_list.front();
   navigation_list.pop_front();
   navigation_lock->unlock();

   return true;
}

bool interface_server::process_joystick_event_request(socket_ptr sock)
{
//printf("user requested joystick event!\n");
        
   unsigned char desired_return_type;

   if(!get_uchar(sock,desired_return_type))
      return false;
      
   if(desired_return_type!=0 && desired_return_type!=1) 
   {
      printf("interface_server::process_joystick_event_request - user is requesting unknown return type: %d\n",desired_return_type);
      return false;
   }

   arMatrix4 jmatrix4;
   unsigned char event=0;
   unsigned char axis=0;
   float jvalue=0;

   joystick_lock->lock();

   if(joystick_list.size()==0)
   {
      joystick_lock->unlock();
      sensor_lock->lock();
      event=0; //no changes
      jmatrix4=recent_matrix4[wand_sensor_id];
      sensor_lock->unlock();
   }
   else
   {
      //printf("list size: %d\n",joystick_list.size());
      joystick_event *jv=joystick_list.front();
      joystick_list.pop_front();
      joystick_lock->unlock();

      jmatrix4=jv->matrix4;
      event=1;
      axis=jv->axis;         
      jvalue=jv->value;
                
      //printf("   found button: %d event: %d\n",button,event);
      delete jv;

   }

   //cout << "sending to matlab - joystick event: " << (int)event << " axis: " << (int)axis << " pos3: " << jpos << endl;

   if(!send_uchar(sock,event))  return false;
   if(!send_uchar(sock,axis))   return false;				    
   if(!send_float(sock,jvalue)) return false;

   if(!send_back_wand_location(desired_return_type, jmatrix4, sock))
      return false;

   return true;
}



bool interface_server::process_button_event_request(socket_ptr sock)
{
   ar_timeval a=ar_time();
   //printf("interface_server - user has requested button event status\n");

   unsigned char desired_return_type;

   if(!get_uchar(sock,desired_return_type)) return false;

   //printf("user request return type of: %d\n",desired_return_type);

   if(desired_return_type!=0 && desired_return_type!=1)
   {
      printf("interface_server::process_button_event_request - user is requesting unknown return type: %d\n",desired_return_type);
      return false; 
   }
 
   arMatrix4 bmatrix4;
   unsigned char event=0;
   unsigned char button=0;

   button_lock->lock();

   if(button_list.size()==0)
   {
      //printf("TO MATLAB: no change\n");
      button_lock->unlock();
      event=0; //no changes
      sensor_lock->lock();
      bmatrix4=recent_matrix4[wand_sensor_id];
      sensor_lock->unlock();
   }
   else
   {
      //printf("list size: %d\n",button_list.size());
      button_event *bv=button_list.front();
      button_list.pop_front();
      button_lock->unlock();

      bmatrix4=bv->matrix4;
      event=bv->event_type;
      button=bv->button;
                
      //printf("   found button: %d event: %d\n",button,event);

      delete bv;


   }

   //printf("  SENDING: event: %d\n",event);

   //TODO Package this up into one send command
  
      if(!send_uchar(sock,event))  return false;
      if(!send_uchar(sock,button)) return false;
      if(!send_back_wand_location(desired_return_type, bmatrix4, sock))
         return false;
  
   ar_timeval b=ar_time();

   //cout << "interface_server - time to send button event status: " << ar_difftime(b,a)/1e6 << " ms" << endl;

   return true;
}              

bool interface_server::process_sensor_location_request(socket_ptr sock)
{
   unsigned char desired_return_type;
   unsigned char sensor_id;

   if(!get_uchar(sock,desired_return_type)) return false;

   if(desired_return_type!=0 && desired_return_type!=1)
   {
      printf("interface_server::process_sensor_location_request - user is requesting unknown return type: %d\n",desired_return_type);
      return false; 
   }

   if(!get_uchar(sock,sensor_id))
      return false;

   sensor_lock->lock();
   arMatrix4 m4=recent_matrix4[sensor_id];
   sensor_lock->unlock();

   if(!send_back_wand_location(desired_return_type, m4, sock))
      return false;
 
   return true;
} 

bool interface_server::process_button_state_request(socket_ptr sock)
{
   unsigned char button_id;

   if(!get_uchar(sock,button_id))
      return false;

   button_lock->lock();
   bool b=current_button[button_id];
   button_lock->unlock();

   if(!send_uchar(sock,b))  
      return false;
 
   return true;
} 

bool interface_server::process_joystick_state_request(socket_ptr sock)
{
   unsigned char axis_id;

   if(!get_uchar(sock,axis_id))
      return false;

   joystick_lock->lock();
   float j=current_joystick[axis_id];
   joystick_lock->unlock();

   if(!send_float(sock,j))  
      return false;
 
   return true;
} 

 
bool interface_server::process_server_variable_request(socket_ptr sock)
{
   unsigned char function_id;

   if(!get_uchar(sock,function_id))
      return false;

   if(function_id==0) //set wand sensor id
   {
      button_lock->lock();
      joystick_lock->lock(); 

      unsigned char b;
      bool ret_val=get_uchar(sock,b);

      if(ret_val)
         wand_sensor_id=b;

      joystick_lock->unlock(); 
      button_lock->unlock();

      printf("interface_server - user just set wand sensor id to: %d\n",wand_sensor_id);

      if(!ret_val)
         return false;
   }
   if(function_id==1) //set joystick threshold
   {
      joystick_lock->lock(); 

      float f;
      bool ret_val;

      ret_val=get_float(sock,f);

      if(ret_val)
        joystick_threshold=f;
        
      joystick_lock->unlock();

      printf("interface_server - user just set joystick threshold to: %f\n",joystick_threshold);

      if(!ret_val)
         return false;
   }

   return true;
} 


bool interface_server::process_is_simulator_request(socket_ptr sock)
{
   misc_lock->lock();
   if(!send_uchar(sock,_is_simulator))  
      return false;
   misc_lock->unlock(); 

   return true;
} 


void interface_server::set_is_simulator(bool val)
{
   misc_lock->lock();
   _is_simulator=val;
   misc_lock->unlock(); 
}

bool interface_server::process_navigation_translate_request(socket_ptr sock)
{ 
   float v1,v2,v3;

   if(!get_float(sock, v1)) return false;
   if(!get_float(sock, v2)) return false;
   if(!get_float(sock, v3)) return false;

   //cout << "adding navigation translate: " << v1 << v2 << v3 << endl;

   navigation_event *ne=new navigation_event(arVector3(v1,v2,v3));

   navigation_lock->lock();
   navigation_list.push_back(ne);
   navigation_lock->unlock();
   return true;
}

bool interface_server::process_navigation_rotate_request(socket_ptr sock)
{ 
   float v1,v2,v3,v4;

   if(!get_float(sock, v1)) return false;
   if(!get_float(sock, v2)) return false;
   if(!get_float(sock, v3)) return false;
   if(!get_float(sock, v4)) return false;

   navigation_event *ne=new navigation_event(arVector3(v1,v2,v3),v4);

   navigation_lock->lock();
   navigation_list.push_back(ne);
   navigation_lock->unlock();

   return true;
}

void interface_server::session(socket_ptr sock)
{
   frame=0;

   //boost::asio::ip::tcp::no_delay option(true);
   //sock->set_option(option);

   cout << "interface_server - started new session" << endl;
   unsigned char command;

   for (;;)
   {
      if(!get_uchar(sock,command)) break;
      
      if(command==0) //button event request
      {
         if(!process_button_event_request(sock))
            break;
      }           
      if(command==1) //joystick event request
      {
         if(!process_joystick_event_request(sock))
            break;
      }
      if(command==2) //sensor state request
      {
         if(!process_sensor_location_request(sock))
            break;
      }
      if(command==3)
      {
         if(!process_navigation_translate_request(sock))
            break;
      }
      if(command==4)
      {
         if(!process_navigation_rotate_request(sock))
            break;
      }
      if(command==5)
      {
         if(!process_set_app_parameter(sock))
            break;
      }
      if(command==6) //clear event queues
      {
         printf("clearing event queues\n");
         button_lock->lock();
         button_list.clear();
         button_lock->unlock();

         joystick_lock->lock();
         joystick_list.clear();
         joystick_lock->unlock();

         app_param_lock->lock();
         app_param_list.clear();
         app_param_lock->unlock();

         navigation_lock->lock();
         navigation_list.clear();
         navigation_lock->unlock();

         printf("done clearing queues\n");
      }
      if(command==7) //button state request
      {
         if(!process_button_state_request(sock))
            break;
      }
      if(command==8) //axis state request
      {
         if(!process_joystick_state_request(sock))
            break;
      }
      if(command==9) //is simulator request
      {
        if(!process_is_simulator_request(sock))
           break;
      }
      if(command==10)
      {
        if(!process_server_variable_request(sock))
           break;
      }
  }
  
  printf("interface_server - exiting session\n");
}

void interface_server::server(short port)
{
   try
   {
      tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
      for (;;)
      {
         cout << "interface_server - waiting for a new connection on port: " << port << endl;
         socket_ptr sock(new tcp::socket(io_service));
         a.accept(*sock);
         sock->set_option(tcp::no_delay(true));
         boost::thread t(boost::bind(&interface_server::session, this, sock));
      }
   }
   catch (std::exception& e)
   {
      std::cerr << "Exception: " << e.what() << "\n";
   }
}

bool interface_server::get_waiting_app_parameter(single_app_param * &ret_val)
{
   app_param_lock->lock();
   
   if(app_param_list.size()==0)
   {
      app_param_lock->unlock();
      return false;
   }
   printf("interface_server - Number of parameter commands waiting in queue: %lu\n",app_param_list.size());

   ret_val=app_param_list.front();

   printf("  size of command is: %d\n",ret_val->size);

   app_param_list.pop_front();
   app_param_lock->unlock();
   //printf("done\n");
   return true;
}


bool interface_server::process_set_app_parameter(socket_ptr sock)
{
   printf("interface_server - user requested to set app parameters\n");

   unsigned char s;
   if(!get_uchar(sock,s)) return false;

   printf("  received size of: %d\n",s);

   unsigned char *data=new unsigned char[s];
   for(int i=0;i<s;i++)
   {
      unsigned char uc;
      if(!get_uchar(sock,uc)) return false;

      data[i]=uc;

      //printf("  got byte: %d\n",i);
   }
   printf("  got all the data\n");

   single_app_param *sap=new single_app_param(s,data);
   //printf("sap has size: %d\n",sap->size);

   //printf("pushing onto queue\n");
   app_param_lock->lock();
   app_param_list.push_back(sap);
   app_param_lock->unlock();

   //printf("done!\n");

   return true;
}                          

interface_server::interface_server(int port)
{
   printf("interface_server - setting up the interface_server for port: %d\n",port);

   //data_lock=new arLock();

   button_lock=new arLock();
   joystick_lock=new arLock();
   app_param_lock=new arLock();
   navigation_lock=new arLock();
   sensor_lock=new arLock();
   misc_lock=new arLock();

   boost::thread t( boost::bind( &interface_server::server, this, port));  
 
   for(int i=0;i<10;i++) //TODO, don't make max button number hardcoded
   {
     previous_button[i]=false;
     current_button[i]=false;
     previous_joystick[i]=0.0;
     current_joystick[i]=0.0;
     recent_matrix4[i]=ar_identityMatrix();
   }

   _is_simulator=false;
   wand_sensor_id=1;
   joystick_threshold=0.25;
}
