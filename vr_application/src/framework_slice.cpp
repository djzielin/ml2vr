#include "framework_slice.h"
#include <GL/gl.h>
#include <stdio.h>

framework_slice::framework_slice(int index)
{
   enable=false;
   size=5.0;
   color=arVector3(0,0,1);
   button_id=1;
   pos3=arVector3(0,0,0);
   plane_up=arVector3(0,1,0);
   plane_right=arVector3(0,0,1);
   was_activated=false;
   _index=index;
   gl_plane=GL_CLIP_PLANE0+index;
   is_global=true;
   _is_clip_dragging=false;
}


void framework_slice::begin()
{
   if(enable==false)
      return;

   //printf("framework_slice::begin - drawing outline\n");    

   glColor3f(color[0],color[1],color[2]);
 
   glBegin(GL_LINE_STRIP); //draw the clipping plane
      glVertex3fv(outline_points[0].v);
      glVertex3fv(outline_points[1].v);
      glVertex3fv(outline_points[2].v);
      glVertex3fv(outline_points[3].v);
      glVertex3fv(outline_points[0].v);
   glEnd();

   glClipPlane(gl_plane,plane_parameters);
   glEnable(gl_plane);
   was_activated=true;
}


void framework_slice::end()
{
   if(was_activated)
   {
      glDisable(gl_plane);
      was_activated=false;
   }
}

void framework_slice::update(arVector3 p3_in_ref, buttons *btns)
{
   if(enable==false)
      return;

   //printf("framework_slice::update - we are enabled\n");
   
   if(_is_clip_dragging)
   {
      if(btns->get_button(button_id)==false)
      {
         printf("we are ending the clip plane drag for %d\n",_index);
         _is_clip_dragging=false;
      }

      //adjust clip position here
      arVector3 norm=plane_right*plane_up;
      arVector3 new_pos=_clip_start+(p3_in_ref-_clip_diff);
      //cout << "p3_in_ref: " << p3_in_ref << endl;
      //cout << "norm: " << norm << endl;
      //cout << "calculated : " << new_pos << endl;
      arVector3 constrained_pt=ar_projectPointToLine(_clip_start, norm, new_pos);
      pos3=constrained_pt;
      //cout << "constrained: " << pos3 << endl;
   }
   else //determine if we should iniate any clip dragging
   {
      if(btns->get_button(button_id))
      {
         cout << "dragging of slice plane: " << _index << " has begun!" << endl;
         _is_clip_dragging=true;
         _clip_start=pos3;
         _clip_diff=p3_in_ref;        
      }
   }
          
   arVector3 plane_normal=plane_right*plane_up;
   arVector3 plane_point=pos3;

   float radius=size*0.5;
   //cout << " radius: " << radius << endl;
   outline_points[0]=plane_point+plane_up*radius-plane_right*radius;
   outline_points[1]=plane_point+plane_up*radius+plane_right*radius;
   outline_points[2]=plane_point-plane_up*radius+plane_right*radius;
   outline_points[3]=plane_point-plane_up*radius-plane_right*radius;

   //for(int i=0;i<4;i++)
   //printf("framework_slice::update - outline%d %f %f %f\n",i,outline_points[i][0],outline_points[i][1],outline_points[i][2]);


   plane_parameters[0]=plane_normal[0];
   plane_parameters[1]=plane_normal[1];
   plane_parameters[2]=plane_normal[2];
   plane_parameters[3]=-plane_point.dot(plane_normal);
}
