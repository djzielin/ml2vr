#ifndef DJZ_APP_PARAMETERS_H
#define DJZ_APP_PARAMETERS_H

#include <GL/gl.h>
#include <vector>

#if defined INCLUDE_ARLOCK || defined FREEVR
  #include "szgLock.h"
  #include "szgMath.h"
#else
  #include "arThread.h"
  #include "arMath.h"
#endif

class single_axis_navigation_parameters;

#include "framework_slice.h"

using namespace std;



class app_parameters
{
public:

   vector<single_axis_navigation_parameters *> nav_vec;
   vector<framework_slice *> slice_vec; 

   bool model_drag_enable;
   int model_drag_button_id;
   bool model_drag_constrain[6];
 
   arMatrix4 model_transform;
   arMatrix4 inverse_model_transform;

   
   bool wand_visable;
   float wand_length;
   float wand_width;
   bool wand_position_at_tip;
   float wand_push_forward;
   arVector3 wand_color;
   int wand_model;

   int wand_sensor_id;
   arVector3 background_color;

   bool local_slice_enable;

   bool fancy_drag_enable;
   bool fancy_drag_constrain[6];
   int  fancy_drag_trigger_type;
   int  fancy_drag_trigger_button;
   int  fancy_drag_release_type;
   int  fancy_drag_release_button;  

   app_parameters(); //constructor

   void apply_single_parameter(unsigned char *d);

private:
   void apply_navigation_parameters(unsigned char *d);
   void apply_app_parameter(unsigned char *d);
   void apply_slice_plane_parameters(unsigned char *d);
   void apply_model_drag_parameters(unsigned char *d);
   void apply_model_transform_parameters(unsigned char *d);
   void apply_grid_parameters(unsigned char *d);
   void apply_cursor_parameters(unsigned char *d);
   void apply_wand_parameters(unsigned char *d);
   void apply_joystick_parameters(unsigned char *d);
   void apply_environment_parameters(unsigned char *d);
   void apply_fancy_drag_parameters(unsigned char *d);
   void apply_local_slice_parameters(unsigned char *d);

   void reset_parameters();

};

#endif
