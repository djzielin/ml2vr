/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl_frame.h"
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include "opengl_shapes.h"
#include "buttons.h"
#include "framework_slice.h"

#ifdef FREEVR
  #include "freevr.h"
#endif

void opengl_frame::reset()
{
   _is_dragging=false;
   _show_bounding=false;
   _bounding_scale=1.0;
   _is_clip_dragging=false;
   _frame=0;

   _transform=ar_identityMatrix();
   _original_position=arVector3(0,0,0);
   _current_position=_transform*_original_position;

   _started_fancy_drag=false;
   _fancy_drag_button_history[0]=false;
   _fancy_drag_button_history[1]=false;
   _fancy_drag_release=false;
   _fancy_drag_trigger=false;
   
   _content_frame=0;
   _old_content_frame=0;
   _data_index=0;
   data_size=0;
   data=_data1;
}

opengl_frame::opengl_frame()
{
   data_lock=new arLock();

   //_log=fopen("frame_log.txt","w");
   //_log2=fopen("fr_rec_log.txt","w");

   _data1=(unsigned char *)malloc(sizeof(unsigned char) * 10000000);  //TODO: don't hardcode
   _data2=(unsigned char *)malloc(sizeof(unsigned char) * 10000000);   

   reset();
}

void opengl_frame::dump_packet(unsigned char *data, unsigned int size)
{
   char filename[100];
   sprintf(filename,"C:/tmp/frame_%d.txt",_frame);
   FILE *out=fopen(filename,"w");

   if(out==NULL) 
     return;

   fprintf(out,"frame size: %d\n",size); fflush(out);
   int i=0;
   //unsigned int *data_length=(unsigned int *)data;
   //fprintf(out,"data_length=%d\n",data_length[0]); fflush(out);
   //i+=4;

   GLfloat *v;
   unsigned int *u;

   for(;i<size;)
   {
     //fprintf(out,"at pos: %d\n",i); fflush(out);
     unsigned int command_length=data[i];
	  unsigned int command=data[i+1];
     
     //fprintf(out,"command length: %d\n",command_length); fflush(out);
     //fprintf(out,"command is: %d\n",command); fflush(out);
     switch(command)
	  {
      case 0: //glbegin
	     fprintf(out,"glBegin: %d\n",data[i+2]); fflush(out);
        break;
	  case 1: //glend
		  glEnd();
		  fprintf(out,"glEnd\n"); fflush(out);
		  break;
	  case 2: //glVertex3f 
        v=((GLfloat *)&data[i+2]);
		  fprintf(out,"  glVertex3f: %f %f %f \n",v[0],v[1],v[2]); fflush(out);
		  //glVertex3fv((GLfloat *)data[i+2]);
		 break;
	  case 3: //glColor4f
        v=((GLfloat *)&data[i+2]);
		  fprintf(out,"  glColor4f: %f %f %f %f\n",v[0],v[1],v[2],v[3]); fflush(out);
         // glColor4fv((GLfloat *)data[i+2]);
		  break;
      case 4:
         fprintf(out,"glPopMatrix\n");
         break;
      case 5:   
         fprintf(out,"glPushMatrix\n");
         break;
      case 6:
         fprintf(out,"glLoadIdentity\n");
         break;
      case 7:
         v=(GLfloat *)&data[i+2];
         fprintf(out,"glScalef %f %f %f\n",v[0],v[1],v[2]);
         break;        
      case 8:
         v=(GLfloat *)&data[i+2];
         fprintf(out,"glTranslatef %f %f %f\n",v[0],v[1],v[2]);
         break;
      case 9:
          v=(GLfloat *)&data[i+2];
          fprintf(out, "glRotatef: %f %f %f %f\n",v[0],v[1],v[2],v[3]);
          break; 
      case 10:
          v=(GLfloat *)&data[i+2];
          fprintf(out, "glLoadMatrixf %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15]);
          break;
      case 11:
          v=(GLfloat *)&data[i+2];
          fprintf(out, "glMultMatrixf %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15]);
          break;
      case 12:
          u=(unsigned int *)&data[i+2];
          fprintf(out, "glGenLists %d. master list: %d our list: %d\n",u[0],u[1],display_list_map[u[1]]);
          break;
      case 13:
          u=(unsigned int *)&data[i+2];
          fprintf(out, "glNewList %d-%d mode: %d\n",u[0],display_list_map[u[0]],u[1]);
          break;
      case 14:
          u=(unsigned int *)&data[i+2];
          fprintf(out, "glCallList %d %d\n",u[0],display_list_map[u[0]]);
          break;
      case 15:
          fprintf(out, "glEndList\n");
          break;
      case 0xff:
          fprintf(out, " SWAP\n");
          break;
      default:
          fprintf(out, " UNKNOWN COMMAND: %d\n",command);
          break;  
      }
	  i+=(1+command_length);
   }
   fclose(out);
}



bool is_fancy_drag_marker(unsigned int p, unsigned int max, unsigned char *data) 
{
   //printf("found a glBegin(GL_TRIANGLES) - testing for fancy drag marker\n");

   p+=3;
   if(p>=max) return false;

   for(int f=0;f<3;f++)
   {
      unsigned int command_length=data[p]+1;
      unsigned char  command=data[p+1];

      if(command!=2)
         return false;
      
      float *v=(GLfloat *)&data[p+2];
           
      if(v[0]!=0.0 || v[1]!=0.0 || v[2]!=0.0) return false;
      
      //printf("found vertex %d with xyz equal to 0.0\n",f+1);

      p+=command_length;
      if(p>=max) return false;
   }
   
   //printf("lets see if we now have a glEnd\n");

   if((p+1)>=max)
       return false;
 
   if(data[p+1]==1) //is our next command glEnd
   {
       //printf("YES! we have found a fancy drag marker!\n");
       return true;
   }
  
   return false;       
}

bool is_local_slice_marker(unsigned int p, unsigned int max, unsigned char *data, int &type, int &slice_num) 
{
   //printf("found a glBegin(GL_TRIANGLES) - testing for fancy drag marker\n");

   p+=3;
   if(p>=max) return false;
  
   bool first_time=true;

   for(int f=0;f<3;f++)
   {
      unsigned int command_length=data[p]+1;
      unsigned char  command=data[p+1];

      if(command!=2)
         return false;
      
      float *v=(GLfloat *)&data[p+2];
      //printf("examing triangle: %f %f %f\n",v[0],v[1],v[2]);

      if(v[0]!=1.0) return false;

      if(first_time) 
      {
         if(v[1]!=1.0 && v[1]!=0.0) return false;
 
         type=v[1];
         slice_num=v[2];
         first_time=false;
      }
      else
      {
         if(v[1]!=type) return false;
         if(v[2]!=slice_num) return false;
      }
      
      //printf("found vertex %d with xyz equal to 0.0\n",f+1);

      p+=command_length;
      if(p>=max) return false;
   }
   
   //printf("lets see if we now have a glEnd\n");

   if((p+1)>=max)
       return false;
 
   if(data[p+1]==1) //is our next command glEnd
   {
       //printf("YES! we have found a local slice marker!\n");
       return true;
   }
  
   return false;       
}


bool old_r=false;


void opengl_frame::_rend_with_local_slice(app_parameters *ap)
{
   //printf("at top of render with local slice\n");
   //printf("opengl_frame - in rend data size: %d\n",data_size);
   //for(int i=0;i<data_size;i++)
   //printf("%d ",data[i]);
   //printf("\n");
   
   if(data_size==0) return;
   //GLfloat *v;

   bool seen_fancy_marker=false;

   int slice_type,slice_num;

   for(int i=0;i<data_size;)
   {
      unsigned int command_length=data[i]+1;
	   unsigned int command=data[i+1];   

      if(command==0)
      {
         //printf("glBegin: %d\n",data[i+2]);
         short begin_type=(short)data[i+2];

         if(begin_type==GL_TRIANGLES)
         {
            if(is_local_slice_marker(i,data_size,data,slice_type,slice_num))
            {
               if(slice_type==0)
               { 
                  printf("activating local slice plane\n");
                  ap->slice_vec[slice_num]->begin();
               }
               else
               {
                  printf("deactivating local slice plane\n");
                  ap->slice_vec[slice_num]->end();
               }
      
              //printf("yeah, we have the local slice marker");
               i+=47; //skip past everything
               continue;
            }  
         }
            
	      glBegin(begin_type);

         i+=command_length;

         command=data[i+1];
         command_length=data[i]+1;

         while(command!=1 && i<data_size)
         {
            //printf("command: %d\n",command);
            //printf("command_length: %d\n",command_length);

            if(command==2)
            {  
              //printf("  glVertex\n");
              GLfloat *v=(GLfloat *)&data[i+2];

              //glVertex3fv((GLfloat *)&data[i+2]);
              glVertex3f(v[0],v[1],v[2]);
              //printf("    %f %f %f\n",v[0],v[1],v[2]);
            }
            if(command==3)
            {
              //printf("  glColor\n");
              GLfloat *v=(GLfloat *)&data[i+2];
              glColor4f(v[0],v[1],v[2],v[3]);
              //printf("    %f\n",v[0],v[1],v[2],v[3]);
            }
            i+=command_length;
            command=data[i+1];
            command_length=data[i]+1;
         }

         glEnd();
         i+=command_length;
      }
       else if(command==3)
       {
            //printf("  glColor\n");
            //glColor4fv((GLfloat *)&data[i+2]); //this caused Bill's system to blow up

            GLfloat *v=(GLfloat *)&data[i+2];
            glColor4f(v[0],v[1],v[2],v[3]);    //do this instead [ie, pass data values directly]

            i+=command_length;
      }
      else
      {
         //printf("skipping some other command: %d\n",command);
         i+=(command_length);
      }
   }

   if(seen_fancy_marker==false)
   {
      if(_started_fancy_drag==true)
      {
         printf("fancy drag has ended on frame: %d\n",_frame);
         _started_fancy_drag=false;
         _fancy_drag_release=false;
         //printf(" final transform of %f %f %f\n",_fancy_recent[12],_fancy_recent[13],_fancy_recent[14]);
    
         old_r=false;
      }
   }
}


void opengl_frame::_rend_with_fancy_drag(app_parameters *ap)
{


   //printf("opengl_frame - in rend data size: %d\n",data_size);
   //for(int i=0;i<data_size;i++)
   //printf("%d ",data[i]);
   //printf("\n");
   
   if(data_size==0) return;
   //GLfloat *v;

   bool seen_fancy_marker=false;


   for(int i=0;i<data_size;)
   {
      unsigned int command_length=data[i]+1;
	   unsigned int command=data[i+1];   

      if(command==0)
      {
         //printf("glBegin: %d\n",data[i+2]);
         short begin_type=(short)data[i+2];

         if(begin_type==GL_TRIANGLES)
         {
            if(is_fancy_drag_marker(i,data_size,data))
            {
               if(seen_fancy_marker==false)
               { 
                  if(_started_fancy_drag==false)
                  {
                     printf("initiating fancy drag! at frame: %d\n",_frame);

                     if(_fancy_drag_trigger==true)
                     {
                         _fancy_drag_offset=_m4_when_triggered.inverse(); //use trigger for more accuracy
                         //printf("  using triggered pos %f %f %f\n",_m4_when_triggered[12],_m4_when_triggered[13],_m4_when_triggered[14]);
                         //printf("  inverse: %f %f %f\n", _fancy_drag_offset[12], _fancy_drag_offset[13], _fancy_drag_offset[14]);
                     }
                     else
                         _fancy_drag_offset=m4.inverse();

                     _started_fancy_drag=true;
                     _fancy_drag_trigger=false;
                     _fancy_recent=ar_identityMatrix();
                  }
 
                  if(_fancy_drag_release==false ||  //watch release for more accuracy
                    (old_r!=_fancy_drag_release))   //if released, do one more update
                  {
                     arMatrix4 m=m4*_fancy_drag_offset;
                     if(ap->fancy_drag_constrain[0]==true)
                        m[12]=0.0;
                     if(ap->fancy_drag_constrain[1]==true)
                        m[13]=0.0;
                     if(ap->fancy_drag_constrain[2]==true)
                        m[14]=0.0;
                     _fancy_recent=m;
                  }
    
                  old_r=_fancy_drag_release;                    
                  glPushMatrix();
                  glMultMatrixf(_fancy_recent.v);
   
                  seen_fancy_marker=true;
               }
               else //second time we see the marker, get back to original transform
               {
                   glPopMatrix();
               }
              

               //printf("yeah, we have the fancy drag marker");
               i+=47; //skip past everything
               continue;
            }  
         }
            

	      glBegin(begin_type);

         i+=command_length;

         command=data[i+1];
         command_length=data[i]+1;

         while(command!=1 && i<data_size)
         {
            //printf("command: %d\n",command);
            //printf("command_length: %d\n",command_length);

            if(command==2)
            {  
              //printf("  glVertex\n");
              GLfloat *v=(GLfloat *)&data[i+2];

              //glVertex3fv((GLfloat *)&data[i+2]);
              glVertex3f(v[0],v[1],v[2]);
              //printf("    %f %f %f\n",v[0],v[1],v[2]);
            }
            if(command==3)
            {
              //printf("  glColor\n");
              GLfloat *v=(GLfloat *)&data[i+2];
              glColor4f(v[0],v[1],v[2],v[3]);
              //printf("    %f\n",v[0],v[1],v[2],v[3]);
            }
            i+=command_length;
            command=data[i+1];
            command_length=data[i]+1;
         }

         glEnd();
         i+=command_length;
      }
       else if(command==3)
       {
            //printf("  glColor\n");
            //glColor4fv((GLfloat *)&data[i+2]); //this caused Bill's system to blow up

            GLfloat *v=(GLfloat *)&data[i+2];
            glColor4f(v[0],v[1],v[2],v[3]);    //do this instead [ie, pass data values directly]

            i+=command_length;
      }
      else
      {
         //printf("skipping some other command: %d\n",command);
         i+=(command_length);
      }
   }

   if(seen_fancy_marker==false)
   {
      if(_started_fancy_drag==true)
      {
         printf("fancy drag has ended on frame: %d\n",_frame);
         _started_fancy_drag=false;
         _fancy_drag_release=false;
         //printf(" final transform of %f %f %f\n",_fancy_recent[12],_fancy_recent[13],_fancy_recent[14]);
    
         old_r=false;
      }
   }
}

void opengl_frame::_rend()  
{
   //printf("opengl_frame content: %d - rendering data size: %d\n",_content_frame,data_size);
   //for(int i=0;i<data_size;i++)
   //printf("%d ",data[i]);
   //printf("\n");
 
   if(data_size==0) 
   {
     return;
   }
   //printf("entered _rend()\n"); fflush(stdout);
  
   for(int i=0;i<data_size;)
   {
      unsigned int command_length=data[i]+1;
	   unsigned int command=data[i+1];   

      if(command==0) //TODO, use switch statement instead?
      {
          //printf("glBegin: %d\n",data[i+2]);
	      glBegin((short)data[i+2]);

         i+=command_length;

         command=data[i+1];
         command_length=data[i]+1;

         while(command!=1 && i<data_size)
         {
            //printf("command: %d\n",command);
            //printf("command_length: %d\n",command_length);

            if(command==2)
            {  
              //printf("  glVertex\n");
              GLfloat *v=(GLfloat *)&data[i+2];

              //glVertex3fv((GLfloat *)&data[i+2]);
              glVertex3f(v[0],v[1],v[2]);
              //printf("    %f %f %f\n",v[0],v[1],v[2]);
            }
            if(command==3)
            {
              //printf("  glColor\n");
              GLfloat *v=(GLfloat *)&data[i+2];
              glColor4f(v[0],v[1],v[2],v[3]);
              //printf("    %f\n",v[0],v[1],v[2],v[3]);
            }
            i+=command_length;
            command=data[i+1];
            command_length=data[i]+1;
         }

         glEnd();
         i+=command_length;
      }
       else if(command==3)
       {
            //printf("  glColor\n");
            //glColor4fv((GLfloat *)&data[i+2]); //this caused Bill's system to blow up

            GLfloat *v=(GLfloat *)&data[i+2];
            glColor4f(v[0],v[1],v[2],v[3]);    //do this instead [ie, pass data values directly]

            i+=command_length;
      }
      /*else if(command==4)  //TODO: fix intercept to make this work again
      {  
         glPopMatrix();  
         i+=command_length;       
      }
      else if(command==5)
      {
         glPushMatrix();
         i+=command_length;
      }
      else if(command==6)
      {
         //glLoadIdentity();  //why does this make the scene a mess?DUH, because its OVERWRITING the matrix
         
      
     
		 glLoadMatrixf(_post_transform_matrix); //do this instead

         i+=command_length;
      }
      else if(command==7)
      {
          GLfloat *v=(GLfloat *)&data[i+2];
          glScalef(v[0],v[1],v[2]);
          i+=command_length;
      }
      else if(command==8)
      {
          GLfloat *v=(GLfloat *)&data[i+2];
          glTranslatef(v[0],v[1],v[2]);
          i+=command_length;
      }
      else if(command==9)
      {  
          GLfloat *v=(GLfloat *)&data[i+2];
          glRotatef(v[0],v[1],v[2],v[3]);
          //printf("glRotatef: %f %f %f %f\n",v[0],v[1],v[2],v[3]);
          i+=command_length;
      }
      else if(command==10)
      {  
          GLfloat *v=(GLfloat *)&data[i+2];
          //glLoadMatrixf(v); //this is bad too
          
          //printf("ignoring load for the moment\n");
          glLoadMatrixf(_post_transform_matrix);
          glMultMatrixf(v);

          i+=command_length;
      }
      else if(command==11)
      {  
          GLfloat *v=(GLfloat *)&data[i+2];
          glMultMatrixf(v);
          i+=command_length;
      }
      else if(command==12)
      {
          unsigned int *v=(unsigned int *)&data[i+2];
          unsigned int result=glGenLists(v[0]);
          if(v[0]>1)
            printf("ERROR: not sophistacted to handle mutiple display lists created at once\n"); //TODO. fix this!
 
          display_list_map[v[1]]=result; //TODO: do something more elegant. hashing?
                    i+=command_length;
      }
      else if(command==13)
      {
          unsigned int *v=(unsigned int *)&data[i+2];     
          glNewList(display_list_map[v[0]], v[1]);
          i+=command_length;
      }
      else if(command==14)
      {
         unsigned int *v=(unsigned int *)&data[i+2];
         glCallList(display_list_map[v[0]]);
          i+=command_length;

      }
      else if(command==15)
      {
         glEndList();
         i+=command_length;

      }
       */
      else 
      {
         //printf("skipping some other command: %d\n",command);
         i+=(command_length);
      }
   }

   if(data_size>0 && _old_content_frame!=_content_frame) 
   {
      printf("dumping packet now\n"); fflush(stdout);
      //dump_packet(data,data_size);
      //exit(1);
   }
   _old_content_frame=_content_frame;

   //printf("done with _rend\n"); fflush(stdout);
}

void opengl_frame::render(app_parameters *ap)
{
   //printf("entered opengl_frame::render. frame: %d\n",_frame); fflush(stdout);

   _final_transform=_transform*ap->model_transform;
   glPushMatrix();
  
   glMultMatrixf(_final_transform.v); //DJZ
   

   
   //cout << _transform << endl;
   //cout << ap->model_transform << endl;
   
   data_lock->lock(); //TODO: actually should lock better, so cant' get content switch in between eyes
   

   glGetFloatv (GL_MODELVIEW_MATRIX, _post_transform_matrix);
  
   //fprintf(_log,"-----------FRAME-------------\n");

   glDisable(GL_LIGHTING);

   //printf("setting up slice planes. vec size: %d\n",ap->slice_vec.size());
   for(int i=0;i<ap->slice_vec.size();i++) //setup slice planes
   {
       if(ap->slice_vec[i]->is_global)
         ap->slice_vec[i]->begin();
   }
		 
   //glPushMatrix();
   //glTranslatef(0,5,0);
   //glutSolidSphere(0.25,20,20);
   //glPopMatrix();
   
		 
		 
   if(ap->fancy_drag_enable)
   {
      //printf("calling _rend_with_fancy_drag\n");
      _rend_with_fancy_drag(ap);    //TODO, combine all these _rend functions
   }
   else if(ap->local_slice_enable)
   { 
      //printf("calling _rend_with_slice\n");
     _rend_with_local_slice(ap);
   }
   else
   {
     //printf("calling regular rend!\n");
     _rend();
   }
   
   for(int i=0;i<ap->slice_vec.size();i++) //shutdown slice planes
      ap->slice_vec[i]->end();

 
   glPopMatrix(); 

   glEnable(GL_LIGHTING);

   //printf("in render. unlock\n");
   data_lock->unlock();

   _frame++; 
   //printf("leaving render\n");

}

void opengl_frame::process_wand(wand *w, buttons *btns, app_parameters *ap, double delta_seconds)  
{
   m4=w->get_pos_full();
   arMatrix4 inv=ap->inverse_model_transform*_transform.inverse();  
  // cout << "inv: " << inv << endl;

   _m4_in_ref=inv*m4;
   //cout << "in process_wand: _m4_in_ref" << _m4_in_ref << endl;
   //cout << "m4: " << m4 << endl;

   arVector3 p3_in_ref=ar_extractTranslation(_m4_in_ref);  

   if(ap->fancy_drag_enable)
   {
      bool trigger_button=btns->get_button(ap->fancy_drag_trigger_button);
      bool release_button=btns->get_button(ap->fancy_drag_release_button);

      //if(trigger_button) printf("FD: trigger pressed\n");
      //if(release_button) printf("FD: release pressed\n");

      if(_started_fancy_drag==false)
      {
          bool trigger_pressed=trigger_button && _fancy_drag_button_history[0]==false;
          bool trigger_released=trigger_button==false && _fancy_drag_button_history[0];
 
          if(ap->fancy_drag_trigger_type==1 && trigger_pressed ||
             ap->fancy_drag_trigger_type==2 && trigger_released) //initiate fancy drag
          {
             _m4_when_triggered=m4;
             _fancy_drag_trigger=true;
             printf("fancy drag trigger at frame: %d\n",_frame);
          }   
      }
      else
      {
          bool release_pressed=release_button && _fancy_drag_button_history[1]==false;
          bool release_released=release_button==false && _fancy_drag_button_history[1];

          if(ap->fancy_drag_release_type==1 && release_pressed ||
             ap->fancy_drag_release_type==2 && release_released) //initiate fancy drag
          {
             _fancy_drag_release=true;
             printf("fancy drag release at: %d\n",_frame);
             //printf("   using of %f %f %f\n",m4[12],m4[13],m4[14]);

          }
      }
 
      _fancy_drag_button_history[0]=trigger_button;
      _fancy_drag_button_history[1]=release_button;
   }

   //printf("about to get button\n");fflush(stdout);
   if(btns->get_button(ap->model_drag_button_id) && ap->model_drag_enable)
   {
      if(_is_dragging==false) //start drag
      { 
         cout << "starting drag" << endl;
         _is_dragging=true;
         _stored_transform=_transform; 
         _drag_offset=m4.inverse();
         _show_bounding=true;
         return;
      }

      if(_is_dragging) //update. TODO: somethign to signify dragging?
      {   
         arMatrix4 m=m4*_drag_offset;
         if(ap->model_drag_constrain[0]==true)
            m[12]=0.0;
         if(ap->model_drag_constrain[1]==true)
            m[13]=0.0;
         if(ap->model_drag_constrain[2]==true)
            m[14]=0.0;

         _transform=m*_stored_transform;  
	 return;
      }
   }
   else //no longer pressing
   {
      if(_is_dragging) //we were dragging, stop now. 
      {
         _is_dragging=false;
         _show_bounding=false;
         
         arMatrix4 m=m4*_drag_offset;
         if(ap->model_drag_constrain[0]==true)
            m[12]=0.0;
         if(ap->model_drag_constrain[1]==true)
            m[13]=0.0;
         if(ap->model_drag_constrain[2]==true)
            m[14]=0.0;

         _transform=m*_stored_transform;  

         _current_position=_transform*_original_position;
         cout << "ending drag" << endl;
      }    
   }
  
   for(int i=0;i<ap->slice_vec.size();i++) //setup slice planes
      ap->slice_vec[i]->update(p3_in_ref,btns);

   //printf("made it past get button\n"); fflush(stdout);
   return;
}

void opengl_frame::swap_new_frame(unsigned char *b, unsigned int length)
{
   printf("opengl_frame - swap in new frame of size: %d\n",length); fflush(stdout);

   unsigned char *destination;
   if(_data_index==0)           //new, switch between 2 buffers so that opengl v commands work properly (and also don't blowup bill's system).
   {
      destination=_data2;
      _data_index=1;
   }
   else
   {
      destination=_data1;
      _data_index=0;
   }

   memcpy(destination,b,length);
   data=destination;

   data_size=length;
   //dump_packet(data,data_size);
   _content_frame++;

   printf("swap completed\n"); fflush(stdout);
}

