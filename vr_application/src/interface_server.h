#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio/read.hpp>
#include "app_parameters.h"

using boost::asio::ip::tcp;
using namespace std;

typedef boost::shared_ptr<tcp::socket> socket_ptr;

class button_event
{
public:
   button_event(arMatrix4 m, int b, int et) { matrix4=m; button=b; event_type=et; }

   arMatrix4 matrix4;

   int button;
   int event_type; //1==press, 2==release

	// time stamp? something else?
};

class joystick_event
{
public:
   joystick_event(arMatrix4 m, int a, int et, float v) { matrix4=m; axis=a; event_type=et; value=v; }

   arMatrix4 matrix4;

   int axis;
   int event_type; //0 == no change, 1==change
   float value;
};

class navigation_event
{
public:
   navigation_event(arVector3 translate) { type=0; v1=translate; }
   navigation_event(arVector3 rotate_axis, float amount) { type=1; v1=rotate_axis; a=amount; }

   int type;
   arVector3 v1;
   float a;
};

class single_app_param
{
public:
   int size;
   unsigned char *data;

   single_app_param(int s, unsigned char *d) { size=s; data=d; }

   ~single_app_param()
   {
      delete data;
   }
};

class interface_server
{
public:
   interface_server(int port);

   void update_wand_matrix4(int sensor_id, arMatrix4 m);
   void update_button(int button_num, bool pressed);
   void update_joystick(int axis, float val);

   bool get_waiting_navigation_event(navigation_event *&ne);
   bool get_waiting_app_parameter(single_app_param *&ret_val);

   void set_is_simulator(bool val);

private:
   bool process_sensor_location_request(socket_ptr sock);
   bool process_button_event_request(socket_ptr sock);
   bool process_joystick_event_request(socket_ptr sock);
   bool send_back_wand_location(int desired_return_type, arMatrix4 &matrix4, socket_ptr sock);
   bool process_navigation_rotate_request(socket_ptr sock);
   bool process_navigation_translate_request(socket_ptr sock);
   bool process_set_app_parameter(socket_ptr sock);
   bool process_button_state_request(socket_ptr sock);
   bool process_joystick_state_request(socket_ptr sock);
   bool process_is_simulator_request(socket_ptr sock);
   bool process_server_variable_request(socket_ptr sock);
 
   bool get_uchar(socket_ptr sock, unsigned char &value);
   bool send_uchar(socket_ptr sock, unsigned char val);
   bool get_float(socket_ptr sock, float &val);
   bool send_float(socket_ptr sock, float val);
   float char_to_float(unsigned char *c);

   void session(socket_ptr sock);
   void server(short port);

   boost::asio::io_service io_service;
  
   arLock *button_lock;
   arLock *sensor_lock;
   arLock *joystick_lock;
   arLock *app_param_lock;
   arLock *navigation_lock;
   arLock *misc_lock;

   list<button_event *> button_list;
   list<joystick_event *> joystick_list;
   list<single_app_param *> app_param_list;
   list<navigation_event *> navigation_list;

   bool previous_button[10]; //TODO make this a vector, ie don't hardcore max, ugh yes this is a big issue. don't hardcode! otherwise we get into big problems 8/12/2015
   bool current_button[10];
   arMatrix4 recent_matrix4[10];
   float previous_joystick[10];   
   float current_joystick[10];
  
   bool _is_simulator;
   float joystick_threshold;
   int wand_sensor_id;
};
