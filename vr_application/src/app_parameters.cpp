/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include "app_parameters.h"
#include "framework_navigation.h"

void reset_navigation(); //defined in ml2vr_szg_app

float char_to_float(unsigned char *c)
{
   unsigned char reversed[4];

   for(int i=0;i<4;i++)
      reversed[i]=c[3-i];   
 
   float *val_p=(float *)reversed;
   return val_p[0];
}

void app_parameters::reset_parameters()
{
   model_drag_enable=false;
   model_drag_button_id=0;
   for(int i=0;i<6;i++)
      model_drag_constrain[i]=false;

   model_transform=ar_identityMatrix();

   wand_visable=true;
   wand_length=0.25;
   wand_width=0.25;
   wand_push_forward=0.25;
   wand_color=arVector3(1,0,0);
   wand_model=0;
   wand_position_at_tip=true;

   wand_sensor_id=1;

   background_color=arVector3(0.5,0.5,0.5); //was 0.8 0.8 0.8

   nav_vec.clear();

   for(int i=0;i<2;i++) //two axis for navigation, x and y. TODO, should probably be more flexible about this
   {
      single_axis_navigation_parameters *sanp=new single_axis_navigation_parameters(i);       
      nav_vec.push_back(sanp);
   }

   fancy_drag_enable=false;
   fancy_drag_release_type=2;
   fancy_drag_release_button=5;
   fancy_drag_trigger_type=1;
   fancy_drag_trigger_button=5;

   local_slice_enable=false;

   for(int i=0;i<6;i++)
      fancy_drag_constrain[i]=false;

   slice_vec.clear();
   for(int i=0;i<2;i++) //TODO: be more flexible about expanding slice plane pool
   {
      framework_slice *fs=new framework_slice(i);     
      slice_vec.push_back(fs);
   }
}

app_parameters::app_parameters()
{
   reset_parameters();
}

void app_parameters::apply_single_parameter(unsigned char *d)
{
   int feature=d[0];
 
   if(feature==0) //navigation
      return apply_navigation_parameters(d); //TODO, this whole return doesn't make sense, now that nothing is returning from the sub functions
   if(feature==1) //slice plane
      return apply_slice_plane_parameters(d);
   if(feature==2) //model drag
      return apply_model_drag_parameters(d);
   if(feature==3) //model transform
      return apply_model_transform_parameters(d);
   if(feature==4) //grid
      return apply_grid_parameters(d);
   if(feature==5) //cursor
      return apply_cursor_parameters(d);
   if(feature==6) //wand 
      return apply_wand_parameters(d);
   if(feature==7) //joystick
      return apply_joystick_parameters(d);
   if(feature==8) //environment
      return apply_environment_parameters(d);
   if(feature==9) //fancy drag
      return apply_fancy_drag_parameters(d);
   if(feature==10) //reset request
   {
      printf("user requested reset of app parameters\n");
      reset_parameters();
      reset_navigation();
      return;
   }
   if(feature==11) //local slice
      return apply_local_slice_parameters(d);   
   
   if(feature>11)
   {
      printf("feature %d is out of range\n",feature);
      exit(1);
   }
}

void app_parameters::apply_environment_parameters(unsigned char *d)
{
   printf("  user requested to set environment\n");

   unsigned char parameter=d[1];

   if(parameter==0) //background color
   {    
      float v1,v2,v3;
      v1=char_to_float(&d[2]);
      v2=char_to_float(&d[6]);
      v3=char_to_float(&d[10]);

      printf("    setting background color: %f %f %f\n",v1,v2,v3);

      background_color=arVector3(v1,v2,v3);
   }
 
}

void app_parameters::apply_local_slice_parameters(unsigned char *d)
{
   printf("  user requested to set local slice parameter\n");

   unsigned char parameter=d[1];

   if(parameter==0) //enable
   {
      unsigned char value=d[2];

      printf("    setting enable: %d\n",value);
      local_slice_enable=value;
   }
}

void app_parameters::apply_fancy_drag_parameters(unsigned char *d)
{
   printf("  user requested to set fancy drag parameter\n");

   unsigned char parameter=d[1];

   if(parameter==0) //enable
   {
      unsigned char value=d[2];

      printf("    setting enable: %d\n",value);
      fancy_drag_enable=value;
   }
   if(parameter==1) //constrain
   {
      for(int i=0;i<6;i++)
      {
         unsigned char value=d[2+i];

         printf("    setting constrain %d: %d\n",i,value);
         fancy_drag_constrain[i]=value;
      }
   }

   if(parameter==2) //trigger type
   {
      unsigned char value=d[2];

      printf("    setting trigger type: %d\n",value);
      fancy_drag_trigger_type=value;
   }
   if(parameter==3) //trigger button
   {
      unsigned char value=d[2];

      printf("    setting trigger button: %d\n",value);
      fancy_drag_trigger_button=value;
   }

   if(parameter==4) //release type
   {
      unsigned char value=d[2];

      printf("    setting release type: %d\n",value);
      fancy_drag_release_type=value;
   }
   if(parameter==5) //release button
   {
      unsigned char value=d[2];

      printf("    setting release button: %d\n",value);
      fancy_drag_release_button=value;
   }

}

void app_parameters::apply_navigation_parameters(unsigned char *d)
{
   printf("  applying navigation parameters\n");

   unsigned char parameter=d[1]; 

   if(parameter==0) //axis  func
   {
      unsigned char axis_id=d[2];
      unsigned char value=d[3];

      printf("  axis: %d\n",axis_id);

      printf("    setting func to value: %d\n",value);
      nav_vec[axis_id]->set_func(value);
   }
   if(parameter==1) //axis x mult
   {
      unsigned char axis_id=d[2];
      float value=char_to_float(&d[3]);

      printf("  axis: %d\n",axis_id);

      printf("    setting x mult to: %f\n",value);
      nav_vec[axis_id]->navigation_mult=value;
   }
  
   if(parameter==2) //constrain
   {
      unsigned char v1,v2,v3;
      v1=d[2];
	   v2=d[3];
	   v3=d[4];

      printf("    setting constrain: %d %d %d\n",v1,v2,v3);

      for(int i=0;i<nav_vec.size();i++)
      {
         nav_vec[i]->navigation_constrain[0]=v1;
         nav_vec[i]->navigation_constrain[1]=v2;
         nav_vec[i]->navigation_constrain[2]=v3;
      }
   }
}

void app_parameters::apply_slice_plane_parameters(unsigned char *d)
{
   printf("  user requested to set slice plane parameters\n");

   unsigned char parameter=d[1];
   unsigned char plane_id=d[2];

   printf("  setting plane: %d\n",plane_id);

   if(parameter==0) //enable
   {
      unsigned char value=d[3];

      printf("    setting enable: %d\n",value);
      slice_vec[plane_id]->enable=value;
   }
   if(parameter==1) //pos3
   {
      float v1,v2,v3;
      v1=char_to_float(&d[3]);
      v2=char_to_float(&d[7]);
      v3=char_to_float(&d[11]);

      printf("    setting pos3: %f %f %f\n",v1,v2,v3);
      slice_vec[plane_id]->pos3=arVector3(v1,v2,v3);
   }
   if(parameter==2) //up3
   {
      float v1,v2,v3;
      v1=char_to_float(&d[3]);
      v2=char_to_float(&d[7]);
      v3=char_to_float(&d[11]);

      printf("    setting up3: %f %f %f\n",v1,v2,v3);
      slice_vec[plane_id]->plane_up=arVector3(v1,v2,v3);
   }
   if(parameter==3) //right3
   {
      float v1,v2,v3;
      v1=char_to_float(&d[3]);
      v2=char_to_float(&d[7]);
      v3=char_to_float(&d[11]);

      printf("    setting right3: %f %f %f\n",v1,v2,v3);
      slice_vec[plane_id]->plane_right=arVector3(v1,v2,v3);
   }
   if(parameter==4) //size
   {
      float v1;
      v1=char_to_float(&d[3]);
     
      printf("    setting size: %f\n",v1);
      slice_vec[plane_id]->size=v1;
   }
   if(parameter==5) //color
   {
      float v1,v2,v3;
      v1=char_to_float(&d[3]);
      v2=char_to_float(&d[7]);
      v3=char_to_float(&d[11]);

      printf("    setting color: %f %f %f\n",v1,v2,v3);
      slice_vec[plane_id]->color=arVector3(v1,v2,v3);
   }
   if(parameter==6) //button id
   {
      unsigned char value=d[3];

      printf("    setting button id: %d\n",value);
      slice_vec[plane_id]->button_id=value;
   }
   if(parameter==7) //is_global
   {
      unsigned char value=d[3];

      printf("    is slice global: %d\n",value);
      slice_vec[plane_id]->is_global=value;
   }
}

void app_parameters::apply_model_drag_parameters(unsigned char *d)
{
   printf("  user requested to set model drag parameters\n");

   unsigned char parameter=d[1];
  
   if(parameter==0) //enable
   {
      unsigned char value=d[2];

      printf("    setting enable: %d\n",value);
      model_drag_enable=value;
   }
   if(parameter==1) //button id
   {
      unsigned char value=d[2];

      printf("    setting button_id: %d\n",value);
      model_drag_button_id=value;
   }
   if(parameter==2) //constrain
   {
      for(int i=0;i<6;i++)
      {
         unsigned char value=d[2+i];

         printf("    setting constrain %d: %d\n",i,value);
         model_drag_constrain[i]=value;
      }
   }
}

void app_parameters::apply_model_transform_parameters(unsigned char *d)
{
   printf("  user requested to set model transform parameters\n");

   unsigned char parameter=d[1];

   if(parameter==0) //matrix4
   {
      float v;
      arMatrix4 m;

      for(int i=0;i<16;i++)
      {
         v=char_to_float(&d[2+i*4]);   
         m.v[i]=v;
      }   

      model_transform=m;
      cout << "got matrix4: " << endl << m << endl;
   }
   if(parameter==1) //inverse matrix4
   {
      float v;
      arMatrix4 m;

      for(int i=0;i<16;i++)
      {
         v=char_to_float(&d[2+i*4]);   
         m.v[i]=v;
      }   

      inverse_model_transform=m;
      cout << "got inverse matrix4: " << endl << m << endl;
   }


}

void app_parameters::apply_grid_parameters(unsigned char *d)
{
   printf("  user requested to set grid parameters\n");

#ifdef DJZ_GRID
   unsigned char parameter=d[1];

   if(parameter==0) //enable
   {
      unsigned char value=d[2];
     
      printf("    setting enable: %d\n",value);
      grid_enable=value;
   }
 
   if(parameter==1 || parameter==2 || parameter==3) //x,y,z param
   {
      int p_id=parameter-1;

      float v1,v2,v3;
      v1=char_to_float(&d[2]);
      v2=char_to_float(&d[6]);
      v3=char_to_float(&d[10]);

      printf("    setting param %d: %f %f %f\n",p_id,v1,v2,v3);
      grid_param[p_id]=arVector3(v1,v2,v3);
   }

   if(parameter==4) //color
   {
      float v1,v2,v3;
      v1=char_to_float(&d[2]);
      v2=char_to_float(&d[6]);
      v3=char_to_float(&d[10]);

      printf("    setting color: %f %f %f\n",v1,v2,v3);
      grid_color=arVector3(v1,v2,v3);
   }
   if(parameter==5) //tick size
   {
      float v1;
      v1=char_to_float(&d[2]);
      printf("    setting tick size: %f\n",v1);
      grid_tick_size=v1;
   }
#endif

}

void app_parameters::apply_cursor_parameters(unsigned char *d)
{
   printf("  user requested to set cursor parameters\n");

   unsigned char parameter=d[1];
 
   if(parameter==0) //enable
   {
      unsigned char value=d[2];

      printf("    setting enable: %d\n",value);
      wand_visable=value;
   }
   if(parameter==1) //height 
   {
      float value=char_to_float(&d[2]);

      printf("    setting length: %f\n",value);
      wand_length=value;
   }
   if(parameter==2) //width 
   {
      float value=char_to_float(&d[2]);

      printf("    setting width: %f\n",value);
      wand_width=value;
   }  
   if(parameter==3) //color
   {
      float v1,v2,v3;
      v1=char_to_float(&d[2]);
      v2=char_to_float(&d[6]);
      v3=char_to_float(&d[10]);

      printf("    setting color: %f %f %f\n",v1,v2,v3);
      wand_color=arVector3(v1,v2,v3);
   }
   if(parameter==4) //model
   {
      unsigned char value=d[2];

      printf("    setting model: %d\n",value);
      wand_model=value;
   }
   if(parameter==5) //position at tip
   {
      unsigned char value=d[2];

      printf("    setting position at tip: %d\n",value);
      wand_position_at_tip=value;
   }
   if(parameter==6) //wand push forward
   {
      float v1=char_to_float(&d[2]);

      printf("    setting wand push forwrad: %f\n",v1);
      wand_push_forward=v1;
   }
}

void app_parameters::apply_wand_parameters(unsigned char *d)
{
   printf("  user requested to set wand parameters\n");
   
   unsigned char parameter=d[1];
 
   if(parameter==0)
   {
      wand_sensor_id=d[2];
      printf("    setting wand_sensor id: %d\n",wand_sensor_id);   
   }
}

void app_parameters::apply_joystick_parameters(unsigned char *d)
{
   printf("  user requested to set joystick parameters\n");
   printf("     currently nothing here! moved to event server variable\n");
   
}

