#include "framework_navigation.h"
#include "app_parameters.h"

arVector3 get_direction_vector(arMatrix4 sensor, arVector3 direction)
{
   return (sensor*direction - sensor*arVector3(0,0,0));
}

void process_single_axis(float delta_move, arMatrix4 wand_sensor, app_parameters *ap, single_axis_navigation_parameters *sanp)
{
   int nf=sanp->navigation_func;

   if(nf==single_axis_navigation_parameters::NAV_NONE)
      return;

   if(nf==single_axis_navigation_parameters::TRANS_RIGHT_LEFT ||
      nf==single_axis_navigation_parameters::TRANS_FORWARD_BACK ||
      nf==single_axis_navigation_parameters::TRANS_UP_DOWN)
   {
      arVector3 v=arVector3(0,0,-1);
      if(nf==single_axis_navigation_parameters::TRANS_RIGHT_LEFT)   v=arVector3(1,0,0);
      if(nf==single_axis_navigation_parameters::TRANS_FORWARD_BACK) v=arVector3(0,0,-1);
      if(nf==single_axis_navigation_parameters::TRANS_UP_DOWN)      v=arVector3(0,1,0);
 
      arVector3 direction;

      if(ap->wand_sensor_id!=0xff)
         direction = get_direction_vector(wand_sensor,v);
      else
         direction = v;

      //process any constraints we may have (ie, restrict flying down to walking)

      for(int i=0;i<3;i++)
      {
         if(sanp->navigation_constrain[i]==true)
            direction[i]=0.0;
      }
    
      arVector3 translate_vec=direction*delta_move;
      #ifdef USE_SZG
         ar_navTranslate(translate_vec);
      #elif USE_FREEVR
         vrUserTravelTranslate3d(VR_ALLUSERS,translate_vec[0],translate_vec[1],translate_vec[2]);
      #endif
   }
   
   if(nf==single_axis_navigation_parameters::YAW ||
      nf==single_axis_navigation_parameters::PITCH ||
      nf==single_axis_navigation_parameters::ROLL)
   {
      arVector3 v=arVector3(0,1,0);
      float rotate_amount=delta_move*-1.0;
      if(nf==single_axis_navigation_parameters::YAW)   v=arVector3(0,1,0);
      if(nf==single_axis_navigation_parameters::ROLL)  v=arVector3(0,0,1);
      if(nf==single_axis_navigation_parameters::PITCH) v=arVector3(1,0,0);

      #ifdef USE_SZG
         ar_navRotate(v,rotate_amount);
      #elif USE_FREEVR
         if(v==arVector3(1,0,0))
            vrUserTravelRotateId(VR_ALLUSERS, VR_X, rotate_amount); 		
         if(v==arVector3(0,1,0))
            vrUserTravelRotateId(VR_ALLUSERS, VR_Y, rotate_amount); 		   
         if(v==arVector3(0,0,1))
            vrUserTravelRotateId(VR_ALLUSERS, VR_Z, rotate_amount); 		
      #endif
   }
}

void framework_navigation(double delta_time, arMatrix4 wand_sensor, joysticks *jsticks, app_parameters *ap)
{
   float delta_move, axis_value;

   for(int i=0;i<ap->nav_vec.size();i++)
   {
      single_axis_navigation_parameters *sanp=ap->nav_vec[i];
  
      axis_value=jsticks->get_joystick(sanp->axis); 
      if(fabs(axis_value)<0.1)
         axis_value=0.0;

      delta_move=delta_time * axis_value * sanp->navigation_mult;
      process_single_axis(delta_move, wand_sensor, ap, sanp);
   }
}
