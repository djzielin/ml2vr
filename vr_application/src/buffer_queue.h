#ifndef BUFFER_QUEUE_DJZ_H
#define BUFFER_QUEUE_DJZ_H

#ifndef FREEVR
  #include"arThread.h"
#else
  #include "szgLock.h"
  #include "szgTime.h"
#endif

#include<vector>
#include<deque>

using namespace std;

class buffer_queue
{
public:
      buffer_queue(unsigned int num, unsigned int size)
      {
         for(int i=0;i<num;i++)
         {
             unsigned char *buffer=new unsigned char[size];
             if(buffer==NULL)
             {
                printf("couldn't alloc the buffer in buffer_queue: %d\n",i);
                exit(1);
             }
             available_buffers.push_back(buffer);
         }
         data_lock=new arLock();
      } 

      void store_buffer(unsigned char *buff, unsigned int length)
      {
         printf("buffer queue - storing buffer of size: %d\n",length);
         unsigned int avail=0;
         while(avail==0)
         {
           data_lock->lock();
           avail=available_buffers.size();
           
           if(avail==0)
           {
              data_lock->unlock();
              printf("used up all our buffer storage, sleeping\n");
              ar_usleep(1000); 
           }
         }

         unsigned char *b=available_buffers.back();       
         memcpy(b,buff,length);

         available_buffers.pop_back();
         fifo_queue.push_back(b);      //TODO: the queue should really be a queue of objects that wrap the char pointer and the size together.
		   fifo_sizes.push_back(length);

         printf("buffer queue - store complete. stored: %d avail: %d\n",fifo_queue.size(),available_buffers.size());
      
         data_lock->unlock();   

      }

      unsigned char *pop_buffers(int how_many, unsigned int &size)
      {
         printf("buffer_queue - poping off %d\n",how_many);
         if(how_many==0) return NULL;

         data_lock->lock(); 
         unsigned char *val=fifo_queue[how_many-1];
		   size=fifo_sizes[how_many-1];

         for(int i=0;i<how_many;i++)
         {
            remove_single_buffer();

         }

         data_lock->unlock();

         return val; 
      }



private:

   deque<unsigned char *> fifo_queue;
   deque<unsigned int> fifo_sizes;

   vector<unsigned char *> available_buffers;

   void remove_single_buffer()
   {
      unsigned char *b=fifo_queue.front();
      available_buffers.push_back(b);
      fifo_queue.pop_front();
	  fifo_sizes.pop_front();
   }

   arLock *data_lock;
};

#endif
