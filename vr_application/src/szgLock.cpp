//********************************************************
// Syzygy is licensed under the BSD license v2
// see the file SZG_CREDITS for details
//********************************************************
// http://syzygy.isl.uiuc.edu/szg/index.html

#include "szgLock.h"

//Implementation lifted from MUTEX class of
// Walmsley, "Multi-threaded Programming in C++"
arLock::arLock() {
#ifdef WIN32
  _mutex = CreateMutex( NULL, FALSE, NULL);
#else
  pthread_mutex_init( &_mutex, NULL );
#endif
}

arLock::~arLock() {
#ifdef WIN32
  CloseHandle(_mutex);
#else
  pthread_mutex_destroy( &_mutex );
#endif
}

void arLock::lock() {
#ifdef WIN32
  WaitForSingleObject( _mutex, INFINITE );
#else
  pthread_mutex_lock( &_mutex );
#endif
}

bool arLock::tryLock() {
#ifdef WIN32
  return (WaitForSingleObject( _mutex, 0 ) != WAIT_TIMEOUT);
#else
  return (pthread_mutex_trylock( &_mutex ) == 0);
#endif
}

void arLock::unlock() {
#ifdef WIN32
  ReleaseMutex( _mutex );
#else
  pthread_mutex_unlock( &_mutex );
#endif
}
