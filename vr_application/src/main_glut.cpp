/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <iostream>
#include <string>
#include <signal.h>
#include <GL/glut.h>
#include "GL_cube.h"

#include "arMath.h"
#include "arTime.h"
#include "arLock.h"

#include "interface_server.h"

using namespace std;

float viewer_angle=0;
float viewer_distance=30;

int glut_window;

bool l_button=false;
bool r_button=false;
bool m_button=false;

arVector3 prev_mouse_position;

arVector3 cs_position;
bool control_pressed;
bool shift_pressed;
bool alt_pressed;

arMatrix4 wand_matrix4;
arVector3 wand_pos=arVector3(0,3,0);
float wand_rot_x=0;
float wand_rot_y=0;

int window_width,window_height;

interface_server *is;
app_parameters *ap;

bool is_spacebar_pressed=false;

void reset_navigation()
{

}

void update_modifiers()
{
   int mod = glutGetModifiers();

   control_pressed=mod & GLUT_ACTIVE_CTRL;
   shift_pressed=  mod & GLUT_ACTIVE_SHIFT;
   alt_pressed=    mod & GLUT_ACTIVE_ALT;
}

void mouse_buttons(int button, int state, int x, int y)
{
   update_modifiers();

   if(button == GLUT_RIGHT_BUTTON)
   {
      if(state == GLUT_DOWN)
         r_button=true;
      else
         r_button=false;

      if(control_pressed || shift_pressed || alt_pressed)
         is->update_button(1,r_button);
      else
         is->update_button(1,false);

   }
   else if(button == GLUT_LEFT_BUTTON)
   {
      if(state == GLUT_DOWN)
      {
         l_button=true;
      }
      else
         l_button=false;

      if(control_pressed || shift_pressed)
         is->update_button(0,l_button);
      else
         is->update_button(0,false);

   }

   else if(button == GLUT_MIDDLE_BUTTON || alt_pressed)
   {
      if(state == GLUT_DOWN)
      {
         m_button=true;
      }
      else
         m_button=false;

      if(control_pressed || shift_pressed || alt_pressed)
         is->update_button(2,m_button);
      else
         is->update_button(2,false);

   }
  
//   printf("button: %d state: %d\n",button,state);

 //  if(button==3) // for mouse scroll wheel
 //  if(button==4)



   prev_mouse_position=arVector3(x,y,0);

}

void compute_wand_matrix()
{
   wand_matrix4=ar_identityMatrix();
   wand_matrix4=wand_matrix4*ar_translationMatrix(wand_pos);
   wand_matrix4=wand_matrix4*ar_rotationMatrix('y',wand_rot_y*M_PI/180.0);
   wand_matrix4=wand_matrix4*ar_rotationMatrix('x',wand_rot_x*M_PI/180.0);
}

void process_wand_control(int x, int y)
{
   if((control_pressed || shift_pressed) && !alt_pressed)
   {
      int x_axis=0;
      int y_axis=1;
      int y_sign=1;

      if(control_pressed)
      {
         x_axis=0;
         y_axis=2;
         y_sign=1;
      }
      if(shift_pressed)
      {
         x_axis=0;
         y_axis=1;
         y_sign=-1;
      }

      arVector3 pos=arVector3(x,y,0);
 
      float difference=pos[0]-prev_mouse_position[0];
      float difference2=pos[1]-prev_mouse_position[1];

      if(fabs(difference)>fabs(difference2))
      {        
         wand_pos[x_axis]+=difference*0.05;
      }
      else
      {
         wand_pos[y_axis]+=difference2*0.05*(float)y_sign;
      }

      

      //cout << "wand pos is now: " << wand_pos << endl;
   }
   if(alt_pressed && !control_pressed && !shift_pressed)
   {
      //printf("adjusting wand orientation\n");

      arVector3 pos=arVector3(x,y,0);
 
      float difference=pos[0]-prev_mouse_position[0];
      float difference2=pos[1]-prev_mouse_position[1];

      if(fabs(difference)>fabs(difference2))
      {        
         wand_rot_y+=difference*(-0.1);
      }
      else
      {
         wand_rot_x+=difference2*(-0.1);
      }
      //printf("rot y: %f rot x: %f\n",wand_rot_y,wand_rot_x);

   }

   if(alt_pressed || control_pressed || shift_pressed)
   {
      compute_wand_matrix();
      is->update_wand_matrix4(1,wand_matrix4*ar_translationMatrix(0.0f,0.0f,-1.0f)); //translate to tip of wand
   }
}

void process_joystick(int x, int y)
{
   arVector3 pos=arVector3(x,y,0);
 
   float difference=pos[0]-prev_mouse_position[0];
   float difference2=pos[1]-prev_mouse_position[1];

   if(x<0) x=0;
   if(x>window_width) x=window_width;
   if(y<0) y=0;
   if(y>window_height) y=window_height;

   float x_val=((float)x/(float)window_width)*2.0-1.0;  // get -1 to 1
   float y_val=-1.0*(((float)y/(float)window_height)*2.0-1.0); // get -1 to 1   

   printf("joystick values: %f %f\n", x_val, y_val);

   is->update_joystick(0,x_val);
   is->update_joystick(1,y_val);
}

void mouse_passive_motion(int x, int y)
{
   update_modifiers();

   process_wand_control(x,y);

   if(is_spacebar_pressed)
      process_joystick(x,y);   

   prev_mouse_position=arVector3(x,y,0);
}

void mouse_motion(int x, int y)
{
   update_modifiers();
   process_wand_control(x,y);
   
   if(!control_pressed && !shift_pressed && !alt_pressed)
   {
     //printf("we will orbit the DiVE\n");

     if (l_button)
     {
      //cout << "Mouse dragged with left button at " << "(" << x << "," << y << ")" << endl;

      arVector3 pos=arVector3(x,y,0);
 
      float difference=pos[0]-prev_mouse_position[0];
      float difference2=pos[1]-prev_mouse_position[1];

      if(fabs(difference)>fabs(difference2))
      {        
         viewer_angle+=difference*0.1;
      }
      else
      {
         viewer_distance+=difference2*0.05;
      }
   }

   if(viewer_distance<0) viewer_distance=0;
   if(viewer_angle>360) viewer_angle-=360;
   if(viewer_angle<360) viewer_angle+=360;
   }

   prev_mouse_position=arVector3(x,y,0);
}


void processSpecialKeys(int key, int x, int y)
{
   switch(key)
   {
      case GLUT_KEY_LEFT :
         break;
      case GLUT_KEY_RIGHT :
         break;
      case GLUT_KEY_UP :
	 break;
      case GLUT_KEY_DOWN :
         break;
   }
}

void processNormalKeys(unsigned char key, int x, int y)
{ 
   //printf("key pressed: %d pos: %d %d\n",key,x,y);

   if(key==27) //escape
   {
      exit(0);
   }
   if(key==32) //spacebar
   {
      is_spacebar_pressed=true;
      process_joystick(x,y);
   }
}

void processNormalKeysRelease(unsigned char key, int x, int y)
{ 
   //printf("key released: %d\n",key);

   if(key==27) //escape
   {
      exit(0);
   }
   if(key==32) //space
   {
     is_spacebar_pressed=false;
     process_joystick(window_width/2.0,window_height/2.0);
   }
}

void draw_dive()
{
   arVector3 dive_corners[8];

   dive_corners[0]=arVector3(-4.755,0,4.755);
   dive_corners[1]=arVector3(-4.755,0,-4.755); 
   dive_corners[2]=arVector3(4.755,0,4.755);
   dive_corners[3]=arVector3(4.755,0,-4.755);    
   dive_corners[4]=arVector3(-4.755,9.51,4.755);
   dive_corners[5]=arVector3(-4.755,9.51,-4.755); 
   dive_corners[6]=arVector3(4.755,9.51,4.755);
   dive_corners[7]=arVector3(4.755,9.51,-4.755);  

   glColor3f(0,0,0);

   glBegin(GL_LINES);
      glVertex3fv(dive_corners[0].v);
      glVertex3fv(dive_corners[1].v);
      glVertex3fv(dive_corners[1].v);
      glVertex3fv(dive_corners[3].v);
      glVertex3fv(dive_corners[3].v);
      glVertex3fv(dive_corners[2].v);
      glVertex3fv(dive_corners[2].v);
      glVertex3fv(dive_corners[0].v);

      glVertex3fv(dive_corners[4].v);
      glVertex3fv(dive_corners[5].v);
      glVertex3fv(dive_corners[5].v);
      glVertex3fv(dive_corners[7].v);
      glVertex3fv(dive_corners[7].v);
      glVertex3fv(dive_corners[6].v);
      glVertex3fv(dive_corners[6].v);
      glVertex3fv(dive_corners[4].v);

      glVertex3fv(dive_corners[0].v);
      glVertex3fv(dive_corners[4].v);
      glVertex3fv(dive_corners[1].v);
      glVertex3fv(dive_corners[5].v);
      glVertex3fv(dive_corners[3].v);
      glVertex3fv(dive_corners[7].v);
      glVertex3fv(dive_corners[2].v);
      glVertex3fv(dive_corners[6].v);
   glEnd();
}

void setup_opengl_settings()
{
   glShadeModel(GL_SMOOTH); 
   glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
   glEnable(GL_COLOR_MATERIAL);
   glEnable(GL_LINE_SMOOTH);
   glEnable(GL_NORMALIZE); 
   glEnable(GL_DEPTH_TEST);

   glFrontFace( GL_CW );
   glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);
}

void setup_lights()
{
   GLfloat  position_0[4] = { 0.0, 0.0, 0.0, 1.0 }, // 0 = directional, 1 = location
            ambient_0[4] =  { 0.4, 0.4, 0.4, 1.0 },
            diffuse_0[4] =  { 1.0, 1.0, 1.0, 1.0 },
            specular_0[4] = { 0.0, 0.0, 0.0, 0.0 };

   glLightfv(GL_LIGHT0, GL_POSITION, position_0);
   glLightfv(GL_LIGHT0, GL_AMBIENT,  ambient_0);  
   glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuse_0);  
   glLightfv(GL_LIGHT0, GL_SPECULAR, specular_0); 
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glPushMatrix();
      glTranslatef(0,10,10);
      glLightfv(GL_LIGHT0, GL_POSITION, position_0);
      glColor3f(1,1,0);
      //glutSolidSphere(0.5, 16, 16 ); //if we want to visualize where light is
   glPopMatrix();
}

void setup_camera()
{
   glLoadIdentity();

   // Set the camera
   gluLookAt(	0.0f, 5.0f, viewer_distance,
		0.0f, 5.0f,  0.0f,
		0.0f, 1.0f,  0.0f);

   glRotatef(viewer_angle, 0.0f, 1.0f, 0.0f);
}



#define WAND_RADIUS 0.025f


//TODO user app parameters
void draw_wand()
{
   glColor3ub(50, 50, 50);

   glPushMatrix();
      glMultMatrixf(wand_matrix4.v);
      glTranslatef(0.0f, 0.0f, -0.5f);
      glScalef(WAND_RADIUS,  WAND_RADIUS, 0.5f); // 1 foot long
      draw_display_list_wireframe_cube();
   glPopMatrix();
}

void renderScene(void)
{
   arVector3 b_color=ap->background_color;

   glClearColor(b_color[0],b_color[1],b_color[2],1.0); //world background color 
   glClearDepth(1.0);
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

   setup_camera();
   //setup_lights();
   glDisable(GL_LIGHTING);
   draw_dive();

   draw_wand();

   //draw world objects here

   glutSwapBuffers();
}

ar_timeval last_time;

void update_scene(void)
{
   ar_timeval now=ar_time();
   double elapsed_time=ar_difftime(now,last_time) / 1e6;
   last_time=now;

   single_app_param *sd;
   bool result=is->get_waiting_app_parameter(sd);
   if(result)
   {
      printf("we got a parameter!\n");

      printf("we have a single waiting app parameter of size: %d\n",sd->size);

      ap->apply_single_parameter(sd->data);

   }
 
   // Force redraw
   glutPostRedisplay();
}

void changeSize(int w, int h)
{
   // Prevent a divide by zero, when window is too short
   // (you cant make a window of zero width).

   if(h == 0)
      h = 1;
   float ratio = 1.0* w / h;

   // Use the Projection Matrix
   glMatrixMode(GL_PROJECTION);

   // Reset Matrix
   glLoadIdentity();

   // Set the viewport to be the entire window
   glViewport(0, 0, w, h);

   // Set the correct perspective.
   gluPerspective(45,ratio,1,1000);

   // Get Back to the Modelview
   glMatrixMode(GL_MODELVIEW);

   window_width=w;
   window_height=h;
}

int main(int argc, char **argv)
{
   ap=new app_parameters();
   is=new interface_server(9999); //TODO: should be based off argc/argv
   is->set_is_simulator(true);

   compute_wand_matrix();
   is->update_wand_matrix4(1,wand_matrix4);

   // init GLUT and create Window 
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
   glutInitWindowPosition(10,10);
   glutInitWindowSize(800,600);
   glut_window=glutCreateWindow("ML2VR Simulator");

   setup_opengl_settings();
   init_display_list_cube();

   last_time=ar_time();

   // register callbacks
   glutDisplayFunc(renderScene);
   glutIdleFunc(update_scene);
   glutReshapeFunc(changeSize);
   glutSpecialFunc(processSpecialKeys);
   glutKeyboardFunc(processNormalKeys);
   glutKeyboardUpFunc(processNormalKeysRelease);
   glutMouseFunc(mouse_buttons);
   glutPassiveMotionFunc(mouse_passive_motion);
   glutMotionFunc(mouse_motion);

   glutIgnoreKeyRepeat(1);


   // enter GLUT event processing cycle
   glutMainLoop();
}
