/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl_receiver.h"
#include <iostream>

#ifndef FREEVR
   #include "arMasterSlaveFramework.h"
#endif

using namespace std;
   
/*
void dump_packet(unsigned char *data, unsigned int size)
{
   GLfloat *v;

   for(int i=0;i<size;)
   {
      unsigned int command_length=data[i];
	  unsigned int command=data[i+1];
     
     switch(command)
	  {
      case 0: //glbegin
	     printf("  glBegin: %d\n",data[i+2]);
        break;
	  case 1: //glend
		  printf("  glEnd\n");
		  break;
	  case 2: //glVertex3f 
        v=((GLfloat *)&data[i+2]);
		  printf("  glVertex3f: %f %f %f \n",v[0],v[1],v[2]);
		 break;
	  case 3: //glColor4f
        v=((GLfloat *)&data[i+2]);
		  printf("  glColor4f: %f %f %f %f\n",v[0],v[1],v[2],v[3]);
 		 break;
      }
	  i+=(1+command_length);
   }
}*/


opengl_receiver::opengl_receiver(int port, string swap_server_ip, string swap_server_port)
{
   printf("opengl_receiver - in constructor\n",port);

   data_lock=new arLock();

   boost::thread t( boost::bind( &opengl_receiver::server, this, port));

   //_log=fopen("receiver_log.txt","w");

   _bq=new buffer_queue(10,10000000);

   connect_to_result_server(swap_server_ip,swap_server_port);

}

void opengl_receiver::connect_to_result_server(string ip, string port)
{
  _connected_to_result_server=false;
  int attempts=0;
  tcp::socket *s;

  while(_connected_to_result_server==false)
  {
      printf("opengl_receiver - trying to connect to swap server. attempt: %d\n",attempts);
      try
      {
         tcp::resolver resolver(io_service);
         tcp::resolver::query query(tcp::v4(), ip, port);
         tcp::resolver::iterator iterator = resolver.resolve(query);
         s=new tcp::socket(io_service);

         tcp::resolver::iterator end;

         // Try each endpoint until we successfully establish a connection.
         boost::system::error_code error = boost::asio::error::host_not_found;
         while (error && iterator != end)
         {
            s->close();
            s->connect(*iterator++, error);
         }
         if (error)
         {
            printf("unable to connect to swap server\n");
            
       
            throw boost::system::system_error(error);
         }
       

       // tcp::resolver resolver(io_service);
        //tcp::resolver::query query(tcp::v4(), ip, port);
        //tcp::resolver::iterator iterator = resolver.resolve(query);
	     _result_socket=s; //new tcp::socket(io_service);

        //boost::asio::connect(*_result_socket, iterator); //not available in new version of BOOST!
 	     _result_socket->set_option(tcp::no_delay(true));
        _connected_to_result_server=true;
      }
      catch (std::exception& e)
      {
         printf("error connecting to the swap server!: %s\n",e.what());
         ar_usleep(100000);
         attempts++;
      }
  }

  printf("opengl_receiver - success connecting to swap server\n");
}

extern unsigned int post_frame;

void opengl_receiver::session(tcp::socket* sock) //receive data from matlab
{
   boost::system::error_code error;
   unsigned int buffer_length;

   cout << "started new session to receive data from matlab" << endl;

   try
   {
      for (;;)
      {       
         size_t length = boost::asio::read(*sock, boost::asio::buffer(&buffer_length,4), boost::asio::transfer_all(), error);
         ar_timeval a=ar_time();
 
         if (error == boost::asio::error::eof || length==0)
         {
            printf("connection close by client\n");
            break; // Connection closed cleanly by peer.
         }
         else if (error)
            throw boost::system::system_error(error); // Some other error.

         printf("opengl_receiver - received buffer length: %d\n",buffer_length);
         
         length = boost::asio::read(*sock, boost::asio::buffer(data,buffer_length), boost::asio::transfer_all(), error);

         ar_timeval b=ar_time();

         if (error == boost::asio::error::eof || length==0)
         {
            printf("connection close by client\n");
            break; // Connection closed cleanly by peer.
         }
         else if (error)
            throw boost::system::system_error(error); // Some other error.

         printf("opengl_receiver - got all the data: %d\n",buffer_length);
         //dump_packet(data,buffer_length);
         _bq->store_buffer(data,buffer_length);

         ar_timeval c=ar_time();

         if(_connected_to_result_server)
         {
            unsigned char message=1;
            _connected_to_result_server=send_buffer(_result_socket,1,&message);
         }

         if(_connected_to_result_server==false) //TODO, better way to disconnect socket?
            break;

         ar_timeval d=ar_time();
 

         printf("opengl_receiver - %f ms read from matlab at frame: %d\n",ar_difftime(b,a)/1e3,post_frame);
         printf("opengl_receiver - %f ms store buffer\n",ar_difftime(c,b)/1e3);
         printf("opengl_receiver - %f ms send packet to swap_controller\n",ar_difftime(d,c)/1e3);

      }
   }

  catch (std::exception& e)
  {
    std::cerr << "Exception in thread: " << e.what() << "\n";
  }
}

bool opengl_receiver::send_buffer(tcp::socket* sock, int buffer_length, unsigned char *buffer)
{
   try
   {
      write(*sock,boost::asio::buffer(buffer,buffer_length));
   }   
   catch (std::exception& e)
   {
       printf("networking exception during send: %s",e.what());
       return false;
   }

   return true;
}

void opengl_receiver::server(short port)
{
   try
   {
      tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
      for (;;)
      {
         printf("opengl_receiver - waiting for a new opengl connection from Matlab on port: %d\n",port);
         tcp::socket *sock(new tcp::socket(io_service));
         a.accept(*sock);
         sock->set_option(tcp::no_delay(true));
         boost::thread t(boost::bind( &opengl_receiver::session, this, sock));
      }
   }
   catch (std::exception& e)
   {
      std::cerr << "Exception: " << e.what() << "\n";
   }
}

