#ifndef DJZ_TRANSFER_THREAD
#define DJZ_TRANSFER_THREAD

#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio/read.hpp>
using boost::asio::ip::tcp;

#ifndef FREEVR
  #include "arThread.h"
  #include "arMasterSlaveFramework.h"
#else
  #include "szgLock.h"
  #include "szgTime.h"
#endif

#include <vector>

typedef boost::shared_ptr<tcp::socket> socket_ptr;



class swap_controller
{
public:
  swap_controller(short port, int number_clients);
  unsigned int get_num_frames_ready_for_swap();
  void reduce_num_frames_ready_for_swap(int how_many);

private:  
   void listen_thread();
   void result_server(short port);

   arLock *data_lock;
   std::vector<socket_ptr> all_clients;
   unsigned int num_frames_ready_for_swap;
   bool _all_connected;
   int _num_clients;

   boost::asio::io_service io_service;
};

#endif
