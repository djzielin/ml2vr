/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FREEVR
   #include "arPrecompiled.h"
   #include "arMasterSlaveFramework.h"
#else
   #include <GL/gl.h>
#endif

void headlamp(arMasterSlaveFramework& fw)
{
   ///////////////////// SETUP LIGHTS //////////////////////////////////////////
   GLfloat  position_0[4] = { 0.0, 0.0,0.0, 1.0 }, // 0 = directional, 1 = location
            ambient_0[4] =  { 0.2, 0.2, 0.2, 1.0 },
            diffuse_0[4] =  { 1.0, 1.0, 1.0, 1.0 },
            specular_0[4] = { 0.0, 0.0, 0.0, 0.0 };

   glPushMatrix();
      glMultMatrixf(fw.getMidEyeMatrix().v);

      glLightfv(GL_LIGHT0, GL_POSITION, position_0);
      glLightfv(GL_LIGHT0, GL_AMBIENT,  ambient_0);  //TODO probably only need to do this once?
      glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuse_0);  //
      glLightfv(GL_LIGHT0, GL_SPECULAR, specular_0); //
      glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
   glPopMatrix();
}
